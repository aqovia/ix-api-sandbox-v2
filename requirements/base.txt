Django==2.2.9
django-enumfields==1.0.0
django-filter==2.1.0 
django-polymorphic==2.1.2
djangorestframework==3.11.0
pytz==2018.9

# Database
# psycopg2==2.8.4
psycopg2-binary==2.8.4

# JWT
pyjwt==1.7.1

# Setup
Faker==1.0.2
prompt-toolkit==2.0.8

# Celery
redis==3.3.11
celery==4.4.0
kombu==4.6.7

# Schema
coreapi
pyyaml

git+https://gitlab.com/ix-api/ix-api-schema.git@develop#egg=ix_api_schema

