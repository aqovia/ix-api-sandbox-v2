"""
JEA URL Configuration
"""
from django.conf.urls import url
from django.urls import include

from jea import admin
from jea.public import urls as public_urls
from jea.api.v2 import urls as api_v2_urls

urlpatterns = [
    url(r"^", include(public_urls)),
    url(r"^admin/", admin.site.urls),
    url(r"^api/v2/", include(api_v2_urls)),
]

