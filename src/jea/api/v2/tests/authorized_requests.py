
"""
API request helpers
"""

from rest_framework import test

from jea.crm import models as crm_models
from jea.auth import roles
from jea.api.v2.auth.users import ApiUser


def _make_api_user(account: crm_models.Account) -> ApiUser:
    """Create api user from account"""
    return ApiUser(account=account, roles=[
        roles.ACCESS,
    ])

#
# Request factories
#
def get(
        account: crm_models.Account,
        data=None,
        path="",
        format="json",
        **extra,
    ):
    """Create an authorized GET request"""
    api_user = _make_api_user(account)
    request = test.APIRequestFactory().get(
        path, data=data, format=format, **extra)
    test.force_authenticate(request, user=api_user)

    return request


def post(
        account: crm_models.Account,
        data=None,
        path="",
        format="json",
        **extra,
    ):
    """Create an authorized POST request"""
    api_user = _make_api_user(account)
    request = test.APIRequestFactory().post(
        path, data=data, format=format, **extra)
    test.force_authenticate(request, user=api_user)

    return request


def put(
        account: crm_models.Account,
        data=None,
        path="",
        format="json",
        **extra,
    ):
    """Create an authorized PUT request"""
    api_user = _make_api_user(account)
    request = test.APIRequestFactory().put(
        path, data=data, format=format, **extra)
    test.force_authenticate(request, user=api_user)

    return request


def patch(
        account: crm_models.Account,
        data=None,
        path="",
        format="json",
        **extra,
    ):
    """Create an authorized PATCH request"""
    api_user = _make_api_user(account)
    request = test.APIRequestFactory().patch(
        path, data=data, format=format, **extra)
    test.force_authenticate(request, user=api_user)

    return request


def delete(
        account: crm_models.Account,
        data=None,
        path="",
        format="json",
        **extra,
    ):
    """Create an authorized DELETE request"""
    api_user = _make_api_user(account)
    request = test.APIRequestFactory().delete(
        path, data=data, format=format, **extra)
    test.force_authenticate(request, user=api_user)

    return request
