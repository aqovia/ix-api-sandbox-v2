
"""
Tests for the api v2 auth ApiUser
"""

from datetime import datetime

from model_bakery import baker

from jea.api.v2.auth.users import ApiUser


def test__token_claims_are_assigned():
    """
    Test that all required token claims are assigned to the
    user object.
    """
    account = baker.prepare("crm.Account")
    root_account = baker.prepare("crm.Account")

    claims = {
        "iss": "1",
        "sub": None,
        "iat": datetime(2000, 1, 1),
        "exp": datetime(2000, 1, 1),
        "roles": [
            "access"
        ]
    }

    user = ApiUser(account, claims)

    assert user.account == account
    assert user.account_id == account.id

    assert user.access_roles == claims["roles"]

