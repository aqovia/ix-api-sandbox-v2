
import pytest
from model_bakery import baker

from rest_framework.test import APIRequestFactory

from jea.auth.services import token_authentication as token_authentication_svc
from jea.api.v2.auth import handlers
from jea.api.v2.auth.users import ApiUser



@pytest.mark.django_db
def test_jwt_authentication_authenticate():
    """Test authenticating a request"""
    # Create account and subaccount
    account = baker.make("crm.Account")
    access_token, _ = token_authentication_svc.issue_tokens(
        account.pk)

    # Make request
    request = APIRequestFactory().get("/api/example")
    request.META["HTTP_AUTHORIZATION"] = b"Bearer " + \
        access_token.encode('utf-8')

    # Authenticate request
    handler = handlers.JWTAuthentication()
    user, token = handler.authenticate(request)

    assert isinstance(user, ApiUser), \
        "Expected user to be a valid API user"

    assert user.account.pk == account.pk


@pytest.mark.django_db
def test_api_session_authentication_authenticate():
    """Test authenticating a request"""
    # Create account and subaccount
    user = baker.make("jea_auth.User")
    account = baker.make("crm.Account")

    # Make request
    request = APIRequestFactory().get("/api/example")
    request.user = user
    request.session = {
        "auth_account_id": account.id,
    }
    request._request = request

    # Authenticate request
    handler = handlers.ApiSessionAuthentication()
    result = handler.authenticate(request)
    assert result

    user, _ = result
    assert isinstance(user, ApiUser), \
        "Expected user to be a valid API user"

    assert user.account.id == account.id

