
"""
Test Catalog Views
"""

import pytest
from model_bakery import baker
from rest_framework import test

from jea.api.v2.tests import authorized_requests
from jea.api.v2.catalog import views
from jea.api.v2.auth import users


@pytest.mark.django_db
def test_facilities_view_set__list():
    """Test list all facilities"""
    account = baker.make("crm.Account")
    baker.make("catalog.Facility")
    view = views.FacilitiesViewSet.as_view({"get": "list"})

    # Make request
    request = authorized_requests.get(account)
    response = view(request)
    assert response.status_code == 200


@pytest.mark.django_db
def test_facilities_view_set__retrieve():
    """Test retrieving a facility"""
    account = baker.make("crm.Account")
    facility = baker.make("catalog.Facility")

    # Make request
    view = views.FacilitiesViewSet.as_view({"get": "retrieve"})
    request = authorized_requests.get(account)
    response = view(request, pk=facility.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_devices_view_set__list():
    """Test listing devices"""
    account = baker.make("crm.Account")
    baker.make("catalog.Device")
    view = views.DevicesViewSet.as_view({"get": "list"})
    request = authorized_requests.get(account)

    response = view(request)
    assert response.status_code == 200


@pytest.mark.django_db
def test_devices_view_set__retrieve():
    """Test retrieving a device"""
    account = baker.make("crm.Account")
    device = baker.make("catalog.Device")

    view = views.DevicesViewSet.as_view({"get": "retrieve"})
    request = authorized_requests.get(account)

    response = view(request, pk=device.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_points_of_presence_view_set__list():
    """Test getting all pops"""
    account = baker.make("crm.Account")
    baker.make("catalog.PointOfPresence")
    request = authorized_requests.get(account)
    view = views.PointsOfPresenceViewSet.as_view({
        "get": "list",
    })

    response = view(request)
    assert response.status_code == 200


@pytest.mark.django_db
def test_points_of_presence_view_set__retrieve():
    """Test getting a specific device"""
    account = baker.make("crm.Account")
    pop = baker.make("catalog.PointOfPresence")
    request = authorized_requests.get(account)
    view = views.PointsOfPresenceViewSet.as_view({
        "get": "retrieve",
    })

    response = view(request, pk=pop.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_products_view_set__list():
    """Test getting all the products"""
    account = baker.make("crm.Account")
    baker.make("catalog.ExchangeLanNetworkProduct")
    baker.make("catalog.ELineNetworkProduct")
    baker.make("catalog.ELanNetworkProduct")
    baker.make("catalog.ETreeNetworkProduct")
    request = authorized_requests.get(account)
    view = views.ProductsViewSet.as_view({
        "get": "list",
    })

    response = view(request)
    assert response.status_code == 200


@pytest.mark.django_db
def test_products_view_set__retrieve():
    """Test getting a specific product"""
    account = baker.make("crm.Account")
    product = baker.make("catalog.ExchangeLanNetworkProduct")
    request = authorized_requests.get(account)
    view = views.ProductsViewSet.as_view({"get": "retrieve"})

    response = view(request, pk=product.pk)
    assert response.status_code == 200


