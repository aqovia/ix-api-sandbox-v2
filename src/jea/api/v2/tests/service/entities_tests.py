
"""
Test Service Serializers
"""

import pytest
from model_bakery import baker
from ixapi_schema.v2.entities import service

from jea.crm.services import contacts as contacts_svc

#
# Helper: Create a service with product
#
def _make_service(service_model, product_model):
    """Create a service with a product"""
    product = baker.make(product_model)
    network_service = baker.make(
        service_model,
        product=product,
        nsc_required_contact_roles=[
            contacts_svc.get_default_role("noc"),
        ])

    return network_service


@pytest.mark.django_db
def test_exchange_lan_network_serializer():
    """
    Test serialization of exchange lan network services
    """
    network_service = _make_service(
        "service.ExchangeLanNetworkService",
        "catalog.ExchangeLanNetworkProduct",
    )
    serializer = service.ExchangeLanNetworkService(network_service)

    result = serializer.data
    assert result, "Serializer should serialize without crashing"

    # Check embedded / inlines
    assert isinstance(result["ips"], list)


@pytest.mark.django_db
def test_eline_serializer():
    """
    Test serialization of eline network services
    """
    network_service = _make_service(
        "service.ELineNetworkService",
        "catalog.ELineNetworkProduct",
    )
    serializer = service.ELineNetworkService(network_service)

    result = serializer.data
    assert result, "Serializer should serialize without crashing"


@pytest.mark.django_db
def test_elan_serializer():
    """
    Test serialization of elan network services
    """
    network_service = _make_service(
        "service.ELanNetworkService",
        "catalog.ELanNetworkProduct",
    )
    serializer = service.ELanNetworkService(network_service)

    result = serializer.data
    assert result, "Serializer should serialize without crashing"


@pytest.mark.django_db
def test_etree_serializer():
    """
    Test serialization of eline network services
    """
    network_service = _make_service(
        "service.ETreeNetworkService",
        "catalog.ETreeNetworkProduct",
    )
    serializer = service.ETreeNetworkService(network_service)

    result = serializer.data
    assert result, "Serializer should serialize without crashing"


@pytest.mark.django_db
def test_polymorphic_network_service_serializer_serialize():
    """
    Test the polymorphic network service serializer
    serialization method.
    """
    exchange_lan = _make_service(
        "service.ExchangeLanNetworkService",
        "catalog.ExchangeLanNetworkProduct",
    )
    eline = _make_service(
        "service.ELineNetworkService",
        "catalog.ELineNetworkProduct",
    )
    elan = _make_service(
        "service.ELanNetworkService",
        "catalog.ELanNetworkProduct",
    )
    etree = _make_service(
        "service.ETreeNetworkService",
        "catalog.ETreeNetworkProduct",
    )

    services = [exchange_lan, eline, elan, etree]
    serializer = service.NetworkService(services, many=True)
    result = serializer.data

    assert result, "Serializer should serialize without crashing."

#
# Features
#
@pytest.mark.django_db
def test_ixp_specific_feature_flag_serializer():
    """Test feature flag serialization"""
    feature_flag = baker.make("service.IXPSpecificFeatureFlag")
    serializer = service.IXPSpecificFeatureFlag(feature_flag)
    assert serializer.data, "Serializer should serialize without crashing"


@pytest.mark.django_db
def test_feature_base_serializer():
    """Test base serializer for features"""
    feature = baker.make("service.NetworkFeature")
    serializer = service.NetworkFeatureBase(feature)
    result = serializer.data
    assert result, "Serializer should serialize without crashing"

    # Check embeddings
    assert isinstance(result["flags"], list)


@pytest.mark.django_db
def test_blackholing_feature_serializer():
    """Test serializing a blackholing feature"""
    feature = baker.make("service.BlackholingNetworkFeature")
    serializer = service.BlackholingNetworkFeature(feature)
    assert serializer.data, "Serializer should serialize without crashing"


@pytest.mark.django_db
def test_route_server_feature_serializer():
    """Test route server feature serialization"""
    ip4 = baker.make("ipam.IpAddress", version=4)
    feature = baker.make("service.RouteServerNetworkFeature",
                         ip_addresses=[ip4])
    serializer = service.RouteServerNetworkFeature(feature)
    result = serializer.data

    assert result, "Serializer should produce data"

    # Check embeddings
    assert isinstance(result["ips"], list)


@pytest.mark.django_db
def test_ixprouter_feature_serializer():
    """Test IXPRouter feature serialization"""
    ip4 = baker.make("ipam.IpAddress", version=4)
    feature = baker.make("service.IXPRouterNetworkFeature",
                         ip_addresses=[ip4])
    serializer = service.IXPRouterNetworkFeature(feature)
    result = serializer.data

    assert result, "Serializer should produce data"

    # Check embeddings
    assert isinstance(result["ips"], list)


@pytest.mark.django_db
def test_polymorphic_feature_serializer():
    """Test polymorphic feature serialization"""
    features = [
        baker.make("service.BlackholingNetworkFeature"),
        baker.make("service.IXPRouterNetworkFeature"),
        baker.make("service.RouteServerNetworkFeature"),
    ]

    serializer = service.NetworkFeature(features, many=True)
    result = serializer.data

    assert result, "Serializer should serialize without crashing"
    assert len(result) == len(features), \
        "Serializer should serialize all features"


@pytest.mark.django_db
def test_blacklist_member_joining_rule():
    """Test blacklist member joining rule serializer"""
    rule = baker.make("service.BlacklistMemberJoiningRule")
    serializer = service.BlacklistMemberJoiningRule(rule)

    assert serializer.data


@pytest.mark.django_db
def test_whitelist_member_joining_rule():
    """Test whitelist member joining rule serializer"""
    rule = baker.make("service.WhitelistMemberJoiningRule")
    serializer = service.BlacklistMemberJoiningRule(rule)

    assert serializer.data



@pytest.mark.django_db
def test_member_joining_rule():
    """Test polymorphic member joining rule"""
    rules = [
        baker.make("service.WhitelistMemberJoiningRule"),
        baker.make("service.BlacklistMemberJoiningRule"),
    ]

    serializer = service.MemberJoiningRule(rules, many=True)

    assert serializer.data

