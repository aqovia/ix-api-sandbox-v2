
"""
Test NetworkService views
"""

import pytest
from model_bakery import baker

from jea.api.v2.tests import authorized_requests
from jea.api.v2.service import views


@pytest.mark.django_db
def test_member_joining_rules_view_set__list():
    """List all member joining rules"""
    account = baker.make("crm.Account")
    network_service = baker.make("service.ELanNetworkService")
    rule = baker.make(
        "service.WhitelistMemberJoiningRule",
        network_service=network_service,
        scoping_account=account)

    view = views.MemberJoiningRulesViewSet.as_view({"get": "list"})
    request = authorized_requests.get(account)
    response = view(request)

    assert response.status_code == 200, response.__dict__


@pytest.mark.django_db
def test_member_joining_rules_view_set__retrieve():
    """Get a specific member joining rule"""
    account = baker.make("crm.Account")
    network_service = baker.make("service.ELanNetworkService")
    rule = baker.make(
        "service.WhitelistMemberJoiningRule",
        network_service=network_service,
        scoping_account=account)

    view = views.MemberJoiningRulesViewSet.as_view({"get": "retrieve"})
    request = authorized_requests.get(account)
    response = view(request, pk=rule.pk)

    assert response.status_code == 200, response.__dict__


@pytest.mark.django_db
def test_member_joining_rules_view_set__create_whitelist():
    """Create a member joining rule"""
    account = baker.make("crm.Account")
    network_service = baker.make(
        "service.ELanNetworkService",
        managing_account=account,
        consuming_account=account,
        scoping_account=account)

    view = views.MemberJoiningRulesViewSet.as_view({"post": "create"})
    request = authorized_requests.post(account, {
        "type": "whitelist",
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "network_service": network_service.pk,
        "capacity_min": 42,
        "capacity_max": 9000,
        "external_ref": "join foo",
    })
    response = view(request)
    assert response.status_code == 201


@pytest.mark.django_db
def test_member_joining_rules_view_set__create_blacklist():
    """Create a member joining rule"""
    account = baker.make("crm.Account")
    network_service = baker.make(
        "service.ELanNetworkService",
        managing_account=account,
        consuming_account=account,
        scoping_account=account)

    view = views.MemberJoiningRulesViewSet.as_view({"post": "create"})
    request = authorized_requests.post(account, {
        "type": "blacklist",
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "network_service": network_service.pk,
        "external_ref": "join foo",
    })
    response = view(request)
    assert response.status_code == 201


@pytest.mark.django_db
def test_member_joining_rules_view_set__update():
    """Update a member joining rule"""
    account = baker.make("crm.Account")
    network_service = baker.make("service.ELanNetworkService")
    rule = baker.make(
        "service.WhitelistMemberJoiningRule",
        network_service=network_service,
        managing_account=account,
        consuming_account=account,
        scoping_account=account)

    view = views.MemberJoiningRulesViewSet.as_view({"put": "update"})
    request = authorized_requests.put(account, {
        "type": "whitelist",
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "capacity_min": 42,
        "capacity_max": 9000,
        "external_ref": "join foo",
    })
    response = view(request, pk=rule.pk)
    assert response.status_code == 200, response.__dict__


@pytest.mark.django_db
def test_member_joining_rules_view_set__partial_update():
    """Update some fields of a member joining rule"""
    account = baker.make("crm.Account")
    network_service = baker.make("service.ELanNetworkService")
    rule = baker.make(
        "service.WhitelistMemberJoiningRule",
        network_service=network_service,
        managing_account=account,
        consuming_account=account,
        scoping_account=account)

    view = views.MemberJoiningRulesViewSet.as_view({"patch": "partial_update"})
    request = authorized_requests.patch(account, {
        "capacity_min": 42,
        "capacity_max": 9000,
    })
    response = view(request, pk=rule.pk)
    assert response.status_code == 200, response.__dict__


@pytest.mark.django_db
def test_member_joining_rules_view_set__delete():
    """Remove a member joining rule"""
    account = baker.make("crm.Account")
    network_service = baker.make("service.ELanNetworkService")
    rule = baker.make(
        "service.BlacklistMemberJoiningRule",
        network_service=network_service,
        managing_account=account,
        consuming_account=account,
        scoping_account=account)

    view = views.MemberJoiningRulesViewSet.as_view({"delete": "destroy"})
    request = authorized_requests.delete(account)
    response = view(request, pk=rule.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_network_service_view_set__list():
    """List all the network services"""
    account = baker.make("crm.Account")
    network_services = [
        baker.make("service.ExchangeLanNetworkService"),
        baker.make("service.ELineNetworkService"),
        baker.make("service.ETreeNetworkService"),
        baker.make("service.ELanNetworkService"),
    ]
    view = views.NetworkServicesViewSet.as_view({"get": "list"})
    request = authorized_requests.get(account)
    response = view(request)

    assert response.status_code == 200


@pytest.mark.django_db
def test_network_service_view_set__retrieve():
    """Get a specific network service"""
    account = baker.make("crm.Account")
    service = baker.make("service.ExchangeLanNetworkService")
    view = views.NetworkServicesViewSet.as_view({"get": "retrieve"})
    request = authorized_requests.get(account)
    response = view(request, pk=service.pk)

    assert response.status_code == 200


@pytest.mark.django_db
def test_network_service_view_set__create_eline():
    """Create a network service"""
    account = baker.make("crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    joining_account = baker.make("crm.Account",
        scoping_account=account)
    product = baker.make("catalog.ELineNetworkProduct")

    view = views.NetworkServicesViewSet.as_view({"post": "create"})
    request = authorized_requests.post(account, {
        "type": "e_line",
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
        "joining_member_account": joining_account.pk,
        "name": "My ELine",
        "capacity": 1000,
        "product": product.pk,
    })
    response = view(request)
    assert response.status_code == 201, response.__dict__


@pytest.mark.django_db
def test_network_service_view_set__update_eline():
    """Update an eline network service"""
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    joining_account = baker.make(
        "crm.Account",
        scoping_account=account)
    new_joining_account = baker.make(
        "crm.Account",
        scoping_account=account)
    product = baker.make("catalog.ELineNetworkProduct")
    new_product = baker.make("catalog.ELineNetworkProduct")
    service = baker.make(
        "service.ELineNetworkService",
        product=product,
        managing_account=account,
        consuming_account=account,
        joining_member_account=joining_account)

    # Make update
    view = views.NetworkServicesViewSet.as_view({"put": "update"})
    request = authorized_requests.put(account, {
        "type": "e_line",
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
        "joining_member_account": new_joining_account.pk,
        "name": "My New ELine",
        "capacity": 2000,
        "product": new_product.pk,
    })
    response = view(request, pk=service.pk)
    assert response.status_code == 200, response.__dict__


@pytest.mark.django_db
def test_network_service_view_set__partial_update_eline():
    """Partially update an eline network service"""
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    joining_account = baker.make(
        "crm.Account",
        scoping_account=account)
    new_joining_account = baker.make(
        "crm.Account",
        scoping_account=account)
    product = baker.make("catalog.ELineNetworkProduct")
    new_product = baker.make("catalog.ELineNetworkProduct")
    service = baker.make(
        "service.ELineNetworkService",
        product=product,
        managing_account=account,
        consuming_account=account,
        joining_member_account=joining_account)

    # Make update
    view = views.NetworkServicesViewSet.as_view({"patch": "partial_update"})
    request = authorized_requests.patch(account, {
        "joining_member_account": new_joining_account.pk,
        "name": "My New ELine",
        "product": new_product.pk,
    })
    response = view(request, pk=service.pk)
    assert response.status_code == 200, response.__dict__


@pytest.mark.django_db
def test_network_service_view_set__create_elan():
    """Create a network service"""
    account = baker.make("crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    product = baker.make("catalog.ELanNetworkProduct")

    view = views.NetworkServicesViewSet.as_view({"post": "create"})
    request = authorized_requests.post(account, {
        "type": "e_lan",
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
        "name": "My ELan",
        "product": product.pk,
        "initiator_capacity": 10000,
        "default_capacity_min": None,
        "default_capacity_max": None,
    })
    response = view(request)
    assert response.status_code == 201, response.__dict__


@pytest.mark.django_db
def test_network_service_view_set__update_elan():
    """Update an ELan network service"""
    account = baker.make("crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    product = baker.make("catalog.ELanNetworkProduct")
    new_product = baker.make("catalog.ELanNetworkProduct")

    service = baker.make(
        "service.ELanNetworkService",
        managing_account=account,
        consuming_account=account,
        billing_account=account,
        product=product)

    # Make update
    view = views.NetworkServicesViewSet.as_view({"put": "update"})
    request = authorized_requests.put(account, {
        "type": "e_lan",
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
        "name": "My New ELan",
        "product": new_product.pk,
        "initiator_capacity": 10000,
        "default_capacity_min": 500,
        "default_capacity_max": 2000,
    })
    response = view(request, pk=service.pk)
    assert response.status_code == 200, response.__dict__


@pytest.mark.django_db
def test_network_service_view_set__partial_update_elan():
    """Partially update an ELan network service"""
    account = baker.make("crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    product = baker.make("catalog.ELanNetworkProduct")
    new_product = baker.make("catalog.ELanNetworkProduct")

    service = baker.make(
        "service.ELanNetworkService",
        managing_account=account,
        consuming_account=account,
        billing_account=account,
        product=product)

    # Make update
    view = views.NetworkServicesViewSet.as_view({"patch": "partial_update"})
    request = authorized_requests.patch(account, {
        "name": "My New ELan",
        "product": new_product.pk,
        "default_capacity_max": 2000,
    })
    response = view(request, pk=service.pk)
    assert response.status_code == 200, response.__dict__


@pytest.mark.django_db
def test_network_service_view_set__create_etree():
    """Create a network service"""
    account = baker.make("crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    product = baker.make("catalog.ETreeNetworkProduct")

    view = views.NetworkServicesViewSet.as_view({"post": "create"})
    request = authorized_requests.post(account, {
        "type": "e_tree",
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
        "name": "My ELan",
        "product": product.pk,
        "initiator_capacity": 10000,
        "default_capacity_min": 1000,
        "default_capacity_max": 9999,
    })
    response = view(request)
    assert response.status_code == 201, response.__dict__


@pytest.mark.django_db
def test_network_service_view_set__update_etree():
    """Update an ETree network service"""
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    product = baker.make("catalog.ETreeNetworkProduct")

    service = baker.make(
        "service.ETreeNetworkService",
        managing_account=account,
        consuming_account=account,
        billing_account=account,
        product=product)

    view = views.NetworkServicesViewSet.as_view({"put": "update"})
    request = authorized_requests.put(account, {
        "type": "e_tree",
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
        "name": "My New ETree",
        "product": product.pk,
        "initiator_capacity": 10000,
        "default_capacity_min": 1000,
        "default_capacity_max": 9999,
    })
    response = view(request, pk=service.pk)
    assert response.status_code == 200, response.__dict__


@pytest.mark.django_db
def test_network_service_view_set__partial_update_etree():
    """Partially update an ETree network service"""
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    product = baker.make("catalog.ETreeNetworkProduct")
    new_product = baker.make("catalog.ETreeNetworkProduct")

    service = baker.make(
        "service.ETreeNetworkService",
        managing_account=account,
        consuming_account=account,
        billing_account=account,
        product=product)

    view = views.NetworkServicesViewSet.as_view({"patch": "partial_update"})
    request = authorized_requests.patch(account, {
        "consuming_account": account.pk,
        "name": "My New ETree",
        "product": new_product.pk,
        "default_capacity_max": 9999,
    })
    response = view(request, pk=service.pk)
    assert response.status_code == 200, response.__dict__


@pytest.mark.django_db
def test_network_service_view_set__destroy():
    """Destroy a network service"""
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    service = baker.make(
        "service.ELineNetworkService",
        managing_account=account,
        billing_account=account,
        consuming_account=account,
        scoping_account=account)

    view = views.NetworkServicesViewSet.as_view({"delete": "destroy"})
    request = authorized_requests.delete(account)

    response = view(request, pk=service.pk)
    assert response.status_code == 200, response.__dict__


@pytest.mark.django_db
def test_network_features_view_set__list():
    """List all the network features"""
    account = baker.make("crm.Account")
    view = views.NetworkFeaturesViewSet.as_view({"get": "list"})
    request = authorized_requests.get(account)
    response = view(request)

    assert response.status_code == 200


@pytest.mark.django_db
def test_network_features_view_set__retrieve():
    """A a specific of all the network features"""
    account = baker.make("crm.Account")
    feature = baker.make("service.RouteServerNetworkFeature")
    view = views.NetworkFeaturesViewSet.as_view({"get": "retrieve"})
    request = authorized_requests.get(account)
    response = view(request, pk=feature.pk)

    assert response.status_code == 200

