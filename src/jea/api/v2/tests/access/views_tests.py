
"""
Test JEA Access Views
"""

import pytest
import mock
from model_bakery import baker
from ixapi_schema.v2.constants.access import VLanType

from jea.crm.services import contacts as contacts_svc
from jea.api.v2.tests import authorized_requests
from jea.api.v2.access import views
from jea.access import models as access_models
from jea.catalog import models as catalog_models

#
# Demarcs
#
@pytest.mark.django_db
def test_demarcs_view_set__list():
    """List all demarcs"""
    account = baker.make("crm.Account")

    baker.make(
        "access.DemarcationPoint",
        scoping_account=account,
        managing_account=account,
        consuming_account=account)
    baker.make(
        "access.DemarcationPoint",
        scoping_account=account,
        managing_account=account,
        consuming_account=account)


    # Create some dmarcs
    view = views.DemarcsViewSet.as_view({"get": "list"})
    request = authorized_requests.get(account)

    response = view(request)
    assert response.status_code == 200


@pytest.mark.django_db
def test_demarcs_view_set__retrieve():
    """Get a specific demarc"""
    view = views.DemarcsViewSet.as_view({"get": "retrieve"})
    account = baker.make(
        "crm.Account")
    demarc = baker.make(
        "access.DemarcationPoint",
        scoping_account=account)
    request = authorized_requests.get(account)
    response = view(request, pk=demarc.pk)
    assert response.status_code == 200


@mock.patch("jea.catalog.services.pops.get_media_type_availability")
@pytest.mark.django_db
def test_demarcs_view_set__create(get_availablity):
    """Create a demarc"""
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    impl_contact = baker.make(
        "crm.Contact",
        roles=[
            contacts_svc.get_default_role("implementation"),
        ],
        scoping_account=account)
    pop = baker.make(
        "catalog.PointOfPresence")

    get_availablity.return_value = 42

    # Make request
    request = authorized_requests.post(account, {
        "media_type": catalog_models.MediaType.TYPE_10GBASE_LR.value,
        "contacts": [impl_contact.pk],
        "pop": pop.pk,
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
    })

    view = views.DemarcsViewSet.as_view({"post": "create"})
    response = view(request)

    assert response.status_code == 201, response.__dict__


@mock.patch("jea.catalog.services.pops.get_media_type_availability")
@pytest.mark.django_db
def test_demarcs_view_set__create_with_connection(
        get_availablity,
    ):
    """Create a demarc with a given connection"""
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    pop = baker.make(
        "catalog.PointOfPresence")
    impl_contact = baker.make(
        "crm.Contact",
        roles=[
            contacts_svc.get_default_role("implementation"),
        ],
        scoping_account=account)
    connection = baker.make(
        "access.Connection",
        scoping_account=account)

    get_availablity.return_value = 42

    assert not connection.demarcation_points.all(), \
        "There should not be an assigned demarc"

    # Make request
    request = authorized_requests.post(account, {
        "media_type": catalog_models.MediaType.TYPE_10GBASE_LR.value,
        "contacts": [impl_contact.pk],
        "pop": pop.pk,
        "connection": connection.pk,
        "billing_account": account.pk,
        "managing_account": account.pk,
        "consuming_account": account.pk,
    })

    view = views.DemarcsViewSet.as_view({"post": "create"})
    response = view(request)

    assert response.status_code == 201, response.__dict__

    # Check result
    connection.refresh_from_db()
    assert connection.demarcation_points.all(), \
        "There should be an assigned demarc"


@pytest.mark.django_db
def test_demarcs_view_set__update():
    """Test updating a demarc"""
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    demarc = baker.make(
        "access.DemarcationPoint",
        scoping_account=account)
    connection = baker.make(
        "access.Connection",
        scoping_account=account)
    impl_contact = baker.make(
        "crm.Contact",
        roles=[baker.make("crm.Role", name="implementation")],
        scoping_account=account)

    # Make update
    request = authorized_requests.put(account, {
        "contacts": [impl_contact.pk],
        "connection": connection.pk,
        "billing_account": account.pk,
        "managing_account": account.pk,
        "consuming_account": account.pk,
    })

    view = views.DemarcsViewSet.as_view({"put": "update"})
    response = view(request, pk=demarc.pk)

    assert response.status_code == 200


@pytest.mark.django_db
def test_demarcs_view_set__partial_update():
    """Test updating a demarc"""
    account = baker.make(
        "crm.Account")
    demarc = baker.make(
        "access.DemarcationPoint",
        scoping_account=account)
    connection = baker.make(
        "access.Connection",
        scoping_account=account)

    # Make partial update
    request = authorized_requests.patch(account, {
        "connection": connection.pk,
    })

    view = views.DemarcsViewSet.as_view({"patch": "partial_update"})
    response = view(request, pk=demarc.pk)

    assert response.status_code == 200


@pytest.mark.django_db
def test_demarcs_view_set__destroy():
    """Test demarc deallocation"""
    account = baker.make(
        "crm.Account")
    demarc = baker.make(
        "access.DemarcationPoint",
        scoping_account=account)

    # Make request
    request = authorized_requests.delete(account)
    view = views.DemarcsViewSet.as_view({"delete": "destroy"})
    response = view(request, pk=demarc.pk)

    assert response.status_code == 200

#
# Connections
#

@pytest.mark.django_db
def test_connections_view_set__list():
    """Test listing connections"""
    account = baker.make(
        "crm.Account")
    # Have some connections to list
    baker.make(
        "access.Connection",
        scoping_account=account,
        managing_account=account,
        consuming_account=account,
        billing_account=account)
    baker.make(
        "access.Connection",
        scoping_account=account,
        managing_account=account,
        consuming_account=account,
        billing_account=account)

    request = authorized_requests.get(account)
    view = views.ConnectionsViewSet.as_view({"get": "list"})
    response = view(request)

    assert response.status_code == 200


@pytest.mark.django_db
def test_connections_view_set__retrieve():
    """Test listing connections"""
    account = baker.make(
        "crm.Account")
    connection = baker.make(
        "access.Connection",
        scoping_account=account)
    request = authorized_requests.get(account)
    view = views.ConnectionsViewSet.as_view({"get": "retrieve"})
    response = view(request, pk=connection.pk)

    assert response.status_code == 200


@pytest.mark.django_db
def test_connections_view_set__create():
    """Test connection create"""
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    impl_contact = baker.make(
        "crm.Contact",
        roles=[
            contacts_svc.get_default_role("implementation"),
        ],
        scoping_account=account,
        consuming_account=account)

    request = authorized_requests.post(account, {
        "mode": "lag_lacp",
        "speed": 2500,
        "contacts": [impl_contact.pk],
        "billing_account": account.pk,
        "managing_account": account.pk,
        "consuming_account": account.pk,
    })
    view = views.ConnectionsViewSet.as_view({"post": "create"})
    response = view(request)
    assert response.status_code == 201


@pytest.mark.django_db
def test_connections_view_set__update():
    """Test updating a connection"""
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    impl_contact = baker.make(
        "crm.Contact",
        roles=[
            contacts_svc.get_default_role("implementation"),
        ],
        scoping_account=account)
    connection = baker.make(
        "access.Connection",
        scoping_account=account)

    request = authorized_requests.put(account, {
        "mode": "lag_lacp",
        "lacp_timeout": "slow",
        "speed": 5000,
        "contacts": [impl_contact.pk],
        "billing_account": account.pk,
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "external_ref": "Test2048",
    })
    view = views.ConnectionsViewSet.as_view({"put": "update"})
    response = view(request, pk=connection.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_connections_view_set__partial_update():
    """Test partially updating a connection"""
    account = baker.make(
        "crm.Account")
    connection = baker.make(
        "access.Connection",
        scoping_account=account)

    request = authorized_requests.patch(account, {
        "lacp_timeout": "fast",
    })
    view = views.ConnectionsViewSet.as_view({"patch": "partial_update"})
    response = view(request, pk=connection.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_connections_view_set__destroy():
    """Destroy a connection"""
    account = baker.make(
        "crm.Account")
    connection = baker.make(
        "access.Connection",
        managing_account=account,
        consuming_account=account,
        scoping_account=account)

    request = authorized_requests.delete(account)
    view = views.ConnectionsViewSet.as_view({"delete": "destroy"})
    response = view(request, pk=connection.pk)
    assert response.status_code == 200


#
# Configurations:
# NetworkServiceConfig
#
@pytest.mark.django_db
def test_network_service_configs_view_set__list():
    """List all the network service configs"""
    account = baker.make("crm.Account")
    # Have some network service configs to list
    baker.make(
        "access.ExchangeLanNetworkServiceConfig",
        scoping_account=account,
        managing_account=account,
        billing_account=account,
        consuming_account=account)
    baker.make(
        "access.ELineNetworkServiceConfig",
        scoping_account=account,
        managing_account=account,
        billing_account=account,
        consuming_account=account)
    baker.make(
        "access.ELanNetworkServiceConfig",
        scoping_account=account,
        managing_account=account,
        billing_account=account,
        consuming_account=account)
    baker.make(
        "access.ETreeNetworkServiceConfig",
        scoping_account=account,
        managing_account=account,
        billing_account=account,
        consuming_account=account)

    request = authorized_requests.get(account)
    view = views.NetworkServiceConfigsViewSet.as_view({"get": "list"})
    response = view(request)

    assert response.status_code == 200


@pytest.mark.django_db
def test_network_service_configs_view_set__retrieve():
    """Get a specific network service config"""
    account = baker.make(
        "crm.Account")
    config = baker.make(
        "access.ExchangeLanNetworkServiceConfig",
        scoping_account=account)

    request = authorized_requests.get(account)
    view = views.NetworkServiceConfigsViewSet.as_view({"get": "retrieve"})
    response = view(request, pk=config.pk)
    assert response.status_code == 200

    # Eline
    config = baker.make(
        "access.ELineNetworkServiceConfig",
        scoping_account=account)

    request = authorized_requests.get(account)
    view = views.NetworkServiceConfigsViewSet.as_view({"get": "retrieve"})
    response = view(request, pk=config.pk)
    assert response.status_code == 200

    # ELan
    config = baker.make(
        "access.ELanNetworkServiceConfig",
        scoping_account=account)

    request = authorized_requests.get(account)
    view = views.NetworkServiceConfigsViewSet.as_view({"get": "retrieve"})
    response = view(request, pk=config.pk)
    assert response.status_code == 200

    # ETree
    config = baker.make(
        "access.ETreeNetworkServiceConfig",
        scoping_account=account)

    request = authorized_requests.get(account)
    view = views.NetworkServiceConfigsViewSet.as_view({"get": "retrieve"})
    response = view(request, pk=config.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_network_service_configs_view_set__create():
    """Create a network service config"""
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    contact = baker.make(
        "crm.Contact",
        roles=[
            contacts_svc.get_default_role("implementation"),
        ],
        scoping_account=account)
    connection = baker.make(
        "access.Connection",
        scoping_account=account)
    net_addr = baker.make(
        "ipam.IpAddress",
        address="10.23.42.0",
        version=4,
        prefix_length=24)
    network_service = baker.make(
        "service.ExchangeLanNetworkService",
        ip_addresses=[net_addr],
        nsc_required_contact_roles=[
            contacts_svc.get_default_role("implementation"),
        ])

    # Make a create request
    view = views.NetworkServiceConfigsViewSet.as_view({"post": "create"})
    request = authorized_requests.post(account, {
        "type": "exchange_lan",
        "connection": connection.pk,
        "network_service": network_service.pk,
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
        "contacts": [contact.pk],
        "listed": True,
        "vlan_config": {
            "vlan_type": "qinq",
            "vlans": [23, [42, "account"]],
            "outer_vlan": ["0x8100", 1000],
        }
    })
    response = view(request)
    assert response.status_code == 201


@pytest.mark.django_db
def test_network_service_configs_view_set__update():
    """Update a network service config"""
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    contact = baker.make(
        "crm.Contact",
        roles=[
            contacts_svc.get_default_role("implementation"),
        ],
        scoping_account=account)
    connection = baker.make(
        "access.Connection",
        scoping_account=account)
    service = baker.make(
        "service.ExchangeLanNetworkService",
        nsc_required_contact_roles=[
            contacts_svc.get_default_role("implementation"),
        ])
    vlan_config = baker.make(
        "access.VLanConfig", vlan_type=VLanType.PORT)
    config = baker.make(
        "access.ExchangeLanNetworkServiceConfig",
        vlan_config=vlan_config,
        network_service=service,
        scoping_account=account)
    mac_addr = baker.make(
        "ipam.MacAddress",
        scoping_account=account)

    view = views.NetworkServiceConfigsViewSet.as_view({"put": "update"})
    request = authorized_requests.put(account, {
        "macs": [mac_addr.pk],
        "connection": connection.pk,
        "network_service": config.network_service.pk,
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
        "contacts": [contact.pk],
        "listed": True,
        "vlan_config": {
            "vlan_type": "dot1q",
            "vlans": [23, [42, "account"]],
        }
    })
    response = view(request, pk=config.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_network_service_configs_view_set__partial_update():
    """Partially update a network service config"""
    account = baker.make(
        "crm.Account")
    billing_account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"),
        scoping_account=account)
    contact = baker.make(
        "crm.Contact",
        roles=[
            contacts_svc.get_default_role("implementation"),
        ],
        scoping_account=account)
    service = baker.make(
        "service.ExchangeLanNetworkService",
        nsc_required_contact_roles=[
            contacts_svc.get_default_role("implementation"),
        ])
    config = baker.make(
        "access.ExchangeLanNetworkServiceConfig",
        scoping_account=account,
        network_service=service)

    view = views.NetworkServiceConfigsViewSet.as_view({
        "patch": "partial_update",
    })
    request = authorized_requests.patch(account, {
        "contacts": [contact.pk],
        "billing_account": billing_account.pk,
    })
    response = view(request, pk=config.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_network_service_configs_view_set__destroy():
    """Delete a network service config"""
    account = baker.make(
        "crm.Account")
    config = baker.make(
        "access.ExchangeLanNetworkServiceConfig",
        scoping_account=account)

    view = views.NetworkServiceConfigsViewSet.as_view({"delete": "destroy"})
    request = authorized_requests.delete(account)
    response = view(request, pk=config.pk)
    assert response.status_code == 200

#
# Network Feature Configs
#
@pytest.mark.django_db
def test_network_feature_configs_view_set__list():
    """Test list in feature config view set"""
    account = baker.make("crm.Account")
    baker.make(
        "access.RouteServerNetworkFeatureConfig",
        scoping_account=account,
        managing_account=account,
        billing_account=account,
        consuming_account=account)
    view = views.NetworkFeatureConfigsViewSet.as_view({"get": "list"})
    request = authorized_requests.get(account)
    response = view(request)
    assert response.status_code == 200


@pytest.mark.django_db
def test_network_feature_configs_view_set__retrieve():
    """Test getting a feature config"""
    account = baker.make(
        "crm.Account")
    config = baker.make(
        "access.RouteServerNetworkFeatureConfig",
        scoping_account=account,
        managing_account=account,
        billing_account=account,
        consuming_account=account)
    view = views.NetworkFeatureConfigsViewSet.as_view({"get": "retrieve"})
    request = authorized_requests.get(account)
    response = view(request, pk=config.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_network_feature_configs_view_set__create():
    """Test creating a network service feature"""
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    network_service = baker.make(
        "service.ExchangeLanNetworkService")
    network_feature = baker.make(
        "service.RouteServerNetworkFeature",
        network_service=network_service,
        address_families=['af_inet'],
        nfc_required_contact_roles=[],
        available_bgp_session_types=[
            access_models.BGPSessionType.TYPE_PASSIVE.value,
        ])
    network_service_config = baker.make(
        "access.ExchangeLanNetworkServiceConfig",
        scoping_account=account,
        network_service=network_service)

    # Create request
    view = views.NetworkFeatureConfigsViewSet.as_view({"post": "create"})
    request = authorized_requests.post(account, {
        "type": "route_server",
        "network_feature": network_feature.pk,
        "network_service_config": network_service_config.pk,
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
        "asn": 23042,
        "as_set_v4": "AS-FOO",
        "session_mode": network_feature.session_mode.value,
        "bgp_session_type":
            access_models.BGPSessionType.TYPE_PASSIVE.value,
        "contacts": [],
    })
    response = view(request)
    assert response.status_code == 201

@pytest.mark.django_db
def test_network_feature_configs_view_set__update():
    """Update a network feature config"""
    account = baker.make(
        "crm.Account")
    network_service = baker.make(
        "service.ExchangeLanNetworkService")
    network_feature = baker.make(
        "service.RouteServerNetworkFeature",
        address_families=['af_inet'],
        network_service=network_service)
    network_service_config = baker.make(
        "access.ExchangeLanNetworkServiceConfig",
        scoping_account=account,
        network_service=network_service)
    network_feature_config = baker.make(
        "access.RouteServerNetworkFeatureConfig",
        scoping_account=account,
        network_feature=network_feature,
        network_service_config=network_service_config)

    # Create request
    view = views.NetworkFeatureConfigsViewSet.as_view({"put": "update"})
    request = authorized_requests.put(account, {
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "asn": 23042,
        "as_set_v4": "AS-FOO",
        "session_mode": network_feature.session_mode.value,
        "bgp_session_type":
            access_models.BGPSessionType.TYPE_ACTIVE.value,
    })
    response = view(request, pk=network_feature_config.pk)

    assert response.status_code == 200


@pytest.mark.django_db
def test_network_feature_configs_view_set__partial_update():
    """Update a network feature config with partial changes"""
    account = baker.make(
        "crm.Account")
    network_service = baker.make(
        "service.ExchangeLanNetworkService")
    network_feature = baker.make(
        "service.RouteServerNetworkFeature",
        network_service=network_service,
        address_families=['af_inet6'])
    network_service_config = baker.make(
        "access.ExchangeLanNetworkServiceConfig",
        scoping_account=account,
        network_service=network_service)
    network_feature_config = baker.make(
        "access.RouteServerNetworkFeatureConfig",
        scoping_account=account,
        as_set_v6="AS-BAR",
        network_feature=network_feature,
        network_service_config=network_service_config)

    # Create request
    view = views.NetworkFeatureConfigsViewSet.as_view({
        "patch": "partial_update",
    })
    request = authorized_requests.patch(account, {
        "asn": 23042,
        "as_set_v6": "AS-FOO",
    })
    response = view(request, pk=network_feature_config.pk)

    assert response.status_code == 200


@pytest.mark.django_db
def test_network_feature_configs_view_set__destroy():
    """Remove a network feature config"""
    account = baker.make("crm.Account")
    network_feature_config = baker.make(
        "access.RouteServerNetworkFeatureConfig",
        scoping_account=account)

    view = views.NetworkFeatureConfigsViewSet.as_view({"delete": "destroy"})
    request = authorized_requests.delete(account)
    response = view(request, pk=network_feature_config.pk)

    assert response.status_code == 200
