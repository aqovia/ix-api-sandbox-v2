
"""
Test IX-API Access Entities
"""

import pytest
from model_bakery import baker
from ixapi_schema.v2.entities import access
from ixapi_schema.v2.constants.access import VLanType

from jea.api.v2.access.serializers import (
    get_config_update_serializer,
)


@pytest.mark.django_db
def test_connection_serialization():
    """Test serialization of a connection"""
    conn = baker.make("access.Connection")
    demarc = baker.make("access.DemarcationPoint", connection=conn)

    serializer = access.Connection(conn)
    data = serializer.data

    assert data, "Serializer should serialize without crashing"
    assert str(demarc.id) in data["demarcs"]


@pytest.mark.django_db
def test_connection_request_serializer():
    """Test create connection request serializer"""
    account = baker.make("crm.Account")
    contact = baker.make("crm.Contact")

    payload = {
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
        "mode": "lag_lacp",
        "lacp_timeout": "slow",
        "contacts": [contact.pk],
    }

    serializer = access.ConnectionRequest(data=payload)
    serializer.is_valid()
    assert not serializer.errors


@pytest.mark.django_db
def test_connection_update_serializer():
    """Test create connection request serializer"""
    account = baker.make("crm.Account")
    contact = baker.make("crm.Contact")

    payload = {
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
        "mode": "lag_static",
        "contacts": [contact.pk],
    }

    serializer = access.ConnectionUpdate(data=payload)
    serializer.is_valid()
    assert not serializer.errors


@pytest.mark.django_db
def test_polymorphic_network_service_config_serializer():
    """Test Polymorphic network service config serializer"""
    config = baker.make("access.ExchangeLanNetworkServiceConfig")
    serializer = access.NetworkServiceConfig(config)

    assert serializer.data
    assert serializer.data["type"] == "exchange_lan"

    config = baker.make("access.ELineNetworkServiceConfig",
        vlan_config=baker.make(
            "access.VLanConfig",
            vlan_type=VLanType.PORT))
    serializer = access.NetworkServiceConfig(config)

    assert serializer.data
    assert serializer.data["type"] == "e_line"

    config = baker.make("access.ELanNetworkServiceConfig",
        vlan_config=baker.make(
            "access.VLanConfig",
            vlan_type=VLanType.PORT))
    serializer = access.NetworkServiceConfig(config)

    assert serializer.data
    assert serializer.data["type"] == "e_lan"

    config = baker.make("access.ETreeNetworkServiceConfig",
        vlan_config=baker.make(
            "access.VLanConfig",
            vlan_type=VLanType.PORT))
    serializer = access.NetworkServiceConfig(config)

    assert serializer.data
    assert serializer.data["type"] == "e_tree"


@pytest.mark.django_db
def test_polymorphic_network_service_config_deserialization():
    """Test deserialization of network service input"""
    account = baker.make("crm.Account")
    conn = baker.make("access.Connection")
    service = baker.make("service.ExchangeLanNetworkService")
    contact = baker.make("crm.Contact")
    mac = baker.make("ipam.MacAddress")

    data = {
        "type": "exchange_lan",
        "capacity": None,
        "contacts": [contact.pk],
        "connection": conn.pk,
        "network_service": service.pk,
        "asns": [2342],
        "macs": [mac.pk],
        "listed": True,
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
        "vlan_config": {"vlan_type": "dot1q", "vlans": [23, [42, "named"]]},
    }

    serializer = access.NetworkServiceConfigRequest(data=data)
    serializer.is_valid()
    assert not serializer.errors
    assert serializer.validated_data["type"] == "exchange_lan"

    data = {
        "type": "exchange_lan",
        "capacity": 1,
        "contacts": [contact.pk],
        "asns": [2342],
        "vlan_config": {"vlan_type": "port"},
        "connection": "",
        "listed": True,
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
    }

    serializer = access.NetworkServiceConfigRequest(data=data)
    assert not serializer.is_valid()


@pytest.mark.django_db
def test_demarcation_point_serialization():
    """Test serialization of a port demarc"""
    demarc = baker.make("access.DemarcationPoint")
    serializer = access.DemarcationPoint(demarc)

    data = serializer.data
    assert data
    assert isinstance(data["contacts"], list)


@pytest.mark.django_db
def test_demarcation_point_request_serializer():
    """Test demarcation point request serializer"""
    connection = baker.make("access.Connection")
    account = baker.make("crm.Account")
    contact = baker.make("crm.Contact")
    pop = baker.make("catalog.PointOfPresence")

    payload = {
        "pop": pop.pk,
        "media_type": "100GBASE-LR",
        "connection": connection.pk,
        "contacts": [contact.pk],
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
    }

    serializer = access.DemarcationPointRequest(data=payload)
    assert serializer.is_valid()


@pytest.mark.django_db
def test_demarcation_point_update_serializer():
    """Test demarcation point update serializer"""
    connection = baker.make("access.Connection")
    account = baker.make("crm.Account")
    contact = baker.make("crm.Contact")

    payload = {
        "connection": connection.pk,
        "contacts": [contact.pk],
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
    }

    serializer = access.DemarcationPointUpdate(data=payload)
    assert serializer.is_valid()


@pytest.mark.django_db
def test_feature_config_serialization():
    """Test serialization of feature config objects"""
    service_config = baker.make("access.ExchangeLanNetworkServiceConfig")
    feature = baker.make("service.RouteServerNetworkFeature")
    feature_config = baker.make(
        "access.RouteServerNetworkFeatureConfig",
        network_service_config=service_config,
        network_feature=feature)
    serializer = access.NetworkFeatureConfig(feature_config)
    assert serializer.data, "Serializer should serialize without crashing"
    assert serializer.data["type"] == "route_server"

    feature = baker.make("service.IXPRouterNetworkFeature")
    feature_config = baker.make(
        "access.IXPRouterNetworkFeatureConfig",
        network_service_config=service_config,
        network_feature=feature)
    serializer = access.NetworkFeatureConfig(feature_config)
    assert serializer.data, "Serializer should serialize without crashing"
    assert serializer.data["type"] == "ixp_router"

    feature = baker.make("service.BlackholingNetworkFeature")
    feature_config = baker.make(
        "access.BlackholingNetworkFeatureConfig",
        network_service_config=service_config,
        network_feature=feature)
    serializer = access.NetworkFeatureConfig(feature_config)
    assert serializer.data, "Serializer should serialize without crashing"
    assert serializer.data["type"] == "blackholing"


@pytest.mark.django_db
def test_network_service_config_udpate_base_serializer():
    """Test service config update base serializer"""
    account = baker.make("crm.Account")
    impl = baker.make("crm.Contact")
    connection = baker.make("access.Connection")

    serializer = access.NetworkServiceConfigUpdateBase(data={
        "contacts": [impl.pk],
        "connection": connection.pk,
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
        "vlan_config": {"vlan_type": "port"},
    })

    assert serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_exchange_lan_network_service_config_update_serializer():
    """Test update serializer for network service config"""
    account = baker.make("crm.Account")
    impl = baker.make("crm.Contact")
    connection = baker.make("access.Connection")
    mac = baker.make("ipam.MacAddress")

    serializer = access.ExchangeLanNetworkServiceConfigUpdate(data={
        "contacts": [impl.pk],
        "connection": connection.pk,
        "macs": [mac.pk],
        "asns": [42],
        "listed": False,
        "vlan_config": {
            "vlan_type": "port",
        },
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
    })

    assert serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_eline_network_service_config_request_serializer():
    """Test request serializer for network service config"""
    account = baker.make("crm.Account")
    impl = baker.make("crm.Contact")
    connection = baker.make("access.Connection")
    network_service = baker.make("service.ELineNetworkService")

    serializer = access.ELineNetworkServiceConfigRequest(data={
        "contacts": [impl.pk],
        "connection": connection.pk,
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
        "network_service": network_service.pk,
        "vlan_config": {
            "vlan_type": "qinq",
            "vlans": [23],
            "outer_vlan": ["0x8100", 42],
        },
    })

    assert serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_eline_network_service_config_update_serializer():
    """Test update serializer for network service config"""
    account = baker.make("crm.Account")
    impl = baker.make("crm.Contact")
    connection = baker.make("access.Connection")

    serializer = access.ELineNetworkServiceConfigUpdate(data={
        "contacts": [impl.pk],
        "connection": connection.pk,
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
        "vlan_config": {
            "vlan_type": "dot1q",
            "vlans": [23],
        },
    })

    assert serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_elan_network_service_config_request_serializer():
    """Test request serializer for network service config"""
    account = baker.make("crm.Account")
    impl = baker.make("crm.Contact")
    connection = baker.make("access.Connection")
    network_service = baker.make("service.ELanNetworkService")

    serializer = access.ELanNetworkServiceConfigRequest(data={
        "contacts": [impl.pk],
        "connection": connection.pk,
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
        "network_service": network_service.pk,
        "vlan_config": {
            "vlan_type": "dot1q",
            "vlans": [23],
        },
    })

    assert serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_elan_network_service_config_update_serializer():
    """Test update serializer for network service config"""
    account = baker.make("crm.Account")
    impl = baker.make("crm.Contact")
    connection = baker.make("access.Connection")

    serializer = access.ELanNetworkServiceConfigUpdate(data={
        "contacts": [impl.pk],
        "connection": connection.pk,
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
        "vlan_config": {
            "vlan_type": "dot1q",
            "vlans": [23],
        },
    })

    assert serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_etree_network_service_config_request_serializer():
    """Test request serializer for network service config"""
    account = baker.make("crm.Account")
    impl = baker.make("crm.Contact")
    connection = baker.make("access.Connection")
    network_service = baker.make("service.ETreeNetworkService")

    serializer = access.ETreeNetworkServiceConfigRequest(data={
        "contacts": [impl.pk],
        "connection": connection.pk,
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
        "network_service": network_service.pk,
        "vlan_config": {
            "vlan_type": "dot1q",
            "vlans": [23],
        },
    })

    assert serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_etree_network_service_config_update_serializer():
    """Test update serializer for network service config"""
    account = baker.make("crm.Account")
    impl = baker.make("crm.Contact")
    connection = baker.make("access.Connection")

    serializer = access.ETreeNetworkServiceConfigUpdate(data={
        "contacts": [impl.pk],
        "connection": connection.pk,
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
        "vlan_config": {
            "vlan_type": "dot1q",
            "vlans": [23],
        },
    })

    assert serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_blackholing_network_feature_config_update_serializer():
    """Test update serializer for feature config: blackholing"""
    account = baker.make("crm.Account")
    prefix = baker.make("ipam.IpAddress")
    serializer = access.BlackholingNetworkFeatureConfigUpdate(data={
        "purchase_order": "fooooooo",
        "filtered_prefixes": [prefix.pk],
        "ixp_specific_configuration": "DELETE FROM `peers` WHERE 1",
        "activated": True,
        "managing_account": account.pk,
        "billing_account": account.pk,
        "consuming_account": account.pk,
    })

    assert serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_route_server_network_feature_config_update_serializer():
    """Test update serializer for route server feature configs"""
    account = baker.make("crm.Account")
    serializer = access.RouteServerNetworkFeatureConfigUpdate(data={
        "asn": 2342,
        "as_set": "AS-FOO",
        "max_prefix_v4": 5000,
        "insert_ixp_asn": True,
        "session_mode": "public",
        "bgp_session_type": "active",
        "billing_account": account.pk,
        "managing_account": account.pk,
        "consuming_account": account.pk,
    })

    assert serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_ixp_router_network_feature_config_update_serializer():
    """Test update serializer for ixp router feature config"""
    account = baker.make("crm.Account")
    serializer = access.IXPRouterNetworkFeatureConfigUpdate(data={
        "max_prefix": 23,
        "bgp_session_type": "active",
        "managing_account": account.pk,
        "billing_account": account.pk,
        "consuming_account": account.pk,
    })

    assert serializer.is_valid(raise_exception=True)

