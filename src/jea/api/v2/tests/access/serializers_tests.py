
import pytest
from model_bakery import baker
from ixapi_schema.v2.entities import access

from jea.api.v2.access.serializers import get_config_update_serializer


@pytest.mark.django_db
def test_get_config_update_serializer():
    """Get serializer based on config type"""
    # Network Services
    #  - Exchange Lan
    config = baker.make("access.ExchangeLanNetworkServiceConfig")
    assert get_config_update_serializer(config) == \
        access.ExchangeLanNetworkServiceConfigUpdate

    #  - ELine
    config = baker.make("access.ELineNetworkServiceConfig")
    assert get_config_update_serializer(config) == \
        access.ELineNetworkServiceConfigUpdate

    #  - ELan
    config = baker.make("access.ELanNetworkServiceConfig")
    assert get_config_update_serializer(config) == \
        access.ELanNetworkServiceConfigUpdate

    #  - ETree
    config = baker.make("access.ETreeNetworkServiceConfig")
    assert get_config_update_serializer(config) == \
        access.ETreeNetworkServiceConfigUpdate

    # Network Features
    #  - Blackholing
    config = baker.make("access.BlackholingNetworkFeatureConfig")
    assert get_config_update_serializer(config) == \
        access.BlackholingNetworkFeatureConfigUpdate

    #  - Route Server
    config = baker.make("access.RouteServerNetworkFeatureConfig")
    assert get_config_update_serializer(config) == \
        access.RouteServerNetworkFeatureConfigUpdate

    #  - IXP Router
    config = baker.make("access.IXPRouterNetworkFeatureConfig")
    assert get_config_update_serializer(config) == \
        access.IXPRouterNetworkFeatureConfigUpdate

    with pytest.raises(TypeError):
        config = baker.make("access.NetworkServiceConfig")
        get_config_update_serializer(config)
