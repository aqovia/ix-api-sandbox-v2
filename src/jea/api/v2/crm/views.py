
"""
CRM :: Views

Manage accounts and account contacts.
"""

from rest_framework import exceptions, status

from ixapi_schema.v2.entities import crm, problems

from jea.api.v2.permissions import require_account
from jea.api.v2.response import tags
from jea.api.v2.viewsets import IxApiViewSet
from jea.api.v2.response import ApiSuccess, ApiError
from jea.crm.services import (
    accounts as accounts_svc,
    contacts as contacts_svc,
)
from jea.crm.models import Account


def _filter_protected_account_data(
        scoping_account,
        account,
    ):
    """
    If the account was included in the result set, because
    it was flagged as discoverable, we still do not want
    to present all information.

    :param scoping_account: The account from the request context.
    :param account: The account to present to the view
    """
    # The account is within the current accounts scope,
    # everything is fine.
    if account.scoping_account == scoping_account:
        return account

    # Outside the current account scope,
    # create minimal account representation
    return {
        "id": account.pk,
        "state": account.state, # Only production should be allowed.
        "name": account.name,
        "discoverable": account.discoverable,
        "address": {
            "country": "",
            "locality": "",
            "postal_code": "",
            "street_address": "",
        },
        "managing_account": None,
    }


class AccountsViewSet(IxApiViewSet):
    """
    A `Account` is a company using services from an IXP. The accounts
    can have a hierarchy. A account can have subaccounts. Each account needs
    to have different contacts, depending on the exchange and the service he
    wants to use.

    For a account to become operational, you need to provide a
    `legal` contact.
    """
    @require_account
    def list(self, request, account=None):
        """
        Retrieve a list of accounts.

        This includes all accounts the current authorized account
        is managing and the current account itself.
        """
        accounts = accounts_svc.get_accounts(
            scoping_account=account,
            filters=request.query_params,
            include_discoverable=True)

        # Filter sensitive information about the account,
        # if the scoping account does not match the account.
        # This can happen if the account was discoverable.
        accounts = (_filter_protected_account_data(account, c)
                     for c in accounts)

        serializer = crm.Account(accounts, many=True)

        return serializer.data

    @require_account
    def retrieve(self, request, account=None, pk=None):
        """
        Get a single account.
        """
        # Retrieve account
        requested_account = accounts_svc.get_account(
            allow_discoverable=True,
            scoping_account=account,
            account=pk)
        serializer = crm.Account(_filter_protected_account_data(
            account, requested_account))

        return serializer.data

    @require_account
    def create(self, request, account=None):
        """
        Create a new account.

        Please remember that a account may require some `contacts`
        to be created. Otherwise it will stay in an `error` state.
        """
        serializer = crm.AccountRequest(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data

        # Use requesting account as manager if no override is present
        if not validated_data["managing_account"]:
            validated_data["managing_account"] = account

        # Create new Account
        account = accounts_svc.create_account(
            scoping_account=account,
            account_request=validated_data)

        serializer = crm.Account(account)
        return ApiSuccess(serializer.data, status=status.HTTP_201_CREATED)

    @require_account
    def update(self, request, account=None, pk=None):
        """Update a account."""
        # Load account and deserialize request. Of course we
        # do not allow for discoverable accounts here.
        update_account = accounts_svc.get_account(
            scoping_account=account,
            account=pk)
        serializer = crm.AccountRequest(data=request.data)
        serializer.is_valid(raise_exception=True)

        update_account = accounts_svc.update_account(
            scoping_account=account,
            account=update_account,
            account_update=serializer.validated_data)

        serializer = crm.Account(update_account)
        return ApiSuccess(serializer.data)

    @require_account
    def partial_update(self, request, account=None, pk=None):
        """Update some fields of a account."""
        # Load account and deserialize request
        update_account = accounts_svc.get_account(
            scoping_account=account,
            account=pk)
        serializer = crm.AccountRequest(
            data=request.data,
            partial=True)
        serializer.is_valid(raise_exception=True)

        update_account = accounts_svc.update_account(
            scoping_account=account,
            account=update_account,
            account_update=serializer.validated_data)

        serializer = crm.Account(update_account)
        return ApiSuccess(serializer.data)

    # TODO: Implement destroy account
    #@require_account
    #def destroy(self, request, account=None, pk=None):
    #    """Destroy a account."""
    #    # Use accounts service to destroy the account
    #    destroyed_account = accounts_svc.delete_account(
    #        account=pk,
    #        scoping_account=account)
    #
    #    serializer = AccountSerializer(destroyed_account)
    #    return ApiSuccess(serializer.data)


class ContactsViewSet(IxApiViewSet):
    """
    A `Contact` is a role undertaking a specific responsibility within a
    account, typically a department or agent of the account company.

    These can be `implementation`,`noc`, `legal`, `peering` or `billing`.

    A contact is bound to the account by the `consuming_account`
    property.
    """
    @require_account
    def list(self, request, account=None):
        """List available contacts managed by the authorized account"""
        contacts = contacts_svc.get_contacts(
            scoping_account=account,
            filters=request.query_params)

        serializer = crm.Contact(contacts, many=True)

        return serializer.data

    @require_account
    def retrieve(self, request, account=None, pk=None):
        """Get a contact by it's id"""
        contact = contacts_svc.get_contact(
            scoping_account=account,
            contact=pk)
        serializer = crm.Contact(contact)

        return serializer.data

    @require_account
    def create(self, request, account=None):
        """
        Create a new contact.

        Please remember to set the correct `type` of the contact
        when sending the request. Available types are `noc`, `billing`,
        `legal` and `implementation`.
        """
        # Validate input
        serializer = crm.ContactRequest(data=request.data)
        serializer.is_valid(raise_exception=True)

        # Everythings fine, lets save our contact
        contact = contacts_svc.create_contact(
            scoping_account=account,
            contact_request=serializer.validated_data)

        # And respond with our fresh contact
        serializer = crm.Contact(contact)

        return ApiSuccess(serializer.data, status=status.HTTP_201_CREATED)

    @require_account
    def update(self, request, account=None, pk=None):
        """Update a contact"""
        contact = contacts_svc.get_contact(
            scoping_account=account,
            contact=pk)

        # Input validation
        serializer = crm.ContactRequest(data=request.data)

        serializer.is_valid(raise_exception=True)
        update = serializer.validated_data

        # Perform update
        contact = contacts_svc.update_contact(
            scoping_account=account,
            contact=contact,
            contact_update=update)

        serializer = crm.Contact(contact)

        return serializer.data

    @require_account
    def partial_update(self, request, account=None, pk=None):
        """Update parts of a contact"""
        contact = contacts_svc.get_contact(
            scoping_account=account,
            contact=pk)

        # Input validation
        serializer = crm.ContactRequest(
            data=request.data,
            partial=True)

        # Validate input
        serializer.is_valid(raise_exception=True)

        # Update contact
        contact = contacts_svc.update_contact(
            scoping_account=account,
            contact=contact,
            contact_update=serializer.validated_data)
        serializer = crm.Contact(contact)

        return serializer.data

    @require_account
    def destroy(self, request, account=None, pk=None):
        """Remove a contact"""
        contact = contacts_svc.delete_contact(
            scoping_account=account,
            contact=pk)

        # Serialize old data
        serializer = crm.Contact(contact)
        return serializer.data


class RolesViewSet(IxApiViewSet):
    """
    View set for all roles
    """
    @require_account
    def list(self, request, account=None):
        """List available roles"""
        roles = contacts_svc.get_roles()
        serializer = crm.Role(roles, many=True)

        return serializer.data

    @require_account
    def retrieve(self, request, account=None, pk=None):
        """Get a single role by it's id"""
        role = contacts_svc.get_role(role=pk)
        serializer = crm.Role(role)

        return serializer.data


class RoleAssignmentsViewSet(IxApiViewSet):
    """
    Manage role assignments for contacts
    """
    @require_account
    def list(self, request, account=None):
        """
        List all role assignments for all contacts
        of the requesting account.
        """
        assignments = contacts_svc.get_role_assignments(
            scoping_account=account)

        serializer = crm.RoleAssignment(
            assignments, many=True)

        return serializer.data

    @require_account
    def retrieve(self, request, account=None, pk=None):
        """Get a single role assignment"""
        assignment = contacts_svc.get_role_assignment(
            scoping_account=account,
            role_assignment=pk)

        serializer = crm.RoleAssignment(assignment)
        return serializer.data


    @require_account
    def create(self, request, account=None):
        """Create a new role assignment"""
        assignment = crm.RoleAssignmentRequest(data=request.data)
        assignment.is_valid(raise_exception=True)

        role = assignment.validated_data["role"]
        contact = assignment.validated_data["contact"]

        # Assign role, if possible
        assignment = contacts_svc.assign_role(
            role=role,
            contact=contact,
            scoping_account=account)

        serializer = crm.RoleAssignment(assignment)
        return serializer.data, status.HTTP_201_CREATED


    @require_account
    def destroy(self, request, account=None, pk=None):
        """
        Remove a role assignment
        """
        assignment = contacts_svc.get_role_assignment(
            scoping_account=account,
            role_assignment=pk)

        role = assignment["role"]
        contact = assignment["contact"]

        # Unassign role
        contacts_svc.unassign_role(
            role=role,
            contact=contact,
            scoping_account=account)

        serializer = crm.RoleAssignment(assignment)
        return serializer.data
