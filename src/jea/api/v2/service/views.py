
"""
Service :: Views

Implements service related endpoints.
In our case services are all network services.
"""

from rest_framework import exceptions, status
from ixapi_schema.v2.entities import service, problems

from jea.api.v2.permissions import require_account
from jea.api.v2.viewsets import IxApiViewSet
from jea.service.services import (
    network as network_svc,
    membership as membership_svc,
)
from jea.service.models import (
    MEMBER_JOINING_RULE_TYPES,
    SERVICE_TYPES,
)


class MemberJoiningRulesViewSet(IxApiViewSet):
    """
    Manage member joining rules for Network Services
    """
    @require_account
    def list(self, request, account=None):
        """List all member joining rules"""
        rules = membership_svc.get_member_joining_rules(
            scoping_account=account)

        return service.MemberJoiningRule(rules, many=True).data

    @require_account
    def retrieve(self, request, account=None, pk=None):
        """Get a single member joining rule"""
        rule = membership_svc.get_member_joining_rule(
            member_joining_rule=pk,
            scoping_account=account)
        return service.MemberJoiningRule(rule).data

    @require_account
    def create(self, request, account=None):
        """Create a new member joining rule"""
        rule_request = service.MemberJoiningRuleRequest(data=request.data)
        rule_request.is_valid(raise_exception=True)

        rule = membership_svc.create_member_joining_rule(
            member_joining_rule_request=rule_request.validated_data,
            scoping_account=account)

        return service.MemberJoiningRule(rule).data, status.HTTP_201_CREATED

    @require_account
    def update(self, request, account=None, pk=None):
        """Update a member joining rule"""
        rule_update = service.MemberJoiningRuleUpdate(data=request.data)
        rule_update.is_valid(raise_exception=True)

        rule = membership_svc.update_member_joining_rule(
            member_joining_rule=pk,
            member_joining_rule_update=rule_update.validated_data,
            scoping_account=account)

        return service.MemberJoiningRule(rule).data

    @require_account
    def partial_update(self, request, account=None, pk=None):
        """Update a member joining rule"""
        # Override type
        rule = membership_svc.get_member_joining_rule(
            member_joining_rule=pk,
            scoping_account=account)
        data = request.data
        data["type"] = MEMBER_JOINING_RULE_TYPES[type(rule)]

        # Deserialize request
        rule_update = service.MemberJoiningRuleUpdate(
            data=data,
            partial=True)
        rule_update.is_valid(raise_exception=True)

        rule = membership_svc.update_member_joining_rule(
            member_joining_rule=pk,
            member_joining_rule_update=rule_update.validated_data,
            scoping_account=account)

        return service.MemberJoiningRule(rule).data

    @require_account
    def destroy(self, request, account=None, pk=None):
        """Delete a member joining rule"""
        rule = membership_svc.delete_member_joining_rule(
            member_joining_rule=pk,
            scoping_account=account)

        return service.MemberJoiningRule(rule).data


class NetworkServicesViewSet(IxApiViewSet):
    """
    A `NetworkService` is an instances of a `Product` accessible by one
    or multiple users, depending on the type of product

    For example, each Exchange Network LAN is considered as a shared
    instance of the related LAN's `Product`
    """
    @require_account
    def list(self, request, account=None):
        """List available `network-services`."""
        network_services = network_svc.get_network_services(
            scoping_account=account,
            filters=request.query_params)

        return service.NetworkService(network_services, many=True).data

    @require_account
    def retrieve(self, request, account=None, pk=None):
        """Get a specific `network-service` by id"""
        network_service = network_svc.get_network_service(
            network_service=pk)

        return service.NetworkService(network_service).data

    @require_account
    def create(self, request, account=None):
        """
        Create a new NetworkService.

        Right now only elines, etrees and elans are supported.
        """
        network_service_request = service.NetworkServiceRequest(
            data=request.data)
        network_service_request.is_valid(raise_exception=True)

        # Create the network service
        network_service = network_svc.create_network_service(
            network_service_request=network_service_request.validated_data,
            scoping_account=account)

        return (
            service.NetworkService(network_service).data,
            status.HTTP_201_CREATED,
        )

    @require_account
    def update(self, request, account=None, pk=None):
        """Update a NetworkService"""
        network_service=network_svc.get_network_service(
            network_service=pk,
            scoping_account=account)

        # Override type
        data = request.data
        data["type"] = SERVICE_TYPES[type(network_service)]

        network_service_update = service.NetworkServiceRequest(data=data)
        network_service_update.is_valid(raise_exception=True)

        # Update the network service
        network_service = network_svc.update_network_service(
            network_service=network_service,
            network_service_update=network_service_update.validated_data,
            scoping_account=account)

        return service.NetworkService(network_service)

    @require_account
    def partial_update(self, request, account=None, pk=None):
        """Update some fields of a NetworkService"""
        network_service=network_svc.get_network_service(
            network_service=pk,
            scoping_account=account)

        # Override type
        data = request.data
        data["type"] = SERVICE_TYPES[type(network_service)]

        network_service_update = service.NetworkServiceRequest(
            data=request.data,
            partial=True)
        network_service_update.is_valid(raise_exception=True)

        # Update the network service
        network_service = network_svc.update_network_service(
            network_service=network_service,
            network_service_update=network_service_update.validated_data,
            scoping_account=account)

        return service.NetworkService(network_service)

    @require_account
    def destroy(self, request, account=None, pk=None):
        """Destroy a network service"""
        network_service = network_svc.get_network_service(
            network_service=pk,
            scoping_account=account)
        network_service_data = service.NetworkService(network_service).data

        # Delete network service
        network_service = network_svc.delete_network_service(
            network_service=network_service,
            scoping_account=account)

        # Respond with old network service
        return network_service_data



class NetworkFeaturesViewSet(IxApiViewSet):
    """
    `NetworkFeatures` are functionality made available to accounts
    within a `NetworkService`.
    Certain features may need to be configured by a account to use that
    service.

    This can be for example a `route server` on an `exchange lan`.

    Some of these features are mandatory to configure if you
    want to access the platform. Which of these features you have to
    configure you can query using: `/api/v1/network-features?required=true`
    """
    @require_account
    def list(self, request, account=None):
        """List available network features."""
        features = network_svc.get_network_features(
            scoping_account=account,
            filters=request.query_params)

        return service.NetworkFeature(features, many=True).data

    @require_account
    def retrieve(self, request, account=None, pk=None):
        """Get a single network feature by it's id"""
        feature = network_svc.get_network_feature(
            scoping_account=account,
            network_feature=pk)

        return service.NetworkFeature(feature).data

