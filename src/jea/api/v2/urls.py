"""
IX-API v2 urls
"""

from django.conf.urls import url, handler404
from django.urls import path, include, re_path
from rest_framework import routers

from jea.api.v2.auth import (
    handlers as auth_handlers,
    views as auth_views
)
from jea.api.v2.errors import views as errors_views
from jea.api.v2.access import views as access_views
from jea.api.v2.events import views as events_views
from jea.api.v2.catalog import views as catalog_views
from jea.api.v2.service import views as service_views
from jea.api.v2.ipam import views as ipam_views
from jea.api.v2.crm import views as crm_views


class IxApiSandboxView(routers.APIRootView):
    """IX-API"""
    authentication_classes = (
        auth_handlers.ApiSessionAuthentication,
        auth_handlers.JWTAuthentication,
    )


class IxApiRouter(routers.DefaultRouter):
    """API Router"""
    APIRootView = IxApiSandboxView

# Create router and register our viewsets
router = IxApiRouter(trailing_slash=False)

# == Auth:
router.register(r"auth/token",
                auth_views.TokenViewSet,
                basename="auth_token")

router.register(r"auth/refresh",
                auth_views.RefreshTokenViewSet,
                basename="auth_refresh")

# == CRM:
router.register(r"accounts",
                crm_views.AccountsViewSet,
                basename="accounts")

router.register(r"contacts",
                crm_views.ContactsViewSet,
                basename="contacts")

router.register(r"roles",
                crm_views.RolesViewSet,
                basename="roles")

router.register(r"role-assignments",
                crm_views.RoleAssignmentsViewSet,
                basename="role-assignments")


# == Catalog:
router.register(r"facilities",
                catalog_views.FacilitiesViewSet,
                basename="facilties")

router.register(r"devices",
                catalog_views.DevicesViewSet,
                basename="devices")

router.register(r"products",
                catalog_views.ProductsViewSet,
                basename="products")

router.register(r"pops",
                catalog_views.PointsOfPresenceViewSet,
                basename="pops")

# == Demarcs and Connections
router.register(r"demarcs",
                access_views.DemarcsViewSet,
                basename="demarcation-points")

router.register(r"connections",
                access_views.ConnectionsViewSet,
                basename="connections")

# == Network Services and Features
router.register(r"network-services",
                service_views.NetworkServicesViewSet,
                basename="network-services")

router.register(r"network-features",
                service_views.NetworkFeaturesViewSet,
                basename="network-features")

# == Member joining rules
router.register(r"member-joining-rules",
                service_views.MemberJoiningRulesViewSet,
                basename="member-joining-rules")

# == Network Services and Feature Configurations
router.register(r"network-service-configs",
                access_views.NetworkServiceConfigsViewSet,
                basename="network-service-configs")

router.register(r"network-feature-configs",
                access_views.NetworkFeatureConfigsViewSet,
                basename="network-service-configs")

# == Ip and Mac addresses:
router.register(r"ips",
                ipam_views.IpAddressViewSet,
                basename="ip-addresses")

router.register(r"macs",
                ipam_views.MacAddressViewSet,
                basename="mac-addresses")

# == Events
router.register(r"events",
                events_views.EventsViewSet,
                basename="events")

urlpatterns = [
    path("", include(router.urls)),
    re_path("(?P<path>.*)", errors_views.resource_not_found),
]

