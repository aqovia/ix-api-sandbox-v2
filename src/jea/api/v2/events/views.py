
"""
Eventmachine :: Views

You can receive events, authorized as a account for
all of your objects and your subaccounts objects
down to one level.
"""


from ixapi_schema.v2.entities import events as api_events

from jea.api.v2.permissions import require_account
from jea.api.v2.viewsets import IxApiViewSet
from jea.eventmachine.services import (
    events as events_svc,
)


class EventsViewSet(IxApiViewSet):
    """
    `Events` are generated on the platform for example, when
    a `demarc` gets put in production or a `account` is
    created and whenever a stateful object changes it's
    state.

    You can provide the serial of the last event
    in the `after` query parameter to retrieve only new events.
    """
    @require_account
    def list(self, request, account=None):
        """Retrieve all events."""
        events = events_svc.get_events(
            account=account,
            after=request.query_params.get("after", 0))

        return api_events.Event(events, many=True)

