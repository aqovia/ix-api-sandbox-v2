
"""
Events Services
"""

from jea.eventmachine.models import (
    Event,
)

def get_events(account=None, after=0, limit=1000):
    """
    Get all events after a given serial scoped to the current
    managing account.

    Returns a queryset with events or none.

    :param account: The entry point account
    :param after: The initial serial
    """
    if not account:
        return None

    events = Event.objects.filter(account=account.pk,
                                  serial__gt=after)

    return events[:limit]

