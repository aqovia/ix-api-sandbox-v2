

"""
Test events services
"""

import pytest
from model_bakery import baker

from jea.eventmachine.services import events as events_svc

@pytest.mark.django_db
def test_get_events():
    """Test getting a list of account events"""
    account = baker.make("crm.Account")

    # Make events
    event = baker.make("eventmachine.Event", account=account)

    account_events = events_svc.get_events(account=account)
    assert event in account_events


@pytest.mark.django_db
def test_get_events_limit():
    """Test getting events with a limit"""
    account = baker.make("crm.Account")

    # Make events
    baker.make("eventmachine.Event", account=account)
    baker.make("eventmachine.Event", account=account)

    events = events_svc.get_events(account=account, limit=1)
    assert events.count() == 1

