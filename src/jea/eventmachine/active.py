
"""
ReActivePy
----------

This is a variant of the concept implemented in:
https://github.com/mhannig/active

This implements a small service layer based on concepts
from functional reactive programming and event sourcing.
"""

from functools import wraps

from jea.eventmachine.models import Event


__EVENT_HANDLERS = []


def make_dispatch(command):
    """Create dispatch function Dispatch to all handlers"""
    # Todo: huh why did I want to keep the command?
    #       Maybe logging?
    def dispatch(event):
        """Dispatch function"""
        if not isinstance(event, Event):
            return

        # Persist event
        event.save()

        for handler in __EVENT_HANDLERS:
            handler(event)

    return dispatch


class handler:
    """
    Handler decorator: Registers the handler in the
    dispatch table and installs a filter for event.types.

    Non eventmachine.models.Event don't pass.
    """
    def __init__(self, *args, **kwargs):
        """Setup handler decorator with action filter"""
        self.event_types = args
        self.dispatch = make_dispatch(None)

    def __call__(self, handler):
        """Decorate handler function and register handler"""
        # Decorate with filter
        @wraps(handler)
        def _handler_wrapper(event):
            """Filter incoming actions"""
            if not isinstance(event, Event):
                return # Nothing to do here
            if self.event_types and not event.type in self.event_types:
                return # Nothing to do for us here

            return handler(self.dispatch, event)

        # Register handler
        register_handler(_handler_wrapper)

        return _handler_wrapper


def register_handler(handler):
    """Register handler"""
    global __EVENT_HANDLERS
    __EVENT_HANDLERS.append(handler)


def command(cmd):
    """
    Decorate a command function and provide a dispatch.
    Make sure the function is unary and only
    accepts a Action.
    """
    # Prepare dispatch function
    dispatch = make_dispatch(cmd)

    @wraps(cmd)
    def _command_wrapper(*args, **kwargs):
        """Accept the request and call wrapped function"""
        # Handle request
        return cmd(dispatch, **kwargs)

    return _command_wrapper

