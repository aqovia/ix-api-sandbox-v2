
"""
Admin Interface for Accounts and Contacts
"""

from django.contrib.admin import (
    ModelAdmin,
    StackedInline,
)

from jea import admin
from jea.crm.models import (
    Account, Contact,
)

#
#Accounts
#

class ContactInline(StackedInline):
    """Contact Inline Admin"""
    model = Contact
    extra = 0
    fk_name = "consuming_account"



class AccountAdmin(ModelAdmin):
    """The account admin"""
    inlines = (
        ContactInline,
    )


admin.site.register(Account, AccountAdmin)

