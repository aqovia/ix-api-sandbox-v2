
"""
Event Creators
"""

from jea.eventmachine.models import Event

CUSTOMER_CREATED = "@crm/account_created"
CUSTOMER_UPDATED = "@crm/account_updated"
CUSTOMER_DELETED = "@crm/account_deleted"
CUSTOMER_STATE_CHANGED = "@crm/account_state_changed"

CONTACT_CREATED = "@crm/contact_created"
CONTACT_UPDATED = "@crm/contact_updated"
CONTACT_DELETED = "@crm/contact_deleted"


def account_created(account):
    """Make account created event"""
    manager = account.scoping_account
    if not manager:
        manager = account

    return Event(
        type=CUSTOMER_CREATED,
        payload={
            "account": account.pk,
        },
        ref=account,
        account=manager,
    )


def account_updated(account):
    """Make account updated event"""
    manager = account.scoping_account
    if not manager:
        manager = account

    return Event(
        type=CUSTOMER_UPDATED,
        payload={
            "account": account.pk,
        },
        ref=account,
        account=manager,
    )


def account_state_changed(account, prev_state, next_state):
    """Account state changed"""
    manager = account.scoping_account
    if not manager:
        manager = account

    return Event(
        type=CUSTOMER_STATE_CHANGED,
        payload={
            "account": account.pk,
            "prev_state": prev_state.value,
            "next_state": next_state.value,
        },
        ref=account,
        account=manager,
    )


def contact_created(contact):
    """Make contact created event"""
    return Event(
        type=CONTACT_CREATED,
        payload={
            "contact": contact.pk,
            "managing_account": contact.managing_account_id,
            "consuming_account": contact.consuming_account_id,
        },
        ref=contact,
        account=contact.scoping_account,
    )


def contact_updated(contact):
    """Make a contact updated eveent"""
    return Event(
        type=CONTACT_UPDATED,
        payload={
            "contact": contact.pk,
            "managing_account": contact.managing_account_id,
            "consuming_account": contact.consuming_account_id,
        },
        ref=contact,
        account=contact.scoping_account,
    )


def contact_deleted(contact):
    """Make contact delete event"""
    return Event(
        type=CONTACT_DELETED,
        payload={
            "contact": contact.pk,
        },
        ref=contact.consuming_account,
        account=contact.scoping_account,
    )

