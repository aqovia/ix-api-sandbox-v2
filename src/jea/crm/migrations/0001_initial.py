# Generated by Django 2.2.9 on 2020-04-08 13:36

import django.contrib.postgres.fields
from django.db import migrations, models
import django.db.models.deletion
import enumfields.fields
import jea.eventmachine.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Account',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('state', enumfields.fields.EnumField(default='requested', enum=jea.eventmachine.models.State, max_length=255)),
                ('name', models.CharField(max_length=128)),
                ('legal_name', models.CharField(max_length=128, null=True)),
                ('external_ref', models.CharField(max_length=128, null=True)),
                ('discoverable', models.BooleanField(default=False)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Address',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('country', models.CharField(max_length=2)),
                ('locality', models.CharField(max_length=128)),
                ('region', models.CharField(max_length=128, null=True)),
                ('street_address', models.CharField(max_length=80)),
                ('postal_code', models.CharField(max_length=128)),
                ('post_office_box_number', models.CharField(max_length=128, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Role',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=80)),
                ('required_fields', django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=80), size=None)),
            ],
        ),
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('external_ref', models.CharField(max_length=128, null=True)),
                ('name', models.CharField(max_length=128, null=True)),
                ('telephone', models.CharField(max_length=128, null=True)),
                ('email', models.CharField(blank=True, max_length=128, null=True)),
                ('consuming_account', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='consumed_contacts', to='crm.Account')),
                ('managing_account', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='managed_contacts', to='crm.Account')),
                ('roles', models.ManyToManyField(related_name='contacts', to='crm.Role')),
                ('scoping_account', models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='scoped_contacts', to='crm.Account')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='BillingInformation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=128)),
                ('vat_number', models.CharField(max_length=20, null=True)),
                ('address', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='billing_information', to='crm.Address')),
            ],
        ),
        migrations.AddField(
            model_name='account',
            name='address',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='account', to='crm.Address'),
        ),
        migrations.AddField(
            model_name='account',
            name='billing_information',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='account', to='crm.BillingInformation'),
        ),
        migrations.AddField(
            model_name='account',
            name='managing_account',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='managed_accounts', to='crm.Account'),
        ),
        migrations.AddField(
            model_name='account',
            name='parent',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='subaccounts', to='crm.Account'),
        ),
        migrations.AddField(
            model_name='account',
            name='scoping_account',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='scoped_accounts', to='crm.Account'),
        ),
    ]
