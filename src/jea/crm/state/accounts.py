
"""
Account State Event Handler

Reacts to events like account created and
contact created.
"""

from django.core.exceptions import ValidationError

from jea.eventmachine import active
from jea.eventmachine.models import (
    State,
)
from jea.crm.status import (
    account_contact_missing,
)
from jea.crm.events import (
    CUSTOMER_CREATED,
    CONTACT_CREATED,
    CONTACT_DELETED,

    account_state_changed,
)
from jea.crm.models import (
    Account,
    Contact,
)


@active.handler(
    CUSTOMER_CREATED,
    CONTACT_CREATED,
    CONTACT_DELETED,
)
def update_account_state(dispatch, event):
    """
    Update account state when the account is created
    and contacts are added
    """
    if isinstance(event.ref, Contact):
        account = event.ref.consuming_account
    elif isinstance(event.ref, Account):
        account = event.ref
    else:
        raise ValidationError("Unsupport account reference")


    # Reset status
    account.status.clear()
    next_state = State.PRODUCTION # optimist.

    # Check state change
    if account.state != next_state:
        prev_state = account.state
        account.state = next_state
        account.save()
        dispatch(account_state_changed(account, prev_state, next_state))
