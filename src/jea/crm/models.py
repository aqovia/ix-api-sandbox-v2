
from django.db import models
from django.contrib.postgres import fields as postgres_fields
from polymorphic.models import PolymorphicModel

from utils.datastructures import reverse_mapping
from jea.eventmachine.models import StatefulMixin


#
# Mixins
#

class BillableMixin(models.Model):
    """
    A billable is linked to an account through
        billed_<modelname>
    A billable has a billing account
    """
    billing_account = models.ForeignKey(
        "crm.Account",
        related_name="billed_%(class)ss",
        on_delete=models.PROTECT)

    class Meta:
        abstract = True


class ManagedMixin(models.Model):
    """
    An entity is managed by another account
    """
    managing_account = models.ForeignKey(
        "crm.Account",
        related_name="managed_%(class)ss",
        on_delete=models.PROTECT)

    class Meta:
        abstract = True


class ManagedOptionalMixin(models.Model):
    """
    An entity can be managed by another account
    """
    managing_account = models.ForeignKey(
        "crm.Account",
        related_name="managed_%(class)ss",
        null=True,
        on_delete=models.PROTECT)

    class Meta:
        abstract = True


class OwnableMixin(models.Model):
    """
    This mixin adds ownership to a model:
    Managing and consuming accounts are referenced.

    An optional external reference
    can be used for linking to foreign datasource.
    """
    consuming_account = models.ForeignKey(
        "crm.Account",
        related_name="consumed_%(class)ss",
        on_delete=models.PROTECT)
    external_ref = models.CharField(
        max_length=128,
        null=True,
        blank=False)

    class Meta:
        abstract = True


class ContactableMixin(models.Model):
    """
    Models with this trait have a polymorphic contacts
    attribute.
    """
    # Contacts
    contacts = models.ManyToManyField(
        "crm.Contact",
        related_name="%(class)ss")

    @property
    def all_contacts(self):
        """Shortcut to all contacts queryset without filter"""
        return self.contacts.all()


    class Meta:
        abstract = True


class InvoiceableMixin(ContactableMixin, models.Model):
    """
    This trait marks objects as billable and
    adds required fields.

    Invoiceable objects are contactable.
    """
    purchase_order = models.CharField(max_length=80)
    contract_ref = models.CharField(
        max_length=128,
        null=True,
        blank=False)

    class Meta:
        abstract = True


class AccountScopedMixin(models.Model):
    """
    Scoped objects can have a related `scoping_account`.
    Permission checks should be done on this relation.

    The default permission if the `scoping_account` is None,
    is read only. This will be the case with most "system"
    entities, like the exchange lan network service provided
    by the IX.

    Rationale: Why are we using 'None' in this case and
               not an IXP account?
    Granting access to a shared object (like a network service)
    requires a M:N relationship. Preferrably with an explicit
    permission like "read/write".

    This still can be introduced in the future, but for now
    the simpler assumption is sufficient.
    """
    scoping_account = models.ForeignKey(
        "crm.Account",
        null=True,
        related_name="scoped_%(class)ss",
        on_delete=models.PROTECT)

    class Meta:
        abstract = True

#
# Models
#
class Account(
        StatefulMixin,
        AccountScopedMixin,
        ManagedOptionalMixin,
        models.Model,
    ):
    """
    A JEA Account.
    The account is one of the entry points to the graph.
    """
    parent = models.ForeignKey(
        "crm.Account",
        related_name="subaccounts",
        null=True,
        on_delete=models.SET_NULL)

    name = models.CharField(max_length=128)

    legal_name = models.CharField(
        max_length=128,
        blank=False, null=True)

    external_ref = models.CharField(
        max_length=128,
        null=True,
        blank=False)

    address = models.OneToOneField(
        "crm.Address",
        related_name="account",
        on_delete=models.CASCADE)

    billing_information = models.OneToOneField(
        "crm.BillingInformation",
        related_name="account",
        null=True,
        on_delete=models.SET_NULL)

    discoverable = models.BooleanField(default=False)

    def __str__(self):
        return "{} ({})".format(self.name, self.pk)


class BillingInformation(models.Model):
    """Account billing information"""
    name = models.CharField(max_length=128)
    address = models.OneToOneField(
        "crm.Address",
        related_name="billing_information",
        on_delete=models.CASCADE)
    vat_number = models.CharField(
        null=True, blank=False,
        max_length=20)


class Address(models.Model):
    """An address model"""
    country = models.CharField(max_length=2)
    locality = models.CharField(max_length=128)
    region = models.CharField(
        max_length=128,
        null=True,
        blank=False)
    street_address = models.CharField(max_length=80)
    postal_code = models.CharField(max_length=128)
    post_office_box_number = models.CharField(max_length=128,
                                              null=True,
                                              blank=False)


#
# Contacts and Roles
#
class Contact(
        OwnableMixin,
        ManagedMixin,
        AccountScopedMixin,
        models.Model,
    ):
    """A contact"""
    name = models.CharField(max_length=128, null=True, blank=False)
    telephone = models.CharField(max_length=128, null=True, blank=False)
    email = models.CharField(max_length=128, null=True, blank=True)

    roles = models.ManyToManyField(
        "crm.Role",
        related_name="contacts")


class Role(models.Model):
    """
    A Role has a name and many contacts.
    It might be required by various (network-) services or -features.
    """
    name = models.CharField(max_length=80)
    required_fields = postgres_fields.ArrayField(
        models.CharField(max_length=80))

