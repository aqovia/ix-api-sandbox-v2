
"""
A service for contact handling
"""

import logging
import base64
from typing import Optional, Any, List
from copy import deepcopy

from jea.exceptions import ResourceAccessDenied, ValidationError
from jea.eventmachine import active
from jea.crm.events import (
    contact_created,
    contact_updated,
    contact_deleted,
)
from jea.crm.filters import (
    ContactFilter,
)
from jea.crm.exceptions import (
    ContactInUse,
    RequiredContactRolesInvalid,
)
from jea.crm.models import (
    Account,
    Contact,
    Role,
)
from jea.crm.services import accounts as accounts_svc


logger = logging.getLogger(__name__)


def get_contacts(
        scoping_account: Account = None,
        filters: dict = None,
    ):
    """
    Get a list of filtered contacts.
    A managing account is required.

    :param scoping_account: The current managing account
    :param filters: An optional dict of query filter params
    """
    if not scoping_account:
        raise ValidationError(
            "Not allowed without managed scope", field="scoping_account")

    contacts = scoping_account.scoped_contacts.all()
    filtered = ContactFilter(filters, queryset=contacts)

    return filtered.qs


def get_contact(
        scoping_account: Account = None,
        contact: Any = None,
    ) -> Contact:
    """
    Get a account contact identified by the primary key.

    :param scoping_account: The managing account.
    :param pk: The contact's primary key

    :raises: Contact.DoesNotExist
    :raises: ResourceAccessDenied
    """
    # Resolve contact
    if not isinstance(contact, Contact):
        contact = Contact.objects.get(pk=contact)

    # Permission check
    if scoping_account:
        if contact.scoping_account_id != scoping_account.pk:
            raise ResourceAccessDenied(contact)

    return contact


@active.command
def create_contact(
        dispatch,
        contact_request=None,
        scoping_account=None,
    ) -> Contact:
    """
    Create a account contact

    :param account: An account instance
    :param contact_request: Contact data. *MUST* be validated.
    """
    # Permission check on referenced accounts
    managing_account = accounts_svc.get_account(
        scoping_account=scoping_account,
        account=contact_request["managing_account"])
    consuming_account = accounts_svc.get_account(
        scoping_account=scoping_account,
        account=contact_request["consuming_account"])

    # Create contact
    contact = Contact(
        name=contact_request.get("name"),
        telephone=contact_request.get("telephone"),
        email=contact_request.get("email"),
        managing_account=managing_account,
        consuming_account=consuming_account,
        scoping_account=scoping_account)
    contact.save()

    dispatch(contact_created(contact))

    return contact


@active.command
def update_contact(
        dispatch,
        contact: Contact = None,
        contact_update = None,
        scoping_account: Account = None,
    ):
    """
    Update a account contact.

    :param contact: The contact to update
    :param contact_update: The validated! update data
    :param scoping_account: A scope limiting account
    """
    contact = get_contact(
        scoping_account=scoping_account,
        contact=contact)
    update_fields = contact_update.keys()

    # Update contact relations
    if "managing_account" in update_fields:
        managing_account = accounts_svc.get_account(
            scoping_account=scoping_account,
            account=contact_update["managing_account"])
        contact.managing_account = managing_account

    if "consuming_account" in update_fields:
        consuming_account = accounts_svc.get_account(
            scoping_account=scoping_account,
            account=contact_update["consuming_account"])
        contact.consuming_account = consuming_account

    # Update contact attributes
    if "name" in update_fields:
        contact.name = contact_update["name"]
    if "telephone" in update_fields:
        contact.telephone = contact_update["telephone"]
    if "email" in update_fields:
        contact.email = contact_update["email"]

    contact.save()
    dispatch(contact_updated(contact))

    return contact


def has_contact_refs(
        contact=None,
        scoping_account=None,
    ):
    """
    Check if the contact has related objects...
    """
    contact = get_contact(
        contact=contact, scoping_account=scoping_account)

    # We go through all related objects...
    # While this might be expensive, it is not as expensive
    # as getting all related objects.
    for rel in contact._meta.related_objects:
        field_name = rel.get_accessor_name()
        field = getattr(contact, field_name)
        if field.exists():
            return True

    return False


def get_contact_refs(
        contact=None,
        scoping_account=None,
    ):
    """
    Get everything the contact is referenced by.
    This is quite expensive. Only use with caution.
    """
    contact = get_contact(
        contact=contact, scoping_account=scoping_account)

    # We go through all related objects... this might be expensive
    results = []
    for rel in contact._meta.related_objects:
        field_name = rel.get_accessor_name()
        field = getattr(contact, field_name)
        results += list(field.all())

    return results


@active.command
def delete_contact(
        dispatch,
        scoping_account=None,
        contact=None,
    ):
    """
    Delete contact either by providing a contact object or
    by passing the id.

    :param scoping_account: The current managing account scope
    :param contact: The contact to delete
    """
    contact = get_contact(
        scoping_account=scoping_account,
        contact=contact)

    # Check if the contact is in use
    in_use = has_contact_refs(
        contact=contact,
        scoping_account=scoping_account)
    if in_use:
        raise ContactInUse

    contact.delete()

    # Trigger state machine transitions
    dispatch(contact_deleted(contact))

    return contact


#
# Roles
#
DEFAULT_ROLES = [
    "noc",
    "implementation",
]


def assert_presence_of_contact_roles(
        required_contact_roles = None,
        contacts: List[Contact] = None,
        scoping_account=None,
    ):
    """
    Check if all required contact roles
    are present in the list of provided contacts.

    :param required_contact_roles: A list of required contact roles
    :param contact: A list of contact objects. Might be references.
    :param scoping_account: An optional account scope.

    :raises RequiredContactRolesInvalid: in case a required
        contact type is missing or not required present.
    """
    if not required_contact_roles:
        return # There is nothing required

    # Load contacts and check permissions
    contacts = [get_contact(contact=c, scoping_account=scoping_account)
                for c in contacts]

    for role in required_contact_roles:
        # Check if role is present in any contact provided
        present = False
        for contact in contacts:
            if role in contact.roles.all():
                present = True
                break # found role

        if not present:
            # We checked all contacts present, however non
            # has actually the role assigned
            raise RequiredContactRolesInvalid()


def get_roles():
    """
    Get all roles available
    """
    # Make sure default roles are present
    for role in DEFAULT_ROLES:
        get_default_role(role)

    return Role.objects.all()


def get_role(name=None, role=None):
    """
    Get a role by an identifier or name.

    :param name: A role name.
    :param role: A role identifier, or role
    """
    # Make sure default roles are present
    for default_role in DEFAULT_ROLES:
        get_default_role(default_role)

    if name:
        return Role.objects.get(name=name)
    if isinstance(role, Role):
        return role

    return Role.objects.get(pk=role)


def assign_role(
        role=None,
        contact=None,
        scoping_account=None,
    ):
    """
    Assign a role to the contact, if the requied
    fields are present.

    :param role: The role to assign
    :param contact: The contact which should assume the role
    :param scoping_account: The current account context
    """
    role = get_role(role=role)
    contact = get_contact(
        contact=contact,
        scoping_account=scoping_account)

    # Check presence of required fields
    for field in role.required_fields:
        value = getattr(contact, field, None)
        if not value:
            raise ValidationError(
                "The field is required.", field=field)

    # Everythings fine? Great! Create a cole assignment.
    contact.roles.add(role)
    contact.save()

    return make_role_assignment(role, contact)


def unassign_role(
        role=None,
        contact=None,
        scoping_account=None,
    ):
    """
    Remove a role assignment.

    :param role: The role to assign
    :param contact: The contact which should assume the role
    :param scoping_account: The current account context
    """
    role = get_role(role=role)
    contact = get_contact(
        contact=contact,
        scoping_account=scoping_account)

    # Check if the contact is in use.
    # This is a bit simplified however should be sufficient
    # behaviour.
    in_use = has_contact_refs(
        contact=contact,
        scoping_account=scoping_account)
    if in_use:
        raise ContactInUse

    if role in contact.roles.all():
        contact.roles.remove(role)
        contact.save()

    return make_role_assignment(role, contact)


def _encode_role_assignment_id(role: Role, contact: Contact) -> str:
    """
    Encode the role and contact id into a role assignment
    :param role: A role
    :param contact: A contact
    """
    return str(
        base64.b16encode(
            bytes("{}:{}".format(role.pk, contact.pk), "utf-8")),
        "utf-8")


def _decode_role_assignment_id(role_assignment: str) -> (str, str):
    """
    Encode the role and contact id into a role assignment

    :param role_assignment: A role assignment 'id'
    :returns: Tuple with role and contact pk
    """
    pk = str(base64.b16decode(bytes(role_assignment, "utf-8")), "utf-8")
    role_pk, contact_pk = pk.split(":", 2)

    return role_pk, contact_pk


def make_role_assignment(
        role=None,
        contact=None,
    ) -> dict:
    """
    Make a role assignment object. This contains the original
    role and contact and needs to be passed to an appropriate
    serializer.

    :param contact: A contact
    :param role: A role assigned to said contact
    """
    return {
        "id": _encode_role_assignment_id(role, contact),
        "role": role,
        "contact": contact,
    }


def get_role_assignments(
        scoping_account=None,
    ):
    """
    Get all role assignments for the given account.

    :param scoping_account: The current scoping account
    """
    # An assignment is a pure virtual construct,
    # as it is stored as a many to many relation without
    # direct access to the join table.
    contacts = get_contacts(scoping_account=scoping_account)
    assignments = [
        make_role_assignment(role, contact)
        for contact in contacts
        for role in contact.roles.all()]

    return assignments


def get_role_assignment(
        scoping_account=None,
        role_assignment=None,
    ):
    """
    Get a virtual role assignment

    :param scoping_account: Limit to this account
    :param role_assignment: A role assignment identifier
    """
    # Decode id
    role_pk, contact_pk = _decode_role_assignment_id(role_assignment)

    # Resolve contact and role
    role = get_role(role=role_pk)
    contact = get_contact(
        scoping_account=scoping_account,
        contact=contact_pk)

    return make_role_assignment(role, contact)


def get_default_role(name: str) -> Role:
    """
    Get a default role.

    The following roles and their required fields
    are predefined in the ix-api sandbox:

        noc:
            - email
            - telephone
        implementation:
            - name
            - telephone
            - email

    :param name: A default role name
    """
    if name == "noc":
        return _get_default_role_noc()
    if name == "implementation":
        return _get_default_role_implementation()

    raise ValueError("{} is not a valid default role.".format(
        name))


def _get_default_role_noc() -> Role:
    """Get NOC default role"""
    role, created = Role.objects.get_or_create(
        name="noc",
        required_fields=["email", "telephone"])
    if created:
        logger.debug("Initialized default role: noc")

    return role


def _get_default_role_implementation() -> Role:
    """Get implementation default role"""
    role, created = Role.objects.get_or_create(
        name="implementation",
        required_fields=["email", "telephone"])

    if created:
        logger.debug("Initialized default role: implementation")

    return role

