
"""
Accounts Service
"""

from typing import Union, Iterable, Optional

from django.db import transaction

from jea.exceptions import ResourceAccessDenied, ValidationError
from jea.eventmachine import active
from jea.eventmachine.models import (
    Event,
    State,
    StatusMessage,
)
from jea.crm.models import (
    Account,
    Address,
    BillingInformation,
    BillableMixin,
)
from jea.crm.filters import (
    AccountFilter,
)
from jea.crm.events import (
    account_created,
    account_updated,
)


def get_accounts(
        scoping_account: Account = None,
        include_discoverable=False,
        filters=None,
    ) -> Iterable[Account]:
    """
    Get the accounts. If there is a managing account
    context, limit the result set to the scope of the
    given account.

    :param scoping_account: The account scope if present
    :param filters: A dict with filter paramters. This will be applied to
                    the subtrees queryset.
    :param include_discoverable: Allow for discoverable accounts to be
        part of the result set.
    """
    queryset = AccountFilter(filters).qs

    # Limit to scoping account
    if scoping_account:
        current_account = queryset.filter(pk=scoping_account.pk)
        queryset = queryset.filter(scoping_account=scoping_account)
        queryset |= current_account

    # Add all discoverable accounts, however: these
    # are limited to production state accounts.
    if include_discoverable:
        queryset |= AccountFilter(filters).qs \
            .filter(discoverable=True) \
            .filter(state="production")

    return queryset.distinct()


def get_account(
        scoping_account: Account = None,
        account=None,
        allow_discoverable=False,
    ) -> Optional[Account]:
    """
    Retrieve a account from the repository.
    Implemented lookups:
        - account_id

    :param account_id: The identifier of the account
    :param allow_discoverable: Include discoverable accounts.
        CAVEAT be extra cautios when assigning a account
        to a resource.

    :raises ResourceAccessDenied: When permission checks fail
    """
    if not account:
        return None

    # Resolve account
    if not isinstance(account, Account):
        account = Account.objects.get(pk=account)

    # If we allow for discoverable accounts, we skip the
    # permission check. HOWEVER! be aware, that you need
    # to make sure the account can be assigned to a resource
    # and the assignment is wanted.
    if allow_discoverable and account.discoverable:
        return account

    # Perform permission check
    if scoping_account and \
        account.pk != scoping_account.pk and \
        account.scoping_account_id != scoping_account.pk:
            raise ResourceAccessDenied(account)

    return account


def _create_address(address_request) -> Address:
    """
    Create an address from request data

    :param address_request: Validated address data
    """
    address = Address(
        country=address_request["country"],
        locality=address_request["locality"],
        region=address_request.get("region"),
        postal_code=address_request["postal_code"],
        street_address=address_request["street_address"],
        post_office_box_number=address_request.get("post_office_box_number"))
    address.save()

    return address


def _update_address(address: Address, address_update) -> Address:
    """
    Update an existing address

    :param address: The address to update
    :param address_update: Validated address update data
    """
    update_fields = address_update.keys()
    if "country" in update_fields:
        address.country = address_update["country"]
    if "locality" in update_fields:
        address.locality = address_update["locality"]
    if "region" in update_fields:
        address.region = address_update["region"]
    if "postal_code" in update_fields:
        address.postal_code = address_update["postal_code"]
    if "post_office_box_number" in update_fields:
        address.post_office_box_number = \
            address_update["post_office_box_number"]
    address.save()

    return address


def _create_billing_information(
        billing_information_request=None,
    ) -> BillingInformation:
    """
    Create billing information, add addres and
    associte with account.

    :param billing_information_request: A dict with billing information.
    :param account: The associtated account
    """
    # Create billing address
    billing_address_request = billing_information_request["address"]
    billing_address = _create_address(billing_address_request)

    # Create billing information
    billing_information = BillingInformation(
        name=billing_information_request["name"],
        vat_number=billing_information_request.get("vat_number"),
        address=billing_address)
    billing_information.save()

    return billing_information


@transaction.atomic
@active.command
def create_account(
        dispatch,
        scoping_account: Account = None,
        account_request=None,
    ) -> Account:
    """
    Create a account in the database.

    :param scoping_account: An optional account managing this account.
    :param account_input: Validated account input data

    :return: a freshly created account
    """
    parent = get_account(
        account=account_request.get("parent"),
        scoping_account=scoping_account)

    # Store address
    address_request = account_request["address"]
    address = _create_address(address_request)

    # Store billing information if present
    billing_request = account_request.get("billing_information")
    if billing_request:
        billing_information = _create_billing_information(billing_request)
    else:
        billing_information = None

    # Persist account
    account = Account(
        parent=parent,
        name=account_request["name"],
        legal_name=account_request.get("legal_name"),
        external_ref=account_request.get("external_ref"),
        address=address,
        billing_information=billing_information,
        scoping_account=scoping_account)
    account.save()

    # Trigger state management
    dispatch(account_created(account))

    # Refresh model after state transition
    account.refresh_from_db()

    return account


@active.command
def update_account(
        dispatch,
        scoping_account: Account = None,
        account: Account = None,
        account_update: dict = {},
    ) -> Account:
    """
    Update a given account in the database.

    :param account_update: A validated account update.
    :param scoping_account: The current managing account.
    """
    account = get_account(scoping_account=scoping_account,
                            account=account)

    update_fields = account_update.keys()

    # Set properties
    if "name" in update_fields:
        account.name = account_update["name"]
    if "legal_name" in update_fields:
        account.name = account_update["name"]
    if "external_ref" in update_fields:
        account.external_ref = account_update["external_ref"]
    if "discoverable" in update_fields:
        account.discoverable = account_update["discoverable"]
    if "parent" in update_fields:
        parent = get_account(
            scoping_account=scoping_account,
            account=account_update["parent"])
        account.parent = parent
    if "address" in update_fields:
        _update_address(
            account.address,
            account_update["address"])
    if "billing_information" in update_fields:
        if account.billing_information:
            account.billing_information.delete()

        # Update billing information
        billing_update = account_update["billing_information"]
        if billing_update is None:
            assert_billing_information_not_in_use(account)
            billing_information = None
        else:
            # We can do this, because we do not allow for partial
            # updates in nested updates. At least for now.
            billing_information = _create_billing_information(billing_update)

        account.billing_information = billing_information

    account.save()
    dispatch(account_updated(account))

    # Refresh from database after state transition
    account.refresh_from_db()

    return account


def delete_account(
        scoping_account=None,
        account=None,
    ):
    """
    Destroy a account.
    Might fail, if protected models are affected.

    :param scoping_account: The account managing the
        account marked for deletion.
    :param account: The account to delete.
        Can be an id, which will be resolved.
    """
    raise NotImplementedError


def assert_billing_information(account=None):
    """
    Test if an account has billing information present.
    If not raise an exception.

    :param account: The account to check

    :raises ValidationError: in case no billing information is present.
    """
    if not account.billing_information:
        raise ValidationError(
            "The account is lacking billing information and "
            "can not be used as billing account.",
            field="billing_account")


def assert_billing_information_not_in_use(account=None):
    """
    Check, that our billing account is not in use by
    any entity.

    :param account: An account with billing information present

    :raises ValidationError: when the account is used as billing
        account in somewhere.
    """
    if any(M.objects.filter(billing_account=account)
           for M in BillableMixin.__subclasses__()):
        # The account is used as billing account somewhere
        raise ValidationError(
            "The account is used as billing_account and the "
            "billing information can not be removed.",
            field="billing_information")

