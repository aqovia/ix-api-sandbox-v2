
from jea.exceptions import ConditionAssertionError, ValidationError

class ContactInUse(ConditionAssertionError):
    """The contact is in use"""
    default_detail = ("The contact is currently in use.")
    default_code = "contact_in_use"

class RequiredContactRolesInvalid(ValidationError):
    default_detail = ("The config requires at least one contact, with "
                      "all roles required from corresponding network-service "
                      "or feature.")
    default_code = "required_contact_roles_invalid"
    default_field = "contacts"

