import logging

from django.apps import AppConfig

logger = logging.getLogger(__name__)

class CrmConfig(AppConfig):
    name = "jea.crm"

    verbose_name = "JEA :: CRM"

    def ready(self):
        """Application startup"""
        logger.info("Initializing app: {}".format(self.name))

        # Connect state change events by importing
        # all event handlers
        from jea.crm.state import accounts
