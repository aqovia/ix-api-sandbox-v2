
"""
Test Account Event Handlers
"""

import pytest
from model_bakery import baker

from jea.eventmachine.models import (
    State,
)
from jea.crm.events import (
    CUSTOMER_CREATED,
    CONTACT_CREATED,

    account_created,
    contact_created,
    contact_deleted,
)
from jea.crm.state import accounts as accounts_state


@pytest.mark.django_db
def test_update_account_state():
    """Test state updating based on events"""
    account = baker.make("crm.Account", state=State.REQUESTED)

    # Apply handler
    event = account_created(account)
    accounts_state.update_account_state(event)

    # Next state should be OK
    account.refresh_from_db()
    assert account.state == State.PRODUCTION

