
"""
Test event creators
"""

import pytest
from model_bakery import baker

from jea.eventmachine.models import (
    Event,
    State,
)
from jea.crm import events


#
# Accounts
#

@pytest.mark.django_db
def test_account_created():
    """Test account create event creator"""
    account = baker.make("crm.Account")
    event = events.account_created(account)

    assert isinstance(event, Event)


@pytest.mark.django_db
def test_account_updated():
    """Test account updated event creator"""
    account = baker.make("crm.Account")
    event = events.account_updated(account)
    assert event


@pytest.mark.django_db
def test_account_state_changed():
    """Test account create event creator"""
    account = baker.make("crm.Account")
    event = events.account_state_changed(
        account,
        account.state, State.ERROR)

    assert isinstance(event, Event)

#
# Contacts
#


@pytest.mark.django_db
def test_contact_created():
    """Test account contact create event creator"""
    contact = baker.make("crm.Contact")
    event = events.contact_created(contact)
    assert isinstance(event, Event)


@pytest.mark.django_db
def test_contact_updated():
    """Test account contact update event"""
    role = baker.make("crm.Role")
    contact = baker.make("crm.Contact", roles=[role])
    event = events.contact_updated(contact)
    assert event


@pytest.mark.django_db
def test_contact_deleted():
    """Test account contact deleted event"""
    role = baker.make("crm.Role")
    contact = baker.make("crm.Contact", roles=[role])
    event = events.contact_deleted(contact)
    assert event

