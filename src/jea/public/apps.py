import logging

from django.apps import AppConfig

logger = logging.getLogger(__name__)

class PublicConfig(AppConfig):
    name = "jea.public"

    def ready(self):
        """Application startup"""
        logger.info("Initializing app: {}".format(self.name))

