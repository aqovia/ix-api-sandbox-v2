
"""
Catalog Admin
-------------

Provide a minimal admin interface for catalog models.
"""

from django.contrib import admin

from jea.admin import site
from jea.catalog.models import (
    ExchangeLanNetworkProduct, ClosedUserGroupNetworkProduct,
    ELineNetworkProduct, CloudNetworkProduct,
    CloudProvider,
    Facility, FacilityOperator, FacilityCluster,
    Device, DeviceCapability, DeviceConnection,
    PointOfPresence,
)


#
# Inline Admins
#


#
# Model Admins
#
class CloudProviderAdmin(admin.ModelAdmin):
    pass

class ExchangeLanNetworkProductAdmin(admin.ModelAdmin):
    pass

class ClosedUserGroupNetworkProductAdmin(admin.ModelAdmin):
    pass

class ELineNetworkProductAdmin(admin.ModelAdmin):
    pass

class CloudNetworkProductAdmin(admin.ModelAdmin):
    pass

class FacilityOperatorAdmin(admin.ModelAdmin):
    pass

class FacilityClusterAdmin(admin.ModelAdmin):
    pass

class FacilityAdmin(admin.ModelAdmin):
    pass

class DeviceAdmin(admin.ModelAdmin):
    pass

class DeviceCapabilityAdmin(admin.ModelAdmin):
    pass

class DeviceConnectionAdmin(admin.ModelAdmin):
    pass

class PhysicalDevicesInline(admin.TabularInline):
    model = Device
    extra = 0

class PointOfPresenceAdmin(admin.ModelAdmin):
    inlines = (PhysicalDevicesInline,)

#
# Register models
#
site.register(CloudProvider, CloudProviderAdmin)

site.register(FacilityOperator, FacilityOperatorAdmin)
site.register(FacilityCluster, FacilityClusterAdmin)
site.register(Facility, FacilityAdmin)

site.register(ExchangeLanNetworkProduct,
              ExchangeLanNetworkProductAdmin)
site.register(ClosedUserGroupNetworkProduct,
              ClosedUserGroupNetworkProductAdmin)
site.register(ELineNetworkProduct,
              ELineNetworkProductAdmin)
site.register(CloudNetworkProduct,
              CloudNetworkProductAdmin)

site.register(Device, DeviceAdmin)
site.register(DeviceConnection, DeviceConnectionAdmin)
site.register(DeviceCapability, DeviceCapabilityAdmin)

site.register(PointOfPresence, PointOfPresenceAdmin)
