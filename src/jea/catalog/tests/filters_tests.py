
"""
Test Model Filters
"""

import pytest
from model_bakery import baker

from jea.catalog.filters import (
    FacilityFilter, DeviceFilter, ProductFilter,
)
from jea.catalog.models import (
    Facility
)


@pytest.mark.django_db
def test_facility_filter():
    """Test facility filtering"""
    cluster = baker.make("catalog.FacilityCluster")
    facility_a = baker.make("catalog.Facility")
    facility_b = baker.make("catalog.Facility", cluster=cluster)
    pop = baker.make("catalog.PointOfPresence",
                     physical_facility=facility_b)
    device = baker.make("catalog.Device",
                        physical_point_of_presence=pop)

    filter_requests = [
        ({"metro_area": facility_b.metro_area}, facility_b),
        ({"address_country": facility_a.address_country}, facility_a),
        ({"address_locality": facility_a.address_locality}, facility_a),
        ({"postal_code": facility_a.postal_code}, facility_a),
        ({"cluster": facility_b.cluster.name}, facility_b),
        ({"device": str(device.id)}, facility_b),
    ]

    for request, expected_facility in filter_requests:
        filtered = FacilityFilter(request)
        assert expected_facility == filtered.qs.first(), \
            (filtered.qs.first(), expected_facility)


@pytest.mark.django_db
def test_devices_filter():
    """Test filtering the devices"""
    device_a = baker.make("catalog.Device")
    device_b = baker.make("catalog.Device")

    # filter by id
    filtered = DeviceFilter({"id": f"{device_b.pk},12345689"})
    assert device_b in filtered.qs
    assert not device_a in filtered.qs

    # filter product
    product = baker.make("catalog.Product")
    filtered = DeviceFilter({"product": product.pk})
    assert device_a in filtered.qs


@pytest.mark.django_db
def test_product_filter():
    """Test product filtering"""
    product_a = baker.make("catalog.ExchangeLanNetworkProduct")
    product_b = baker.make("catalog.CloudNetworkProduct")

    filtered = ProductFilter({"type": "exchange_lan"})
    assert product_a in filtered.qs
    assert not product_b in filtered.qs

    # Filter device
    device = baker.make("catalog.Device")
    filtered = ProductFilter({"device": device.pk})
    assert product_a in filtered.qs

