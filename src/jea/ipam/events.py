
"""
IPAM events
"""

from jea.eventmachine.models import Event


IP_ADDRESS_ALLOCATED = "@ipam/ip_address_allocated"
IP_ADDRESS_RELEASED = "@ipam/ip_address_released"

MAC_ADDRESS_CREATED = "@ipam/mac_address_created"
MAC_ADDRESS_ASSIGNED = "@ipam/mac_address_assigned"
MAC_ADDRESS_REMOVED = "@ipam/mac_address_removed"


def ip_address_allocated(ip_address):
    """Ip address was allocated"""
    return Event(
        type=IP_ADDRESS_ALLOCATED,
        payload={
            "ip_address_id": ip_address.pk,
            "managing_account": ip_address.managing_account.pk,
            "consuming_account": ip_address.consuming_account.pk,
        },
        ref=ip_address,
        account=ip_address.scoping_account)


def ip_address_released(ip_address):
    return Event(
        type=IP_ADDRESS_RELEASED,
        payload={
            "ip_address_id": ip_address.pk,
            "managing_account": ip_address.managing_account.pk,
            "consuming_account": ip_address.consuming_account.pk,
        },
        account=ip_address.scoping_account)


def ip_address_updated(ip_address):
    return Event(
        type=IP_ADDRESS_RELEASED,
        payload={
            "ip_address_id": ip_address.pk,
            "managing_account": ip_address.managing_account.pk,
            "consuming_account": ip_address.consuming_account.pk,
        },
        account=ip_address.scoping_account)


def mac_address_assigned(mac_address):
    return Event(
        type=MAC_ADDRESS_ASSIGNED,
        payload={
            "mac_address_id": mac_address.pk,
            "managing_account": mac_address.managing_account.pk,
            "consuming_account": mac_address.consuming_account.pk,
        },
        ref=mac_address,
        account=mac_address.scoping_account)


def mac_address_removed(mac_address):
    return Event(
        type=MAC_ADDRESS_REMOVED,
        payload={
            "mac_address_id": mac_address.pk,
            "managing_account": mac_address.managing_account.pk,
            "consuming_account": mac_address.consuming_account.pk,
        },
        account=mac_address.scoping_account)


def mac_address_created(mac_address):
    return Event(
        type=MAC_ADDRESS_CREATED,
        payload={
            "mac_address_id": mac_address.pk,
            "managing_account": mac_address.managing_account.pk,
            "consuming_account": mac_address.consuming_account.pk,
        },
        ref=mac_address,
        account=mac_address.scoping_account)

