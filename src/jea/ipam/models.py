
"""
Ip Address Management
---------------------

Define models for managing ip address across products,
services, features, and so on.
"""

import enumfields
from django.db import models
from django.utils import timezone
from ixapi_schema.v1.constants.ipam import (
    IpVersion,
    AddressFamilies,
)

from jea.access import models as access_models
from jea.service import models as service_models
from jea.crm.models import (
    OwnableMixin,
    InvoiceableMixin,
    ManagedMixin,
    AccountScopedMixin,
)

class IpAddress(
        ManagedMixin,
        OwnableMixin,
        AccountScopedMixin,
        models.Model,
    ):
    """
    An ip address model.

    Stores the ip, a assigned fqdn and
    the prefix length of the address.

    Ip addresses can be owned by a account. And can be managed.
    """
    address = models.GenericIPAddressField()
    prefix_length = models.PositiveSmallIntegerField()
    version = enumfields.EnumIntegerField(IpVersion)

    fqdn = models.CharField(max_length=100, null=True, blank=False)

    assigned_at = models.DateTimeField(default=timezone.now)

    valid_not_before = models.DateTimeField(default=timezone.now)
    valid_not_after = models.DateTimeField(null=True, blank=False)

    ixp_allocated = models.BooleanField(default=False)

    # Relations
    #  - Services
    exchange_lan_network_service = models.ForeignKey(
        "service.ExchangeLanNetworkService",
        null=True, blank=True,
        related_name="ip_addresses",
        on_delete=models.CASCADE)

    elan_network_service = models.ForeignKey(
        "service.ELanNetworkService",
        null=True,
        related_name="ip_addresses",
        on_delete=models.CASCADE)

    etree_network_service = models.ForeignKey(
        "service.ETreeNetworkService",
        null=True,
        related_name="ip_addresses",
        on_delete=models.CASCADE)

    # - Features
    route_server_network_feature = models.ForeignKey(
        "service.RouteServerNetworkFeature",
        null=True, blank=True,
        related_name="ip_addresses",
        on_delete=models.CASCADE)

    ixp_router_network_feature = models.ForeignKey(
        "service.IXPRouterNetworkFeature",
        null=True, blank=True,
        related_name="ip_addresses",
        on_delete=models.CASCADE)

    # - Service Access
    exchange_lan_network_service_config = models.ForeignKey(
        "access.ExchangeLanNetworkServiceConfig",
        null=True, blank=True,
        related_name="ip_addresses",
        on_delete=models.CASCADE)

    # - Feature Access
    blackholing_network_feature_config = models.ForeignKey(
        "access.BlackholingNetworkFeatureConfig",
        null=True, blank=True,
        related_name="filtered_prefixes",
        on_delete=models.CASCADE)


    def __str__(self):
        """IP address as string"""
        return self.address

    def __repr__(self):
        """Mac representation"""
        return f"<IpAddress id='{self.pk}' ip='{self.address}'>"


    @property
    def assigned_rel(self):
        """
        Get the assigned relation, if there is any.
        """
        if self.exchange_lan_network_service:
            return self.exchange_lan_network_service
        if self.exchange_lan_network_service_config:
            return self.exchange_lan_network_service_config
        if self.elan_network_service:
            return self.elan_network_service
        if self.etree_network_service:
            return self.etree_network_service
        if self.route_server_network_feature:
            return self.route_server_network_feature
        if self.ixp_router_network_feature:
            return self.ixp_router_network_feature
        if self.blackholing_network_feature_config:
            return self.blackholing_network_feature_config

    @assigned_rel.setter
    def assigned_rel(self, rel):
        """
        Set the assgined relation
        """
        # unbind all
        self.exchange_lan_network_service = None
        self.exchange_lan_network_service_config = None
        self.elan_network_service = None
        self.etree_network_service = None
        self.route_server_network_feature = None
        self.ixp_router_network_feature = None
        self.blackholing_network_feature_config = None

        if rel is None:
            return # We are done here

        # bind field
        if isinstance(rel, service_models.ExchangeLanNetworkService):
            self.exchange_lan_network_service = rel
        elif isinstance(rel, service_models.ELanNetworkService):
            self.elan_network_service = rel
        elif isinstance(rel, service_models.ETreeNetworkService):
            self.etree_network_service = rel
        elif isinstance(rel, service_models.RouteServerNetworkFeature):
            self.route_server_network_feature = rel
        elif isinstance(rel, service_models.IXPRouterNetworkFeature):
            self.ixp_router_network_feature = rel
        elif isinstance(rel, access_models.ExchangeLanNetworkServiceConfig):
            self.exchange_lan_network_service_config = rel
        elif isinstance(rel, access_models.BlackholingNetworkFeatureConfig):
            self.blackholing_network_feature_config = rel
        else:
            raise ValueError("Unsupported related object: {}".format(
                type(rel)))

    @property
    def in_use(self):
        """
        An ip address is in use if it was assigned
        to a (network) service, feature or config.
        """
        return self.assigned_rel is not None

    class Meta:
        verbose_name = "IP Address"
        verbose_name_plural = "IP Addresses"


class MacAddress(
        ManagedMixin,
        OwnableMixin,
        AccountScopedMixin,
        models.Model,
    ):
    """
    A MAC address model: This model has ownership traits and
    is manageable.

    The ownership is defined by the billing and owning account,
    which might be a reseller / subaccount.
    """
    address = models.CharField(max_length=17)

    assigned_at = models.DateTimeField(default=timezone.now)
    valid_not_before = models.DateTimeField(default=timezone.now)
    valid_not_after = models.DateTimeField(null=True, blank=False)

    # Relations
    # - Service Access
    exchange_lan_network_service_configs = models.ManyToManyField(
        "access.ExchangeLanNetworkServiceConfig",
        related_name="mac_addresses")

    closed_user_group_network_service_configs = models.ManyToManyField(
        "access.ClosedUserGroupNetworkServiceConfig",
        related_name="mac_addresses")

    elan_network_service_configs = models.ManyToManyField(
        "access.ELanNetworkServiceConfig",
        related_name="mac_addresses")

    etree_network_service_configs = models.ManyToManyField(
        "access.ETreeNetworkServiceConfig",
        related_name="mac_addresses")


    def __str__(self):
        """Mac address as string"""
        return self.address

    def __repr__(self):
        """Mac representation"""
        return f"<MacAddress id='{self.pk}' mac='{self.address}'>"


    class Meta:
        verbose_name = "MAC Address"
        verbose_name_plural = "MAC Addresses"

