
"""
Test ip-address models
"""

import pytest
from model_bakery import baker

from jea.ipam import models


def test_address_families_enum():
    """Test address families"""
    assert models.AddressFamilies.AF_INET
    assert models.AddressFamilies.AF_INET6


@pytest.mark.django_db
def test_host_ip_storage_v4():
    """Test saving a host ip"""
    account = baker.make("crm.Account")
    ip_address = models.IpAddress(
        scoping_account=account,
        managing_account=account,
        consuming_account=account,
        version=models.IpVersion.IPV4,
        address="192.168.0.1",
        prefix_length=32)

    ip_address.save()

    # Test auto fields
    assert ip_address.assigned_at, \
        "assigned_at should be set and not falsy"
    assert ip_address.valid_not_before, \
        "valid_not_before should be set"

    # Some optional fields
    assert ip_address.fqdn is None


@pytest.mark.django_db
def test_host_ip_storage_v6():
    """Test saving a host ip"""
    # Test with an ipv6 address
    account = baker.make("crm.Account")
    ip_address = models.IpAddress(
        scoping_account=account,
        managing_account=account,
        consuming_account=account,
        version=models.IpVersion.IPV6,
        address="fd23::42ec:ffff:bbbb:a001/64",
        fqdn="foobar.example.net",
        prefix_length=64)
    ip_address.save()

    assert ip_address.pk, \
        "IpAddress should have been saved and assigned a pk"


@pytest.mark.django_db
def test_network_address_v4():
    """Test saving a network address"""
    account = baker.make("crm.Account")
    ip_net = models.IpAddress(
        managing_account=account,
        consuming_account=account,
        scoping_account=account,
        version=models.IpVersion.IPV6,
        address="10.0.0.0",
        fqdn="net.example.net",
        prefix_length=8)

    ip_net.save()
    assert ip_net.pk, \
        "IpAddress should have been saved and assigned a pk"

    assert ip_net.address == "10.0.0.0"


@pytest.mark.django_db
def test_network_address_v6():
    """Test saving a v6 ip"""
    # Test with an ipv6 address
    account = baker.make("crm.Account")
    ip_net = models.IpAddress(
        scoping_account=account,
        consuming_account=account,
        managing_account=account,
        version=models.IpVersion.IPV6,
        address="fd42::0",
        fqdn="net6.example.net",
        prefix_length=32)

    ip_net.save()
    assert ip_net.pk, \
        "IpAddress should have been saved and assigned a pk"

    assert ip_net.address == "fd42::0"


@pytest.mark.django_db
def test_ip_address__assigned_rel():
    """Test asigned_rel property"""
    relations = {
        "exchange_lan_network_service": baker.make(
            "service.ExchangeLanNetworkService"),
        "elan_network_service": baker.make(
            "service.ELanNetworkService"),
        "etree_network_service": baker.make(
            "service.ETreeNetworkService"),

        "route_server_network_feature": baker.make(
            "service.RouteServerNetworkFeature"),
        "ixp_router_network_feature": baker.make(
            "service.IXPRouterNetworkFeature"),

        "exchange_lan_network_service_config": baker.make(
            "access.ExchangeLanNetworkServiceConfig"),
        "blackholing_network_feature_config": baker.make(
            "access.BlackholingNetworkFeatureConfig"),
    }

    for attrib, rel in relations.items():
        ip_addr = baker.make(
            "ipam.IpAddress", **{attrib: rel})

        assert ip_addr.assigned_rel == rel


@pytest.mark.django_db
def test_ip_address__assigned_rel__set():
    """Test assigning the related object"""
    relations = {
        "exchange_lan_network_service": baker.make(
            "service.ExchangeLanNetworkService"),
        "elan_network_service": baker.make(
            "service.ELanNetworkService"),
        "etree_network_service": baker.make(
            "service.ETreeNetworkService"),

        "route_server_network_feature": baker.make(
            "service.RouteServerNetworkFeature"),
        "ixp_router_network_feature": baker.make(
            "service.IXPRouterNetworkFeature"),

        "exchange_lan_network_service_config": baker.make(
            "access.ExchangeLanNetworkServiceConfig"),
        "blackholing_network_feature_config": baker.make(
            "access.BlackholingNetworkFeatureConfig"),
    }


    for attrib, rel in relations.items():
        ip_addr = baker.make("ipam.IpAddress")
        ip_addr.assigned_rel = rel
        assert getattr(ip_addr, attrib) == rel

    # Test with unsupported type
    with pytest.raises(ValueError):
        account = baker.make("crm.Account")
        ip_addr = baker.make("ipam.IpAddress")
        ip_addr.assigned_rel = account


@pytest.mark.django_db
def test_ip_address__in_use():
    """Test in_use property of an ip address"""
    ip_addr = baker.make("ipam.IpAddress")
    assert not ip_addr.in_use

    ip_addr = baker.make(
        "ipam.IpAddress", elan_network_service=baker.make(
            "service.ELanNetworkService"))
    assert ip_addr.in_use


@pytest.mark.django_db
def test_mac_address():
    """Test mac address model"""
    c = baker.make("crm.Account")
    mac = models.MacAddress(
        address="23:42:42:42:23:23",
        scoping_account=c,
        managing_account=c,
        consuming_account=c)
    mac.save()

    assert mac.assigned_at, "Should be autofilled"
    assert mac.valid_not_before, "Should be autofilled"


def test_mac_address_representations():
    """Test to string an repr"""
    mac = models.MacAddress(address="23:42:42:42:23:23")

    assert str(mac), "__str__ should return a string"
    assert repr(mac), "__repr__ should return a string"

