
"""
Test events
"""

import pytest
from model_bakery import baker

from jea.ipam import events

@pytest.mark.django_db
def test_ip_address_allocated():
    """Test ip address allocation event creator"""
    c1 = baker.make("crm.Account")
    s1 = baker.make("service.ExchangeLanNetworkService")
    ip_address = baker.make("ipam.IpAddress",
        scoping_account=c1,
        exchange_lan_network_service=s1)
    event = events.ip_address_allocated(ip_address)

    assert event
    assert event.account == c1


@pytest.mark.django_db
def test_ip_address_released():
    """Test ip address deallocation event creator"""
    ip_address = baker.make("ipam.IpAddress")
    event = events.ip_address_released(ip_address)
    assert event


@pytest.mark.django_db
def test_mac_address_assigned():
    """Test mac address assigned event"""
    c1 = baker.make("crm.Account")
    mac_address = baker.make("ipam.MacAddress",
        scoping_account=c1)

    event = events.mac_address_assigned(mac_address)
    assert event
    assert event.account == c1


@pytest.mark.django_db
def test_mac_address_removed():
    """Test mac address removal event"""
    mac_address = baker.make("ipam.MacAddress")
    event = events.mac_address_removed(mac_address)
    assert event


@pytest.mark.django_db
def test_mac_address_created():
    """Test mac address created event"""
    mac_address = baker.make("ipam.MacAddress")
    event = events.mac_address_created(mac_address)
    assert event

