
import logging

from django.apps import AppConfig

logger = logging.getLogger(__name__)

class CtrlConfig(AppConfig):
    name = "jea.ctrl"
    verbose_name = "JEA :: Control"

    def ready(self):
        logger.info("Initializing app: {}".format(self.name))

