
"""
The Sanbox Bootstrapping Service
--------------------------------

Populates the catalog and services with
everything our demo exchange needs.
"""

import re
import time
import random
import secrets
import ipaddress

from faker import Faker
from django.utils import text as text_utils

from utils.prompt import (
    progress_exec_tasks,
    task,
)
from jea.crm.services import contacts as contacts_svc
from jea.auth import models as auth_models
from jea.access import models as access_models
from jea.catalog import models as catalog_models
from jea.crm import models as crm_models
from jea.service import models as service_models
from jea.ipam import models as ipam_models
from jea.eventmachine import models as eventmachine_models


def generate_airport_code():
    """Make some airport code"""
    faker = Faker()

    return "".join(faker.random_letters(3)).upper()


def generate_ix_name():
    """Generate a fake ix name"""
    faker = Faker()
    phrase = faker.catch_phrase().split(" ")
    suffix = random.choice(["AB", "Inc", "GmbH", "AG"])
    prefix = " ".join(phrase[:2]).title()
    net =  random.choice(["Networks", "Connectivity", "Communications"])

    return f"{prefix} {net} {suffix}"


def generate_dc_name():
    """Generate a fake ix name"""
    faker = Faker()
    phrase = faker.catch_phrase().split(" ")
    suffix = faker.company_suffix()
    prefix = " ".join(phrase[:1]).title()
    dc =  random.choice(["Datacenters", "Dataction", "Data Operations"])

    return f"{prefix} {dc} {suffix}"


def generate_pop_name(facility, exchange_name):
    """Generate a name for a point of presence"""
    ex_tokens = exchange_name.split()
    ex_name = ex_tokens[0].lower()

    fac_tokens = facility.name.split()
    fac_name = fac_tokens[-1].lower()
    pop_count = facility.physical_points_of_presence.count()

    return "{}-{}-{}".format(fac_name, ex_name, pop_count + 1)


def generate_exchange_lan_name(exchange_name, metro_area):
    """Generate exchange lan name"""
    prefix = "-".join(exchange_name.split(" ")[:2]).upper()

    return f"{prefix}-{metro_area}"


def generate_device_name(demarc, exchange_name):
    """Generate a name for a device in a facility"""
    facility = demarc.physical_facility
    # Naming schemag
    #  <dev>.<fac>.<metro>.<exchange>.com
    dev_count = facility.physical_devices.count()
    fac_tokens = facility.name.split()
    fac_name = fac_tokens[-1].lower()
    fac_company = "-".join(fac_tokens[:2]).lower()

    return "device{}.{}.{}.{}.com".format(
        dev_count + 1,
        fac_name,
        facility.metro_area.lower(),
        text_utils.slugify(exchange_name))


def generate_asn():
    """Make some fake private ASN"""
    return random.randint(64512, 65534)


def generate_email(user=None):
    """Create some email address with an optional fixed user"""
    faker = Faker()
    if not user:
        return faker.email()

    return user + "@" + faker.domain_name()


def generate_pbox_number():
    """Generate some post office box number"""
    faker = Faker()
    return "PB " + re.sub("\W", ".", faker.phone_number())


def generate_vat_number():
    """Generate a vat number"""
    faker = Faker()
    return faker.country_code() + " " + str(faker.random_number(12, True))


def get_account_domain(account):
    """Make up some domain from the account name"""
    tokens = account.name.split(" ")
    prefix = "-".join(tokens[:2]).lower()

    return f"{prefix}.net"


def create_random_ip4_prefix(account, fqdn=None):
    """Generate a random ip v4 prefix"""
    if not fqdn:
        fqdn = "net.noc.provider.example.net"

    addr = [str(random.randint(100, 150)),
            str(random.randint(0, 200)),
            str(random.randint(0, 200)),
            "0"]

    ip = ipam_models.IpAddress(
        version=4,
        address=".".join(addr),
        prefix_length=24,
        fqdn=fqdn,
        ixp_allocated=True,
        managing_account=account,
        consuming_account=account)

    ip.save()

    return ip


def create_random_ip6_prefix(account, fqdn=None):
    """Create a random ip prefix"""
    if not fqdn:
        fqdn = "net.noc.provider.example.net"

    addr = "2001:" + secrets.token_hex(2) + "::"

    ip = ipam_models.IpAddress(
        version=6,
        address=addr,
        prefix_length=64,
        fqdn=fqdn,
        ixp_allocated=True,
        managing_account=account,
        consuming_account=account)

    ip.save()

    return ip


def create_random_host(account, net, fqdn=None):
    """Create a random host in network"""
    cidr = f"{net.address}/{net.prefix_length}"
    ip_net = ipaddress.ip_network(cidr)

    max_hosts = 254
    host_generator = ip_net.hosts()

    hosts = [host_generator.send(None)
             for _ in range(max_hosts)]

    faker = Faker()
    host_ip = faker.random_element(hosts)

    prefix_length = 32
    if net.version == 6:
        prefix_length = 128

    ip = ipam_models.IpAddress(
        version=net.version,
        fqdn=fqdn,
        ixp_allocated=True,
        prefix_length=prefix_length,
        address=str(host_ip),
        managing_account=account,
        consuming_account=account)

    ip.save()

    return ip


def _clear_model(model):
    """Clear a model"""
    for m in model.objects.all().order_by("-pk"):
        m.delete()


@task("nop")
def nop(options):
    """Do nothing."""
    # If a task is done too fast, the progress indicator get's
    # a glitch and keeps hanging.
    # Maybe a racecondition.
    time.sleep(0.25)


@task()
def clear_database(options):
    """Remove all exchange data"""
    # Deleting all users
    auth_models.User.objects \
        .exclude(is_superuser=True).delete()

    # Clear access
    _clear_model(access_models.NetworkFeatureConfig)
    _clear_model(access_models.NetworkServiceConfig)
    _clear_model(access_models.DemarcationPoint)
    _clear_model(access_models.Connection)

    # Clear services
    _clear_model(service_models.NetworkFeature)
    _clear_model(service_models.NetworkService)

    # Clear catalog
    _clear_model(catalog_models.Product)
    _clear_model(catalog_models.PointOfPresence)
    _clear_model(catalog_models.DeviceCapability)
    _clear_model(catalog_models.DeviceConnection)
    _clear_model(catalog_models.Device)
    _clear_model(catalog_models.Facility)
    _clear_model(catalog_models.FacilityOperator)
    _clear_model(catalog_models.FacilityCluster)
    _clear_model(catalog_models.CloudProvider)

    # IPAM
    _clear_model(ipam_models.IpAddress)
    _clear_model(ipam_models.MacAddress)

    # Clear accounts and contacts
    _clear_model(crm_models.Contact)
    _clear_model(crm_models.Account)

#
# Accounts
#

def _create_noc_contact(account, options):
    """Create a noc contact"""
    noc_role = contacts_svc.get_default_role("noc")
    faker = Faker()
    contact = crm_models.Contact(
        telephone=faker.phone_number(),
        email=generate_email("noc"),
        scoping_account=account,
        consuming_account=account,
        managing_account=account,)
    contact.save()
    noc_role.contacts.add(contact)

    return contact


def _create_implementation_contact(account, options):
    """Create an implementation contact"""
    impl_role = contacts_svc.get_default_role("implementation")
    faker = Faker()
    contact = crm_models.Contact(
        name=faker.name(),
        email=faker.email(),
        telephone=faker.phone_number(),
        scoping_account=account,
        managing_account=account,
        consuming_account=account)
    contact.save()
    impl_role.contacts.add(contact)

    return contact


def _create_address():
    """Create some address data"""
    faker = Faker()
    address = crm_models.Address(
        country=faker.country_code(),
        locality=faker.city(),
        region=faker.country_code(),
        street_address=faker.street_address(),
        postal_code=faker.postalcode(),
        post_office_box_number=None)
    address.save()
    return address



def _create_billing_info(account_name):
    """Create a billing information contact"""
    faker = Faker()
    address = _create_address()
    billing = crm_models.BillingInformation(
        name=account_name,
        address=address,
        vat_number=generate_vat_number())
    billing.save()
    return billing


@task("public_account")
def create_public_account(options):
    """
    Create a account discoverable through the API
    """
    # Create account name
    account_name = generate_ix_name()

    address = _create_address()
    billing = _create_billing_info(account_name)

    account = crm_models.Account(
        state=eventmachine_models.State.REQUESTED,
        name=account_name,
        billing_information=billing,
        address=address,
        discoverable=True,
        external_ref="discoverable_account")
    account.save()

    # Add all contacts
    _create_noc_contact(account, options)
    _create_implementation_contact(account, options)

    account.state = eventmachine_models.State.PRODUCTION
    account.save()

    return account


@task("ix_account")
def create_ix_account(options):
    """Create the IX's own account"""
    address = _create_address()
    billing = _create_billing_info(options["exchange_name"])
    account = crm_models.Account(
        address=address,
        billing_information=billing,
        state=eventmachine_models.State.REQUESTED,
        name=options["exchange_name"],
        external_ref="demo_ix")
    account.save()

    # Add some contacts
    _create_noc_contact(account, options)
    _create_implementation_contact(account, options)

    account.state = eventmachine_models.State.PRODUCTION
    account.save()

    return account


@task("ix_account")
def load_root_account(options):
    """Get the IX's own account"""
    account = crm_models.Account.objects.order_by("id").first()
    assert account, "IX account not found. Sandbox bootstrapped?"
    assert not account.managing_account, "IX account should not have a managing_account."

    return account


@task("api_user")
def create_reseller_account(options, state):
    """Create the reseller account"""
    time.sleep(0.15)
    address = _create_address()
    billing = _create_billing_info(options["api_account"])
    account = crm_models.Account(
        state=eventmachine_models.State.REQUESTED,
        managing_account=state["ix_account"],
        billing_information=billing,
        address=address,
        name=options["api_account"],
        external_ref="demo_reseller")
    account.save()

    # Add some contacts
    _create_noc_contact(account, options)
    _create_implementation_contact(account, options)

    # Create api access
    params = {
        "username": text_utils.slugify(account.name),
        "account": account,
    }
    if options["api_key"]:
        params["api_key"] = options["api_key"]
    if options["api_secret"]:
        params["api_secret"] = options["api_secret"]

    user = auth_models.User(**params)
    user.save()

    account.state = eventmachine_models.State.PRODUCTION
    account.save()

    return user


@task("sub_account")
def create_account(options, state):
    """Create a resold account"""
    time.sleep(0.15)
    address = _create_address()
    account = crm_models.Account(
        state=eventmachine_models.State.REQUESTED,
        managing_account=state["api_user"].account,
        scoping_account=state["api_user"].account,
        address=address,
        name=options["api_account"],
        external_ref="demo_reseller")
    account.save()

    # Add some contacts
    _create_noc_contact(account, options)
    _create_implementation_contact(account, options)

    account.state = eventmachine_models.State.PRODUCTION
    account.save()

    return account



#
# Catalog
# - Facilities
#

def _create_facility(cluster=None, operator=None, metro_area=None):
    """Create a facility"""
    faker = Faker()

    if not metro_area:
        metro_area = generate_airport_code()

    if not operator:
        operator = catalog_models.FacilityOperator(
            name=generate_dc_name())
        operator.save()

    facility_name = " ".join(operator.name.split(" ")[:2]) \
        + " DC" + str(random.randint(1,100))

    facility_input = {
        "name": facility_name,
        "metro_area": metro_area,
        "address_country": faker.country_code(),
        "address_locality": faker.city(),
        "address_region": faker.country_code(),
        "postal_code": faker.postalcode(),
        "street_address": faker.street_address(),
        "operator": operator,
    }
    if cluster:
        facility_input["cluster"] = cluster

    facility = catalog_models.Facility(**facility_input)
    facility.save()

    return facility


@task("catalog_facilities")
def create_catalog_facilities(options):
    """Create facilities where services are provided"""
    cluster = catalog_models.FacilityCluster(name="60 Hopston")
    cluster.save()

    # Create standalone facilities
    facility_a = _create_facility(metro_area="MAA")
    facility_b = _create_facility(metro_area="MAB")

    # Create clustered facility
    facility_c1 = _create_facility(cluster=cluster, metro_area="MAC")
    facility_c2 = _create_facility(cluster=cluster, metro_area="MAC")

    # Operated by a operator
    operator = catalog_models.FacilityOperator(
        name=generate_dc_name())
    operator.save()

    facility_op_1 = _create_facility(operator=operator, metro_area="MAD")
    facility_op_2 = _create_facility(operator=operator, metro_area="MAD")

    facility_c3_op_1 = _create_facility(cluster=cluster,
                                        operator=operator,
                                        metro_area="MAC")

    # Cluster
    cluster = [facility_c1, facility_c2, facility_c3_op_1]
    for facility in cluster:
        facility.metro_area = "HOP"
        facility.save()

    return {
        "standalone": [facility_a, facility_b],
        "cluster": cluster,
        "op_1": [facility_op_1, facility_op_2, facility_c3_op_1],
    }


@task("catalog_pops")
def create_catalog_points_of_presence(options, state):
    """Install pops"""
    facilities = state["catalog_facilities"]
    all_facilities = list(set(
        facilities["standalone"] + \
        facilities["cluster"] + \
        facilities["op_1"]))

    pops = {}

    # Create a pop in each facility
    for facility in all_facilities:
        pop_name = generate_pop_name(facility, options["exchange_name"])
        pop = catalog_models.PointOfPresence(physical_facility=facility,
                                             name=pop_name)
        pop.save()
        pops[facility.id] = [pop]

    # First facility has two pops
    facility = facilities["standalone"][0]
    pop_name = generate_pop_name(facility, options["exchange_name"])
    pop = catalog_models.PointOfPresence(physical_facility=facility,
                                         name=pop_name)
    pop.save()
    pops[facility.id].append(pop)

    return pops

#
# - Devices
#
def _create_device(
        physical_pop,
        exchange_name,
        reachable_pops=[],
        bandwidth=None
    ):
    """Create a device in a facility"""
    device = catalog_models.Device(
        name=generate_device_name(physical_pop, exchange_name),
        physical_point_of_presence=physical_pop)
    device.save()

    for pop in reachable_pops:
        pop.reachable_devices.add(device)

    # Add device capabilities
    capabilitiy_1g = catalog_models.DeviceCapability(
        device=device,
        media_type=catalog_models.MediaType.TYPE_1000BASE_LX.value,
        speed=10000,
        q_in_q=True,
        max_lag=8,
        availability_count=25)
    capabilitiy_1g.save()

    capabilitiy_10g = catalog_models.DeviceCapability(
        device=device,
        media_type=catalog_models.MediaType.TYPE_10GBASE_LR.value,
        speed=10000,
        q_in_q=True,
        max_lag=8,
        availability_count=25)
    capabilitiy_10g.save()

    capabilitiy_100g = catalog_models.DeviceCapability(
        device=device,
        media_type=catalog_models.MediaType.TYPE_100GBASE_LR4.value,
        speed=100000,
        q_in_q=True,
        max_lag=8,
        availability_count=25)
    capabilitiy_100g.save()

    if bandwidth == 400000:
        capabilitiy_400g = catalog_models.DeviceCapability(
            device=device,
            media_type=catalog_models.MediaType.TYPE_400GBASE_LR8.value,
            speed=400000,
            q_in_q=True,
            max_lag=8,
            availability_count=15)
        capabilitiy_400g.save()

    return device


def _connect_devices(devices, bandwidth=400000):
    """Full meshed connection"""
    for device_a in devices:
        for device_b in devices:
            if device_a == device_b:
                continue

            connection = catalog_models.DeviceConnection(
                max_capacity=bandwidth,
                device=device_a,
                connected_device=device_b)
            connection.save()


@task("catalog_devices")
def create_catalog_devices(options, state):
    """Install devices in facilities"""
    facilities = state["catalog_facilities"]
    all_facilities = list(set(
        facilities["standalone"] + \
        facilities["cluster"] + \
        facilities["op_1"]))

    devices = {}

    # Topology:
    # - All devices in a facility are connected
    # - For all facilities: the last device of a facility
    #   is connected to the first device of the next facility
    # - The last device is connected to the first, forming
    #   a ring through all facilities.
    for facility in all_facilities:
        pop = facility.physical_points_of_presence.first()

        # Create devices a
        # Create and connect devices
        devices[facility.id] = [
            _create_device(pop, options["exchange_name"]),
            _create_device(pop, options["exchange_name"]),
            _create_device(pop, options["exchange_name"]),
        ]

        _connect_devices(devices[facility.id])

    # Create special 400G device in first facility at second demarc,
    # but reachable from the first demarc.
    facility = all_facilities[0]
    pops = facility.physical_points_of_presence.all().order_by("name")
    assert pops.count() == 2
    assert pops[0].physical_devices.count() > 0
    assert pops[1].physical_devices.count() == 0

    device = _create_device(pops[1],
                            options["exchange_name"],
                            reachable_pops=[pops[0]],
                            bandwidth=400000)

    _connect_devices([device, devices[facility.id][0]])


    # Connect devices between facilities
    for i, _ in enumerate(all_facilities):
        facility_a = all_facilities[i]
        facility_b = all_facilities[(i + 1) % len(all_facilities)]

        device_a = devices[facility_a.id][-1] # Last device
        device_b = devices[facility_b.id][0] # First device

        _connect_devices([device_a, device_b], bandwidth=10000)

    return devices

#
# Allocate reseller demarcs
#

def _allocate_demarc(account, pop):
    """Create a demarc a a pop"""
    demarc_num = access_models.DemarcationPoint.objects.filter(
        consuming_account=account.pk).count() + 1
    name = "{} c({}) d({})".format(
        pop.name,
        account.pk,
        demarc_num)

    demarc = access_models.DemarcationPoint(
        state=eventmachine_models.State.ALLOCATED,
        point_of_presence=pop,
        managing_account=account,
        consuming_account=account,
        scoping_account=account,
        billing_account=account,
        media_type=access_models.MediaType.TYPE_10GBASE_LR.value,
        name=name)
    demarc.save()

    return name


@task("reseller_demarcs")
def create_reseller_demarcs(options, state):
    """Allocate demarcs for a reseller"""
    time.sleep(0.1)
    reseller = state["api_user"].account

    pops = catalog_models.PointOfPresence.objects.all()
    demarcs = [
        _allocate_demarc(reseller, pop)
        for pop in pops]
    # Allocate another round of demarcs,
    # but only in the first two pop
    demarcs += [
        _allocate_demarc(reseller, pop)
        for pop in pops[:2]]

    return demarcs
#
# - Products
#

#
# -- Peering / ExchangeLan
#
@task("exchange_lan_products")
def create_exchange_lan_products(options, state):
    """Create exchange lan product(s)"""
    products = []

    product = catalog_models.ExchangeLanNetworkProduct(
        name="Planetary Connect")
    product.save()
    products.append(product)

    # We provide some local peering product in MAA
    product = catalog_models.ExchangeLanNetworkProduct(
        name="Optimal Reach MAA",
        metro_area="MAA")
    product.save()
    products.append(product)

    return products


# Setup Exchange Lans:

def _create_blackholing_feature(exchange_lan):
    """Create blackholing feature"""
    feature = service_models.BlackholingNetworkFeature(
        network_service=exchange_lan,
        required=False,
        name="Secure Protect Blackholing")
    feature.save()

    return feature


def _create_route_server_feature(options, exchange_lan):
    """Create a routeserver on the exchange lan"""
    asn = options["exchange_asn"]
    rsnum = exchange_lan.network_features.instance_of(
        service_models.RouteServerNetworkFeature).count() + 1
    account = exchange_lan.consuming_account
    domain = get_account_domain(account)
    metro_area = exchange_lan.metro_area.lower()
    fqdn = f"rs{rsnum}.{metro_area}.noc.{domain}"
    looking_glass_url = (f"https://lg.{domain}/routeservers/rs{rsnum}_"
                         f"{metro_area}")

    # Get exchange lan ip ranges
    exchange_lan_ips = exchange_lan.ip_addresses.all()
    # Check address families supported
    ip_versions = [ip.version.value for ip in exchange_lan_ips]
    address_families = []
    if 4 in ip_versions:
        address_families.append(service_models.AddressFamilies.AF_INET)
    if 6 in ip_versions:
        address_families.append(service_models.AddressFamilies.AF_INET6)

    bgp_session_types = [
        access_models.BGPSessionType.TYPE_ACTIVE,
        access_models.BGPSessionType.TYPE_PASSIVE,
    ]

    rs = service_models.RouteServerNetworkFeature(
        name=f"rs{rsnum}-{exchange_lan.metro_area.lower()}",
        session_mode=access_models.RouteServerSessionMode.MODE_PUBLIC,
        address_families=address_families,
        available_bgp_session_types=bgp_session_types,
        asn=asn,
        fqdn=fqdn,
        looking_glass_url=looking_glass_url,
        required=True,
        network_service=exchange_lan)
    rs.save()
    rs.nfc_required_contact_roles.add(
        contacts_svc.get_default_role("implementation"))

    ip_addresses = [
        create_random_host(account, net, fqdn=fqdn)
        for net in exchange_lan.ip_addresses.all()]

    rs.ip_addresses.set(ip_addresses)

    return rs


def _create_ixprouter_feature(options, exchange_lan):
    """Create an ixprouter on the exchange lan"""
    asn = options["exchange_asn"]
    rsnum = exchange_lan.network_features.instance_of(
        service_models.IXPRouterNetworkFeature).count() + 1
    account = exchange_lan.consuming_account
    domain = get_account_domain(account)
    metro_area = exchange_lan.metro_area.lower()
    fqdn = f"ixp-router{rsnum}.{metro_area}.noc.{domain}"

    rs = service_models.IXPRouterNetworkFeature(
        name=f"ixp-router{rsnum}-{metro_area}",
        asn=asn,
        fqdn=fqdn,
        required=False,
        network_service=exchange_lan)
    rs.save()

    ip_addresses = [
        create_random_host(account, net, fqdn=fqdn)
        for net in exchange_lan.ip_addresses.all()]

    rs.ip_addresses.set(ip_addresses)

    return rs


def _create_exchange_lan_network_service(
        options,
        exchange_name,
        exchange_account,
        product,
        metro_area,
    ):
    """Create an exchange lan network service"""
    exchange_lan = service_models.ExchangeLanNetworkService(
        name=generate_exchange_lan_name(exchange_name, metro_area),
        metro_area=metro_area,
        managing_account=exchange_account,
        consuming_account=exchange_account,
        product=product)
    exchange_lan.save()
    exchange_lan.nsc_required_contact_roles.add(
        contacts_svc.get_default_role("implementation"))
    exchange_lan.nsc_required_contact_roles.add(
        contacts_svc.get_default_role("noc"))

    domain = get_account_domain(exchange_account)
    fqdn = f"noc.{domain}"

    # Assign IP addresses
    net_v4 = create_random_ip4_prefix(exchange_account, fqdn=fqdn)
    net_v6 = create_random_ip6_prefix(exchange_account, fqdn=fqdn)

    exchange_lan.ip_addresses.add(net_v4)
    exchange_lan.ip_addresses.add(net_v6)

    # Create features:
    _create_blackholing_feature(
        exchange_lan)

    _create_route_server_feature(
        options,
        exchange_lan)
    _create_route_server_feature(
        options,
        exchange_lan)

    _create_ixprouter_feature(
        options,
        exchange_lan)

    return exchange_lan


@task("exchange_lan_services")
def create_exchange_lan_services(options, state):
    """Create services for the product"""
    # Get our own exchange account
    exchange_name = options["exchange_name"]
    ix_account = state["ix_account"]
    products = state["exchange_lan_products"]

    # Create peering services in each metro area
    product = products[0] # Planet Peering
    metro_areas = list(set(
        f.metro_area
        for f in catalog_models.Facility.objects.all()))

    services = []
    for metro in metro_areas:
        service = _create_exchange_lan_network_service(
            options,
            exchange_name,
            ix_account,
            product,
            metro)
        services.append(service)

    # This other lan
    product = products[1]
    metro = product.metro_area
    service = _create_exchange_lan_network_service(
        options, "METRONET", ix_account, product, metro)

    services.append(service)

    return services
#
# - Virtual Network Products
# -- Eline
#
@task("eline_products")
def create_eline_products(options, state):
    """Create eline network product(s)"""
    products = []

    product = catalog_models.ELineNetworkProduct(
        name="Private Jet Connect")
    product.save()
    products.append(product)

    return products


@task("elan_products")
def create_elan_products(options, state):
    """Create elan network product(s)"""
    products = []

    product = catalog_models.ELanNetworkProduct(
        name="Private Peer Connect")
    product.save()
    products.append(product)

    return products


@task("etree_products")
def create_etree_products(options, state):
    """Create elan network product(s)"""
    products = []

    product = catalog_models.ETreeNetworkProduct(
        name="Private Secure Connect")
    product.save()
    products.append(product)

    return products


def setup_sandbox(**options):
    """
    Setup the sandbox.

    Create required models and populate
    our catalog and services.
    """
    tasks = [
        "Clearing database...", [clear_database],
        "Creating accounts...", [
            create_ix_account,
            create_reseller_account,
            create_account,
            create_public_account,
        ],
        "Building facilities...", [
            create_catalog_facilities,
            create_catalog_points_of_presence,
        ],
        "Installing switches and splicing fibers...", [
            create_catalog_devices,
        ],
        "Inflating cloud providers...", [
            nop,
            # create_cloud_products,
        ],
        "Getting products from marketing...", [
            create_exchange_lan_products,
            create_eline_products,
            create_elan_products,
            create_etree_products,
        ],
        "Provisioning services...", [
            create_exchange_lan_services,
        ],
    ]

    if options["allocate_demarcs"]:
        tasks += [
            "Installing optics...", [
                create_reseller_demarcs,
            ]
        ]

    return progress_exec_tasks(tasks, options)


def setup_account(**options):
    """Create a account"""
    tasks = [
        "Getting IXP account...", [
            load_root_account,
        ],
        "Creating account...", [
            create_reseller_account,
            create_account,
            create_public_account,
        ],
    ]

    if options["allocate_demarcs"]:
        tasks += [
            "Installing optics...", [
                create_reseller_demarcs,
            ]
        ]

    return progress_exec_tasks(tasks, options)
