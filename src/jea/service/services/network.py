
"""
JEA Service: Network

Access and manage all network services, like
Exchange Lans, Closed User Groups, etc...
"""

from typing import Optional

from django.db.models.query import QuerySet
from ixapi_schema.v2.constants.service import (
    NETWORK_SERVICE_TYPE_E_LINE,
    NETWORK_SERVICE_TYPE_E_LAN,
    NETWORK_SERVICE_TYPE_E_TREE,
)

from jea.exceptions import ResourceAccessDenied, ValidationError
from jea.eventmachine import active
from jea.service.models import (
    NetworkService,
    ExchangeLanNetworkService,
    ELineNetworkService,
    ETreeNetworkService,
    ELanNetworkService,
    NetworkFeature,
    MemberJoiningRule,
)
from jea.catalog.models import (
    ELineNetworkProduct,
    ELanNetworkProduct,
    ETreeNetworkProduct,
)
from jea.service.exceptions import (
    NetworkFeatureNotAvailable,
    ProductTypeMismatch,
)
from jea.service.filters import (
    NetworkServiceFilter,
    NetworkFeatureFilter,
)
from jea.crm import models as crm_models
from jea.crm.services import (
    accounts as accounts_svc,
    contacts as contacts_svc,
)
from jea.catalog.services import products as products_svc
from jea.ipam.services import ip_addresses as ip_addresses_svc


def get_network_services(
        scoping_account=None,
        filters=None,
    ) -> QuerySet:
    """
    List all network services and apply filters

    :param filters: A dict of filters.
    """
    # Make queryset:
    #
    # Allow for all exchange lan network services,
    # services within the account scope and services
    # which are public.

    # Legacy Network Services
    exchange_lans = NetworkServiceFilter(
        filters,
        queryset=NetworkService.objects.instance_of(
            ExchangeLanNetworkService)).qs

    # Scoped network services
    scoped_network_services = NetworkServiceFilter(
        filters,
        queryset=NetworkService.objects.filter(
            scoping_account=scoping_account)).qs

    # Public network services
    public_network_services = NetworkServiceFilter(
        filters,
        queryset=NetworkService.objects.filter(public=True)).qs

    # Make queryset
    queryset = exchange_lans | scoped_network_services | public_network_services

    return queryset.distinct()


def get_network_service(
        network_service=None,
        scoping_account=None,
    ) -> Optional[NetworkService]:
    """
    Get a specific network service.
    Right now the supported lookup methods are:
     - service_id

    TODO: Add billing and owning account check for services
          which are account instances. E.g. ClosedUserGroups

    :param service_id: The primary key of the network service
    """
    if not network_service:
        raise ValidationError("A service identifier or object is required")

    if isinstance(network_service, NetworkService):
        return network_service

    return NetworkService.objects.get(pk=network_service)


def create_network_service(
        scoping_account=None,
        network_service_request=None,
    ) -> NetworkService:
    """
    Create a new network service.

    Only the creation of elan, eline and etree network
    services is supported right now.

    :param scoping_account: The scoping account managing network services
    :param network_service_request: A request for a new network service.
    """
    # Get related accounts
    managing_account = accounts_svc.get_account(
        scoping_account=scoping_account,
        account=network_service_request["managing_account"])
    consuming_account = accounts_svc.get_account(
        scoping_account=scoping_account,
        account=network_service_request["consuming_account"])
    billing_account = accounts_svc.get_account(
        scoping_account=scoping_account,
        account=network_service_request["billing_account"])
    accounts_svc.assert_billing_information(account=billing_account)

    # Get product and make sure it matches the requested
    # network service. In this case an ELine
    product = products_svc.get_product(
        scoping_account=scoping_account,
        product=network_service_request["product"])

    # Dispatch the creation of the network service based on
    # the given type.
    if network_service_request["type"] == NETWORK_SERVICE_TYPE_E_LINE:
        # Validate product
        if not isinstance(product, ELineNetworkProduct):
            raise ProductTypeMismatch()
        return _create_eline_network_service(
            scoping_account=scoping_account,
            managing_account=managing_account,
            consuming_account=consuming_account,
            billing_account=billing_account,
            product=product,
            network_service_request=network_service_request)
    if network_service_request["type"] == NETWORK_SERVICE_TYPE_E_LAN:
        if not isinstance(product, ELanNetworkProduct):
            raise ProductTypeMismatch()
        return _create_elan_network_service(
            scoping_account=scoping_account,
            managing_account=managing_account,
            consuming_account=consuming_account,
            billing_account=billing_account,
            product=product,
            network_service_request=network_service_request)
    if network_service_request["type"] == NETWORK_SERVICE_TYPE_E_TREE:
        if not isinstance(product, ETreeNetworkProduct):
            raise ProductTypeMismatch()
        return _create_etree_network_service(
            scoping_account=scoping_account,
            managing_account=managing_account,
            consuming_account=consuming_account,
            billing_account=billing_account,
            product=product,
            network_service_request=network_service_request)

    # We do not support the creation of any other network service
    # right now.
    raise NotImplementedError


def _create_eline_network_service(
        scoping_account=None,
        managing_account=None,
        consuming_account=None,
        billing_account=None,
        product=None,
        network_service_request=None,
    ) -> ELineNetworkService:
    """
    Create an ELine network service

    :param scoping_account: The scoping account managing network services
    :param managing_account: A resolved and validated managing account
    :param consuming_account: A validated consuming account
    :param billing_account: An account with billing information present
    :param product: A validated eline product
    :param network_service_request: A request for a new network service.
    """
    joining_member_account = accounts_svc.get_account(
        scoping_account=scoping_account,
        account=network_service_request["joining_member_account"])

    # Validate capacity
    # TODO: Check that the capacity does not exceed the total
    #       capcity of the customers demcars.
    capacity = network_service_request["capacity"]
    if capacity < 1:
        raise ValidationError("Capacity can not be < 1", field="capacity")


    # Let's create the eline network service!
    network_service = ELineNetworkService(
        scoping_account=scoping_account,
        managing_account=managing_account,
        consuming_account=consuming_account,
        joining_member_account=joining_member_account,
        billing_account=billing_account,
        product=product,
        capacity=capacity,
        external_ref=network_service_request.get("external_ref"),
        purchase_order=network_service_request.get("purchase_order", ""),
        contract_ref=network_service_request.get("contract_ref"),
        public=False,
        name=network_service_request["name"])

    network_service.save()

    # Assign required contact roles: NOC, Implementation
    network_service.nsc_required_contact_roles.add(
        contacts_svc.get_default_role("noc"))
    network_service.nsc_required_contact_roles.add(
        contacts_svc.get_default_role("implementation"))

    return network_service


def _create_elan_network_service(
        scoping_account=None,
        managing_account=None,
        consuming_account=None,
        billing_account=None,
        product=None,
        network_service_request=None,
    ) -> ELanNetworkService:
    """
    Create an ELan network service

    :param scoping_account: The scoping account managing network services
    :param network_service_request: A request for a new network service.
    """

    # Validate capacity
    default_capacity_min = network_service_request["default_capacity_min"]
    default_capacity_max = network_service_request["default_capacity_max"]
    if default_capacity_max is not None and \
        default_capacity_max is not None and \
        default_capacity_min > default_capacity_max:
            raise ValidationError(
                "default_capacity_min is greater than default_capacity_max",
                field="default_capacity_min")

    # Let's create the elan network service!
    network_service = ELanNetworkService(
        managing_account=managing_account,
        consuming_account=consuming_account,
        scoping_account=scoping_account,
        billing_account=billing_account,
        product=product,
        external_ref=network_service_request.get("external_ref"),
        purchase_order=network_service_request.get("purchase_order", ""),
        contract_ref=network_service_request.get("contract_ref"),
        public=network_service_request.get("public", False),
        name=network_service_request["name"],
        initiator_capacity=network_service_request["initiator_capacity"],
        default_capacity_min=default_capacity_min,
        default_capacity_max=default_capacity_max)
    network_service.save()

    # Assign required contact roles:
    # This is IX dependent - for now we require a NOC
    # and an Implementation contact
    network_service.nsc_required_contact_roles.add(
        contacts_svc.get_default_role("noc"))
    network_service.nsc_required_contact_roles.add(
        contacts_svc.get_default_role("implementation"))

    _assign_ip_addresses(
        network_service=network_service,
        ip_addresses=network_service_request.get("ip_addresses"),
        scoping_account=scoping_account)

    return network_service


def _create_etree_network_service(
        scoping_account=None,
        managing_account=None,
        consuming_account=None,
        billing_account=None,
        product=None,
        network_service_request=None,
    ) -> ETreeNetworkService:
    """
    Create an ETree network service

    :param scoping_account: The scoping account managing network services
    :param network_service_request: A request for a new network service.
    """
    # Validate capacity
    default_capacity_min = network_service_request["default_capacity_min"]
    default_capacity_max = network_service_request["default_capacity_max"]
    if default_capacity_max is not None and \
        default_capacity_min is not None and \
        default_capacity_min > default_capacity_max:
            raise ValidationError(
                "default_capacity_min is greater than default_capacity_max",
                field="default_capacity_min")

    # Let's create the elan network service!
    network_service = ETreeNetworkService(
        managing_account=managing_account,
        consuming_account=consuming_account,
        billing_account=billing_account,
        scoping_account=scoping_account,
        product=product,
        external_ref=network_service_request.get("external_ref"),
        purchase_order=network_service_request.get("purchase_order", ""),
        contract_ref=network_service_request.get("contract_ref"),
        public=network_service_request.get("public", False),
        name=network_service_request["name"],
        initiator_capacity=network_service_request["initiator_capacity"],
        default_capacity_min=default_capacity_min,
        default_capacity_max=default_capacity_max)
    network_service.save()

    # Assign required contact roles: NOC, Implementation
    network_service.nsc_required_contact_roles.add(
        contacts_svc.get_default_role("noc"))
    network_service.nsc_required_contact_roles.add(
        contacts_svc.get_default_role("implementation"))

    _assign_ip_addresses(
        network_service=network_service,
        ip_addresses=network_service_request.get("ip_addresses"),
        scoping_account=scoping_account)

    return network_service


def _assign_ip_addresses(
        network_service=None,
        ip_addresses=None,
        scoping_account=None,
    ):
    """Use the ip addresses by the network service"""
    # Unbind all ip addresses
    for addr in network_service.ip_addresses.all():
        ip_addresses_svc.release_ip_address(
            ip_address=addr,
            scoping_account=scoping_account)

    if not ip_addresses:
        return # We are done here.

    # Bind new ip addresses
    for ip_address in ip_addresses:
        ip_addresses_svc.bind_ip_address(
            ip_address=ip_address,
            to=network_service,
            scoping_account=scoping_account)


def update_network_service(
        network_service=None,
        network_service_update=None,
        scoping_account=None,
    ):
    """
    Update a network service.

    :param network_service: The network service to update
    :param network_service_update: Updates to the network service
    :param scoping_account: A account context
    """
    network_service = get_network_service(
        network_service=network_service,
        scoping_account=scoping_account)

    # We only allow for elan, eline and etree network
    # services to be updated
    if not isinstance(network_service, (
            ELineNetworkService,
            ELanNetworkService,
            ETreeNetworkService,
        )):
            raise NotImplementedError

    update_fields = network_service_update.keys()

    # Update related accounts
    if "managing_account" in update_fields:
        managing_account = accounts_svc.get_account(
            scoping_account=scoping_account,
            account=network_service_update["managing_account"])
        network_service.managing_account = managing_account

    if "consuming_account" in update_fields:
        consuming_account = accounts_svc.get_account(
            scoping_account=scoping_account,
            account=network_service_update["consuming_account"])
        network_service.consuming_account = consuming_account

    if "billing_account" in update_fields:
        billing_account = accounts_svc.get_account(
            scoping_account=scoping_account,
            account=network_service_update["billing_account"])
        accounts_svc.assert_billing_information(billing_account)
        network_service.billing_account = billing_account

    # Name
    if "name" in update_fields:
        network_service.name = \
            network_service_update["name"]

    # Purchaseable
    if "external_ref" in update_fields:
        network_service.external_ref = \
            network_service_update["external_ref"]
    if "purchase_order" in update_fields:
        network_service.purchase_order = \
            network_service_update["purchase_order"]
    if "contract_ref" in update_fields:
        network_service.contract_ref = \
            network_service_update["contract_ref"]

    # Dispatch updates for all other fields, which might
    # not be common across all network services.
    if isinstance(network_service, ELineNetworkService):
        return _update_eline_network_service(
            update_fields=update_fields,
            network_service=network_service,
            network_service_update=network_service_update,
            scoping_account=scoping_account)
    if isinstance(network_service, ELanNetworkService):
        return _update_elan_network_service(
            update_fields=update_fields,
            network_service=network_service,
            network_service_update=network_service_update,
            scoping_account=scoping_account)
    if isinstance(network_service, ETreeNetworkService):
        return _update_etree_network_service(
            update_fields=update_fields,
            network_service=network_service,
            network_service_update=network_service_update,
            scoping_account=scoping_account)


def _update_eline_network_service(
        network_service=None,
        network_service_update=None,
        scoping_account=None,
        update_fields=None,
    ):
    """
    Update eline specific fields.
    """
    # Product change
    if "product" in update_fields:
        product = products_svc.get_product(
            scoping_account=scoping_account,
            product=network_service_update["product"])
        if not isinstance(product, ELineNetworkProduct):
            raise ProductTypeMismatch()
        network_service.product = product

    # Capcity change
    if "capacity" in update_fields:
        # Validate capacity
        # TODO: Check that the capacity does not exceed the total
        #       capcity of the customers demcars.
        capacity = network_service_update["capacity"]
        if capacity < 1:
            raise ValidationError("Capacity can not be < 1", field="capacity")
        network_service.capacity = capacity

    # Persist changes
    network_service.save()

    return network_service


def _update_elan_network_service(
        network_service=None,
        network_service_update=None,
        scoping_account=None,
        update_fields=None,
    ):
    """
    Update elan specific fields and relations
    """
    # Product change
    if "product" in update_fields:
        product = products_svc.get_product(
            scoping_account=scoping_account,
            product=network_service_update["product"])
        if not isinstance(product, ELanNetworkProduct):
            raise ProductTypeMismatch()
        network_service.product = product

    # Ip address updates
    if "ip_addresses" in update_fields:
        _assign_ip_addresses(
            network_service=network_service,
            ip_addresses=network_service_update["ip_addresses"],
            scoping_account=scoping_account)

    # Capacity
    if "initiator_capacity" in update_fields:
        network_service.initiator_capacity = \
            network_service_update["initiator_capacity"]
    if "default_capacity_min" in update_fields:
        network_service.default_capacity_min = \
            network_service_update["default_capacity_min"]
    if "default_capacity_max" in update_fields:
        network_service.default_capacity_max = \
            network_service_update["default_capacity_max"]

    # Validate capacity
    capacity_min = network_service.default_capacity_min
    capacity_max = network_service.default_capacity_max

    if capacity_max is not None and \
        capacity_min is not None and \
        capacity_min > capacity_max:
            raise ValidationError(
                "default_capacity_min is greater than default_capacity_max",
                field="default_capacity_min")

    network_service.save()

    return network_service


def _update_etree_network_service(
        network_service=None,
        network_service_update=None,
        scoping_account=None,
        update_fields=None,
    ):
    """
    Update elan specific fields and relations
    """
    # Product change
    if "product" in update_fields:
        product = products_svc.get_product(
            scoping_account=scoping_account,
            product=network_service_update["product"])
        if not isinstance(product, ETreeNetworkProduct):
            raise ProductTypeMismatch()
        network_service.product = product

    # Ip address updates
    if "ip_addresses" in update_fields:
        _assign_ip_addresses(
            network_service=network_service,
            ip_addresses=network_service_update["ip_addresses"],
            scoping_account=scoping_account)

    # Capacity
    if "initiator_capacity" in update_fields:
        network_service.initiator_capacity = \
            network_service_update["initiator_capacity"]
    if "default_capacity_min" in update_fields:
        network_service.default_capacity_min = \
            network_service_update["default_capacity_min"]
    if "default_capacity_max" in update_fields:
        network_service.default_capacity_max = \
            network_service_update["default_capacity_max"]

    # Validate capacity
    capacity_min = network_service.default_capacity_min
    capacity_max = network_service.default_capacity_max
    if capacity_max is not None and \
        capacity_min is not None and \
        capacity_min > capacity_max:
            raise ValidationError(
                "default_capacity_min is greater than default_capacity_max",
                field="default_capacity_min")

    network_service.save()

    return network_service


def delete_network_service(
        network_service=None,
        scoping_account=None,
    ):
    """Remove a network service"""
    network_service = get_network_service(
        network_service=network_service,
        scoping_account=scoping_account)

    if not network_service.scoping_account == scoping_account:
        raise ResourceAccessDenied(network_service)

    # Huh. I guess.
    network_service.delete()

    return network_service


def get_network_features(
        scoping_account=None,
        filters=None,
    ):
    """
    Get a list of features.
    These might be filtered by network service.

    :param filters: A dict of filters.
    """
    # In case the network service is passed as an object,
    # we use the filter pk. TODO: patch filtering fields to
    # accept objects.
    if filters:
        network_service = filters.get("network_service")
        if network_service and isinstance(network_service, NetworkService):
            filters["network_service"] = network_service.pk

    filtered = NetworkFeatureFilter(filters)

    return filtered.qs


def get_network_feature(
        scoping_account=None,
        network_feature=None,
    ) -> NetworkFeature:
    """
    Retrieve a single feature by it's primary key

    :param feature: The pk of the feature
    """
    # Lookup strategies
    if network_feature and isinstance(network_feature, NetworkFeature):
        return network_feature

    return NetworkFeature.objects.get(pk=network_feature)


def assert_network_feature_available(
        network_feature=None,
        network_service=None,
        scoping_account=None,
    ):
    """
    Check for the availability of a feature in a given network service.

    :raises NetworkFeatureNotAvailable: In case the feature is not available

    :param scoping_account: The managing account for interface homogeny
    :param network_feature: The network feature
    :param network_service: The service to check for feature availability
    """
    if not network_feature in network_service.network_features.all():
        raise NetworkFeatureNotAvailable(network_feature, network_service)

