
"""
Services Models
---------------

Provide models for describing services and service features.
"""

import enumfields
from django.db import models
from django.contrib.postgres import fields as postgres_fields
from polymorphic.models import PolymorphicModel

from jea.crm.models import (
    OwnableMixin,
    ManagedMixin,
    BillableMixin,
    InvoiceableMixin,
    AccountScopedMixin,
)
from jea.ipam.models import AddressFamilies
from jea.access.models import (
    RouteServerSessionMode,
    BGPSessionType,
)
from utils.datastructures import reverse_mapping


#
# Network Service Membership
#
class MemberJoiningRule(
        OwnableMixin,
        ManagedMixin,
        AccountScopedMixin,
        PolymorphicModel,
    ):
    """A rule for members to join a network service."""
    network_service = models.ForeignKey(
        "NetworkService",
        related_name="member_joining_rules",
        on_delete=models.CASCADE)


class BlacklistMemberJoiningRule(MemberJoiningRule):
    """A blacklist member joining rule"""


class WhitelistMemberJoiningRule(MemberJoiningRule):
    """A whitelist member joining rule"""
    capacity_min = models.PositiveIntegerField(null=True)
    capacity_max = models.PositiveIntegerField(null=True)


MEMBER_JOINING_RULE_TYPE_WHITELIST = "whitelist"
MEMBER_JOINING_RULE_TYPE_BLACKLIST = "blacklist"

MEMBER_JOINING_RULE_TYPES = {
    WhitelistMemberJoiningRule: MEMBER_JOINING_RULE_TYPE_WHITELIST,
    BlacklistMemberJoiningRule: MEMBER_JOINING_RULE_TYPE_BLACKLIST,
}
INV_MEMBER_JOINING_RULE_TYPES = reverse_mapping(MEMBER_JOINING_RULE_TYPES)


#
# Network Services
#
class NetworkService(
        OwnableMixin,
        ManagedMixin,
        InvoiceableMixin,
        AccountScopedMixin,
        PolymorphicModel,
    ):
    """Polymorphic network service base"""
    product = models.ForeignKey(
        "catalog.Product",
        related_name="network_services",
        on_delete=models.PROTECT)

    public = models.BooleanField(default=False)

    nsc_required_contact_roles = models.ManyToManyField(
        "crm.Role",
        blank=True,
        related_name="required_by_network_services")

    @property
    def all_nsc_required_contact_roles(self):
        """Contact roles queryset shortcut"""
        return self.nsc_required_contact_roles.all()


class ExchangeLanNetworkService(NetworkService):
    """An exchange lan network service model"""
    name = models.CharField(max_length=40)
    metro_area = models.CharField(max_length=3)

    # External references
    peeringdb_ixid = models.PositiveIntegerField(null=True, blank=True)
    ixfdb_ixid = models.PositiveIntegerField(null=True, blank=True)

    # IP Addresses
    @property
    def network_addresses_v4(self):
        """Get ipv4 network prefixes"""
        return self.ip_addresses.filter(version=4)

    @property
    def network_addresses_v6(self):
        """Get ipv4 network prefixes"""
        return self.ip_addresses.filter(version=6)

    def __str__(self):
        """Make string representation of exchange lan network service"""
        return f"ExchangeLanService: {self.name} ({self.metro_area})"

    def __repr__(self):
        """Make shortrepresentation"""
        return f"<ExchangeLanNetworkService id='{self.pk}' name='{self.name}'>"

    class Meta:
        verbose_name = "ExchangeLAN Network Service"


class ClosedUserGroupNetworkService(NetworkService):
    """
    Deprecated: ClosedUserGroup Network Service
    """
    def __str__(self):
        """Closed user group to string"""
        return "Closed User Group Network Service"

    def __repr__(self):
        """Closed user group representation"""
        return f"<ClosedUserGroupNetworkService id='{self.pk}'>"

    class Meta:
        verbose_name = "Closed UserGroup Network Service"


class ELanNetworkService(BillableMixin, NetworkService):
    """
    E-Lan Network Service

    This is a model for a multi point network, e.g. a
    closed user group.
    """
    name = models.CharField(max_length=120)

    initiator_capacity = models.PositiveIntegerField()
    default_capacity_min = models.PositiveIntegerField(null=True)
    default_capacity_max = models.PositiveIntegerField(null=True)

    def __str__(self):
        """E-Lan to string"""
        return "E-Lan Network Service"

    def __repr__(self):
        """E-Lan representation"""
        return f"<ELanNetworkService id='{self.pk}'>"

    class Meta:
        verbose_name = "E-Lan Network Service"


class ELineNetworkService(BillableMixin, NetworkService):
    """
    E-Line Network Service

    A model for point to point networks.
    """
    name = models.CharField(max_length=120)

    joining_member_account = models.ForeignKey(
        "crm.Account",
        null=False,
        on_delete=models.PROTECT)

    capacity = models.PositiveIntegerField()

    def __str__(self):
        """Eline to string"""
        return "E-Line Network Service"

    def __repr__(self):
        """E-Line Service representation"""
        return f"<ELineNetworkService id='{self.pk}'>"

    class Meta:
        verbose_name = "E-Line Network Service"


class ETreeNetworkService(BillableMixin, NetworkService):
    """
    E-Tree Network Service

    A multi point network service, where multiple leaf nodes
    are connected to one or more roots.
    """
    name = models.CharField(max_length=120)

    initiator_capacity = models.PositiveIntegerField()
    default_capacity_min = models.PositiveIntegerField(null=True)
    default_capacity_max = models.PositiveIntegerField(null=True)

    def __str__(self):
        """E-Tree to string"""
        return "E-Tree Network Service"

    def __repr__(self):
        """E-Tree Service representation"""
        return f"<ETreeNetworkService id='{self.pk}'>"

    class Meta:
        verbose_name = "E-Tree Network Service"


class CloudNetworkService(NetworkService):
    """A Cloud network service model"""
    cloud_provider = models.ForeignKey(
        "catalog.CloudProvider",
        related_name="cloud_network_services",
        on_delete=models.PROTECT)

    @property
    def cloud_provider_name(self):
        """Get the name of the cloud provider"""
        return self.cloud_provider.name

    def __str__(self):
        """Cloud network service to string"""
        if self.cloud_provider_id:
            return f"CloudService: {self.cloud_provider}"

        return "CloudService"

    def __repr__(self):
        """Closed user group representation"""
        return (f"<CloudNetworkService id='{self.pk}' "
                f"cloud_provider='{self.cloud_provider.name}'>")

    class Meta:
        verbose_name = "Cloud Network Service"


SERVICE_TYPE_EXCHANGE_LAN = "exchange_lan"
SERVICE_TYPE_ELAN = "e_lan"
SERVICE_TYPE_ELINE = "e_line"
SERVICE_TYPE_ETREE = "e_tree"
SERVICE_TYPE_CLOUD = "cloud"

SERVICE_TYPES = {
    ExchangeLanNetworkService: SERVICE_TYPE_EXCHANGE_LAN,
    ELanNetworkService: SERVICE_TYPE_ELAN,
    ELineNetworkService: SERVICE_TYPE_ELINE,
    ETreeNetworkService: SERVICE_TYPE_ETREE,
    CloudNetworkService: SERVICE_TYPE_CLOUD,
}

INV_SERVICE_TYPES = reverse_mapping(SERVICE_TYPES)

#
# Features
#

class NetworkFeature(PolymorphicModel):
    """Polymorphic base feature"""
    name = models.CharField(max_length=80)
    required = models.BooleanField()

    network_service = models.ForeignKey(
        NetworkService,
        related_name="network_features",
        on_delete=models.CASCADE)

    nfc_required_contact_roles = models.ManyToManyField(
        "crm.Role",
        blank=True,
        related_name="required_by_network_features")

    @property
    def all_nfc_required_contact_roles(self):
        """Contact roles queryset shortcut"""
        return self.nfc_required_contact_roles.all()


class IXPSpecificFeatureFlag(models.Model):
    """
    A model describing some IXP specifig feature flag.
    Flags have a name and a description.
    """
    name = models.CharField(max_length=20)
    description = models.CharField(max_length=80)

    network_feature = models.ForeignKey(
        NetworkFeature,
        related_name="ixp_specific_flags",
        on_delete=models.CASCADE)

    def __str__(self):
        """Feature flag as string"""
        return f"IXPFeatureFlag: {self.name}"

    def __repr__(self):
        """Feature flag representation"""
        return f"<IXPSpecificFeatureFlag id='{self.pk}' name='{self.name}'>"

    class Meta:
        verbose_name = "IXP-specific feature flag"


class BlackholingNetworkFeature(NetworkFeature):
    """A blackholing feature"""
    def __str__(self):
        """Blackholing Feature as string"""
        return "Blackholing"

    def __repr__(self):
        """Feature flag representation"""
        return f"<BlackholingFeature id='{self.pk}'>"

    class Meta:
       verbose_name = "Blackholing Feature"


class RouteServerNetworkFeature(NetworkFeature):
    """A route server feature model"""
    asn = models.PositiveIntegerField(null=True)
    fqdn = models.CharField(max_length=80)
    looking_glass_url = models.URLField(null=True, blank=True)
    address_families = postgres_fields.ArrayField(
        enumfields.EnumField(AddressFamilies, max_length=10))

    session_mode = enumfields.EnumField(
        RouteServerSessionMode, max_length=20)
    available_bgp_session_types = postgres_fields.ArrayField(
        enumfields.EnumField(BGPSessionType, max_length=20))

    @property
    def ip_addresses_v4(self):
        """Get assigned ipv4 addresses"""
        return self.ip_addresses.filter(version=4)

    @property
    def ip_addresses_v6(self):
        """Get assigned ipv6 addresses"""
        return self.ip_addresses.filter(version=6)

    def __str__(self):
        """RouteServer Feature as string"""
        return f"RouteServer: {self.fqdn}"

    def __repr__(self):
        """RouteServer representation"""
        return f"<RouteServerFeature id='{self.pk}' fqdn='{self.fqdn}'>"

    class Meta:
        verbose_name = "RouteServer Feature"


class IXPRouterNetworkFeature(NetworkFeature):
    """An IXP router feature"""
    asn = models.PositiveIntegerField()
    fqdn = models.CharField(max_length=80)

    @property
    def ip_addresses_v4(self):
        """Get assigned ipv4 addresses"""
        return self.ip_addresses.filter(version=4)

    @property
    def ip_addresses_v6(self):
        """Get assigned ipv6 addresses"""
        return self.ip_addresses.filter(version=6)

    def __str__(self):
        """IXPRouter Feature as string"""
        return f"IXPRouter: {self.fqdn}"

    def __repr__(self):
        """Feature flag representation"""
        return f"<IXPRouterFeature id='{self.pk}' fqdn='{self.fqdn}'>"

    class Meta:
        verbose_name = "IXPRouter Feature"




FEATURE_TYPE_BLACKHOLING = "blackholing"
FEATURE_TYPE_ROUTESERVER = "route_server"
FEATURE_TYPE_IXPROUTER = "ixp_router"

FEATURE_TYPES = {
    BlackholingNetworkFeature: FEATURE_TYPE_BLACKHOLING,
    RouteServerNetworkFeature: FEATURE_TYPE_ROUTESERVER,
    IXPRouterNetworkFeature: FEATURE_TYPE_IXPROUTER,
}

INV_FEATURE_TYPES = reverse_mapping(FEATURE_TYPES)

