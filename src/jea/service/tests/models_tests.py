
"""
Services Models Tests
"""

import pytest
from model_bakery import baker
from django.db.models import ProtectedError

from jea.service.models import (
    MemberJoiningRule,
    BlacklistMemberJoiningRule,
    WhitelistMemberJoiningRule,
    NetworkService,
    ExchangeLanNetworkService,
    ClosedUserGroupNetworkService,
    ELineNetworkService,
    ELanNetworkService,
    ETreeNetworkService,
    CloudNetworkService,
    IXPSpecificFeatureFlag,
    NetworkFeature,
    BlackholingNetworkFeature,
    RouteServerNetworkFeature,
    IXPRouterNetworkFeature,
)

from jea.ipam.models import (
    IpAddress,
)


@pytest.mark.django_db
def test_member_joining_rule():
    """Test a member joining rule"""
    # This should work without failing:
    r1 = baker.make(BlacklistMemberJoiningRule)
    r2 = baker.make(WhitelistMemberJoiningRule,
                    capacity_min=0,
                    capacity_max=None)

    assert r1 in MemberJoiningRule.objects.all()
    assert r2 in MemberJoiningRule.objects.all()


@pytest.mark.django_db
def test_network_services():
    """Test polymorphic network services"""
    s_1 = baker.make(ExchangeLanNetworkService)
    s_2 = baker.make(ClosedUserGroupNetworkService)
    s_3 = baker.make(ELineNetworkService)
    s_4 = baker.make(ClosedUserGroupNetworkService)

    # Find polymorphic by id
    assert NetworkService.objects.get(pk=s_1.id)
    assert NetworkService.objects.get(pk=s_2.id)
    assert NetworkService.objects.get(pk=s_3.id)
    assert NetworkService.objects.get(pk=s_4.id)


@pytest.mark.django_db
def test_network_service_protected_relations():
    """Check protected relations"""
    s_1 = baker.make(ExchangeLanNetworkService)

    consuming_account = s_1.consuming_account
    managing_account = s_1.managing_account
    product = s_1.product

    assert consuming_account, \
        "There should be an assigned owning account"
    assert managing_account, \
        "There should be an assigned billing account"
    assert product, \
        "There should be an assigned product"

    # Deletion should not be possible unless the product referenced
    # service is gone
    with pytest.raises(ProtectedError):
        product.delete()

    with pytest.raises(ProtectedError):
        managing_account.delete()

    with pytest.raises(ProtectedError):
        consuming_account.delete()

    # After deleting the service, we should be able
    # to remove the refences.
    s_1.delete()
    product.delete()
    managing_account.delete()
    consuming_account.delete()


@pytest.mark.django_db
def test_exchange_lan_network_service():
    """Test Exchange Lan Network Service"""
    # Check, that we can assign IP addresses
    service = baker.make(ExchangeLanNetworkService)
    ip = baker.make(IpAddress)
    service.ip_addresses.add(ip)
    baker.make(IpAddress, exchange_lan_network_service=service)

    assert service.ip_addresses.count() == 2 # one is already assigned


@pytest.mark.django_db
def test_exchange_lan_network_service_network_addresses_access():
    """Test Exchange Lan Network Service network address properties"""
    service = baker.make(ExchangeLanNetworkService)
    ip4 = baker.make(IpAddress, version=4)
    ip6 = baker.make(IpAddress, version=6)

    service.ip_addresses.add(ip4)
    service.ip_addresses.add(ip6)

    assert service.ip_addresses.count() == 2

    assert ip4 == service.network_addresses_v4.first()
    assert ip6 == service.network_addresses_v6.first()


@pytest.mark.django_db
def test_cloud_network_service():
    """Test cloud network service"""
    service = baker.make(CloudNetworkService)
    provider = service.cloud_provider

    # Test reference protection
    with pytest.raises(ProtectedError):
        provider.delete()


@pytest.mark.django_db
def test_polymorphic_features():
    """Test polymorphic features model"""
    s = baker.make(NetworkService)

    f_1 = baker.make(BlackholingNetworkFeature, network_service=s)
    f_2 = baker.make(RouteServerNetworkFeature, network_service=s)
    f_3 = baker.make(IXPRouterNetworkFeature, network_service=s)

    assert s.network_features.get(pk=f_1.pk)
    assert s.network_features.get(pk=f_2.pk)
    assert s.network_features.get(pk=f_3.pk)


@pytest.mark.django_db
def test_service_feature_relation():
    """Test feature to service relation"""
    s = baker.make(NetworkService)
    f_1 = baker.make(BlackholingNetworkFeature, network_service=s)

    assert f_1 in list(s.network_features.all())

    # Delete network service, feature should be gone aswell.
    s.delete()

    with pytest.raises(NetworkFeature.DoesNotExist):
        f_1.refresh_from_db()


@pytest.mark.django_db
def test_feature_routes_erver_ipaddresses():
    """Test Routeserver Feature with IP addresses"""
    feature = baker.make(RouteServerNetworkFeature)
    ip_addr = baker.make(IpAddress)
    feature.ip_addresses.add(ip_addr) # Alternative assignement

    assert feature.ip_addresses.count() == 1
    assert ip_addr.route_server_network_feature.pk == feature.pk

@pytest.mark.django_db
def test_feature_route_server_ipaddress_properties():
    """Test ip address property access"""
    feature = baker.make(RouteServerNetworkFeature)
    ip4 = baker.make(IpAddress, version=4)
    ip6 = baker.make(IpAddress, version=6)
    feature.ip_addresses.add(ip4)
    feature.ip_addresses.add(ip6)

    assert feature.ip_addresses_v4.first() == ip4
    assert feature.ip_addresses_v6.first() == ip6

@pytest.mark.django_db
def test_feature_ixp_router():
    """Test Routeserver Feature with IP addresses"""
    ip_addr = baker.make(IpAddress)
    feature = baker.make(IXPRouterNetworkFeature, ip_addresses=[ip_addr])

    assert feature.ip_addresses.count() == 1
    assert ip_addr.ixp_router_network_feature.pk == feature.pk


@pytest.mark.django_db
def test_feature_ixprouter_ipaddress_properties():
    """Test ip address property access"""
    feature = baker.make(IXPRouterNetworkFeature)
    ip4 = baker.make(IpAddress, version=4)
    ip6 = baker.make(IpAddress, version=6)
    feature.ip_addresses.add(ip4)
    feature.ip_addresses.add(ip6)

    assert feature.ip_addresses_v4.first() == ip4
    assert feature.ip_addresses_v6.first() == ip6


@pytest.mark.django_db
def test_eline_network_service__relations():
    """Test eline network service reltations"""
    billing_account = baker.make("crm.Account")
    service = baker.make(
        ELineNetworkService,
        billing_account=billing_account)

    with pytest.raises(ProtectedError):
        billing_account.delete()


@pytest.mark.django_db
def test_network_service__member_joining_rules():
    """Get the member joining rules of a network service"""
    account = baker.make("crm.Account")
    rule = baker.prepare(
        WhitelistMemberJoiningRule,
        consuming_account=account,
        managing_account=account)
    service = baker.make(
        ELanNetworkService,
        consuming_account=account,
        managing_account=account,
        member_joining_rules=[rule])

    assert rule in service.member_joining_rules.all()


@pytest.mark.django_db
def test_elan_network_service__capacity():
    """Test elan network service capacity"""
    baker.make(
        ELanNetworkService,
        default_capacity_min=None,
        default_capacity_max=0)
    baker.make(
        ELanNetworkService,
        default_capacity_min=0,
        default_capacity_max=None)


@pytest.mark.django_db
def test_elan_network_service__relations():
    """Test elan network service relations"""
    billing_account = baker.make("crm.Account")
    service = baker.make(
        ELanNetworkService,
        billing_account=billing_account)
    prefix = baker.make("ipam.IpAddress", elan_network_service=service)

    with pytest.raises(ProtectedError):
        billing_account.delete()

    assert prefix in service.ip_addresses.all()


@pytest.mark.django_db
def test_etree_network_service__relations():
    """Test etree relations"""
    billing_account = baker.make("crm.Account")
    service = baker.make(
        ETreeNetworkService,
        billing_account=billing_account)

    with pytest.raises(ProtectedError):
        billing_account.delete()

