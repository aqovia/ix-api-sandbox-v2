
"""
Test network service
"""

import pytest
from model_bakery import baker

from jea.exceptions import ValidationError
from jea.service.services import network as network_svc
from jea.service import exceptions


@pytest.mark.django_db
def test_get_network_services():
    """Test listing network services"""
    exchange_lan = baker.make("service.ExchangeLanNetworkService")
    closed_user_group = baker.make("service.ClosedUserGroupNetworkService")
    eline = baker.make("service.ELineNetworkService")
    cloud = baker.make("service.CloudNetworkService")

    services = network_svc.get_network_services()

    assert exchange_lan in services
    assert closed_user_group in services
    assert eline in services
    assert cloud in services


@pytest.mark.django_db
def test_get_network_services_filter_type():
    """Test listing network services filtered by type"""
    exchange_lan = baker.make("service.ExchangeLanNetworkService")
    eline = baker.make("service.ELineNetworkService")
    elan = baker.make("service.ELanNetworkService")
    etree = baker.make("service.ETreeNetworkService")

    # Exchange Lan
    services = network_svc.get_network_services(filters={
        "type": "exchange_lan",
    })
    assert exchange_lan in services
    assert not etree in services
    assert not eline in services
    assert not elan in services

    # ELine
    services = network_svc.get_network_services(filters={
        "type": "e_line",
    })
    assert not exchange_lan in services
    assert not elan in services
    assert eline in services
    assert not etree in services

    # ELan
    services = network_svc.get_network_services(filters={
        "type": "e_lan",
    })
    assert not exchange_lan in services
    assert not etree in services
    assert not eline in services
    assert elan in services

    # Etree
    services = network_svc.get_network_services(filters={
        "type": "e_tree",
    })
    assert not exchange_lan in services
    assert etree in services
    assert not eline in services
    assert not elan in services


@pytest.mark.django_db
def test_create_network_service__eline():
    """Test creating an eline network service"""
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    joining_account = baker.make(
        "crm.Account",
        scoping_account=account)
    product = baker.make("catalog.ELineNetworkProduct")

    # Create network service
    network_service = network_svc.create_network_service(
        network_service_request={
            "type": "e_line",
            "managing_account": account,
            "consuming_account": account,
            "billing_account": account,
            "joining_member_account": joining_account,
            "capacity": 1000,
            "product": product,
            "name": "Eline23",
        })

    assert network_service


@pytest.mark.django_db
def test_create_network_service__elan():
    """Test creating an elan network service"""
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    product = baker.make("catalog.ELanNetworkProduct")

    # Create network service
    network_service = network_svc.create_network_service(
        network_service_request={
            "type": "e_lan",
            "managing_account": account,
            "consuming_account": account,
            "billing_account": account,
            "product": product,
            "name": "Elan2000",
            "initiator_capacity": 5000,
            "default_capacity_min": 3000,
            "default_capacity_max": 5000,
            "ip_addresses": None,
        })

    assert network_service


@pytest.mark.django_db
def test_create_network_service__etree():
    """Test creating an elan network service"""
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    product = baker.make("catalog.ETreeNetworkProduct")

    # Create network service
    network_service = network_svc.create_network_service(
        network_service_request={
            "type": "e_tree",
            "managing_account": account,
            "consuming_account": account,
            "billing_account": account,
            "product": product,
            "name": "ETree42",
            "initiator_capacity": 5000,
            "default_capacity_min": 3000,
            "default_capacity_max": 5000,
            "ip_addresses": None,
        })

    assert network_service


@pytest.mark.django_db
def test_create_network_service__unsupported():
    """Test creating an unsupported network service"""
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    product = baker.make("catalog.ExchangeLanNetworkProduct")

    with pytest.raises(Exception):
        network_svc.create_network_service(
            network_service_request={
                "type": "exchange_lan",
                "managing_account": account,
                "consuming_account": account,
                "billing_account": account,
                "product": product,
                "name": "ExchangeLan DUS",
            })


@pytest.mark.django_db
def test_update_network_service__accounts():
    """Test updating a network service"""
    network_service = baker.make("service.ELineNetworkService")
    account = baker.make("crm.Account")

    network_service = network_svc.update_network_service(
        network_service=network_service,
        network_service_update={
            "managing_account": account,
            "consuming_account": account,
        })

    assert network_service.managing_account == account
    assert network_service.consuming_account == account


@pytest.mark.django_db
def test_update_network_service__billing_account():
    """Test updating the billing contact"""
    network_service = baker.make("service.ELineNetworkService")
    billing_account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    non_billing_account = baker.make("crm.Account")

    network_service = network_svc.update_network_service(
        network_service=network_service,
        network_service_update={
            "billing_account": billing_account,
        })

    assert network_service.billing_account == billing_account

    with pytest.raises(ValidationError):
        network_svc.update_network_service(
            network_service=network_service,
            network_service_update={
                "billing_account": non_billing_account,
            })


@pytest.mark.django_db
def test_update_network_service__common_attributes():
    """Common attributes update"""
    network_service = baker.make("service.ELineNetworkService")
    network_service = network_svc.update_network_service(
        network_service=network_service,
        network_service_update={
            "name": "name",
            "external_ref": "external_ref",
            "purchase_order": "purchase_order",
            "contract_ref": "contract_ref",
        })

    assert network_service.name == "name"
    assert network_service.external_ref == "external_ref"
    assert network_service.purchase_order == "purchase_order"
    assert network_service.contract_ref == "contract_ref"


@pytest.mark.django_db
def test_update_network_service__product():
    """Network service product change"""
    updates = [
        (baker.make("service.ELineNetworkService"),
         baker.make("catalog.ELineNetworkProduct")),
        (baker.make("service.ELanNetworkService"),
         baker.make("catalog.ELanNetworkProduct")),
        (baker.make("service.ETreeNetworkService"),
         baker.make("catalog.ETreeNetworkProduct")),
    ]

    for service, product in updates:
        network_service = network_svc.update_network_service(
            network_service=service,
            network_service_update={
                "product": product,
            })

        assert network_service.product == product


@pytest.mark.django_db
def test_update_network_service__elan():
    """Update elan specific fields"""
    network_service = baker.make("service.ELanNetworkService")
    update = {
        "initiator_capacity": 9000,
        "default_capacity_min": 5000,
        "default_capacity_max": None,
    }

    network_service = network_svc.update_network_service(
        network_service=network_service,
        network_service_update=update)

    assert network_service.initiator_capacity == \
        update["initiator_capacity"]
    assert network_service.default_capacity_min == \
        update["default_capacity_min"]
    assert network_service.default_capacity_max == \
        update["default_capacity_max"]


@pytest.mark.django_db
def test_update_network_service__etree():
    """Update etree specific fields"""
    network_service = baker.make("service.ETreeNetworkService")

    # These are pretty much the same as with an elan
    update = {
        "initiator_capacity": 9000,
        "default_capacity_min": 5000,
        "default_capacity_max": None,
    }

    network_service = network_svc.update_network_service(
        network_service=network_service,
        network_service_update=update)

    assert network_service.initiator_capacity == \
        update["initiator_capacity"]
    assert network_service.default_capacity_min == \
        update["default_capacity_min"]
    assert network_service.default_capacity_max == \
        update["default_capacity_max"]


@pytest.mark.django_db
def test__assign_ip_addresses():
    """Test ip address assign helper"""
    network_service = baker.make(
        "service.ELanNetworkService")
    ip4 = baker.make(
        "ipam.IpAddress",
        address="23.42.0.0",
        version=4,
        prefix_length=23)
    ip6 = baker.make(
        "ipam.IpAddress",
        address="fd42::0",
        version=6,
        prefix_length=48)

    network_svc._assign_ip_addresses(
        network_service=network_service,
        ip_addresses=[ip4, ip6])

    assert ip4 in network_service.ip_addresses.all()
    assert ip6 in network_service.ip_addresses.all()


@pytest.mark.django_db
def test__assign_ip_addresses__release():
    """Test ip address assign helper"""
    network_service = baker.make(
        "service.ELanNetworkService")
    ip4 = baker.make(
        "ipam.IpAddress",
        address="23.42.0.0",
        version=4,
        prefix_length=23,
        elan_network_service=network_service)
    ip6 = baker.make(
        "ipam.IpAddress",
        address="fd42::0",
        version=6,
        prefix_length=48,
        elan_network_service=network_service)

    # Update
    network_svc._assign_ip_addresses(
        network_service=network_service,
        ip_addresses=[ip6])

    assert not ip4 in network_service.ip_addresses.all()
    assert ip6 in network_service.ip_addresses.all()

    # Clear all
    network_svc._assign_ip_addresses(
        network_service=network_service,
        ip_addresses=[])

    assert not ip4 in network_service.ip_addresses.all()
    assert not ip6 in network_service.ip_addresses.all()


@pytest.mark.django_db
def test_get_network_service__by_service_id():
    """Test getting a single service by id"""
    service = baker.make("service.ExchangeLanNetworkService")
    service_ = network_svc.get_network_service(
        network_service=str(service.pk))
    assert service.pk == service_.pk

    with pytest.raises(Exception):
        network_svc.get_network_service()


@pytest.mark.django_db
def test_get_network_features():
    """Get all the network features"""
    feature = baker.make("service.RouteServerNetworkFeature")
    features = network_svc.get_network_features()
    assert feature in features

    # TODO: Add tests for filtering

@pytest.mark.django_db
def test_get_network_feature__cached():
    """Get a feature"""
    feature = baker.make("service.RouteServerNetworkFeature")
    result = network_svc.get_network_feature(
        network_feature=feature)

    assert result == feature


@pytest.mark.django_db
def test_get_network_feature__cached():
    """Get a feature"""
    feature = baker.make("service.RouteServerNetworkFeature")
    result = network_svc.get_network_feature(
        network_feature=feature.pk)

    assert result == feature


@pytest.mark.django_db
def test_assert_network_feature_available():
    """Test availability assertion for a given network feature"""
    s1 = baker.make("service.ExchangeLanNetworkService")
    s2 = baker.make("service.ExchangeLanNetworkService")
    f1 = baker.make("service.BlackholingNetworkFeature",
                     network_service=s1)
    f2 = baker.make("service.BlackholingNetworkFeature",
                    network_service=s2)

    # Network feature is available:
    network_svc.assert_network_feature_available(f1, s1)

    # Feature is not available
    with pytest.raises(exceptions.NetworkFeatureNotAvailable):
        network_svc.assert_network_feature_available(f2, s1)

