
"""
Service Access Models
---------------------

Access to services is granted to accounts through
these models.
"""

import enumfields
from django.db import models
from django.contrib.postgres import fields as postgres_fields
from polymorphic.models import PolymorphicModel
from ixapi_schema.v1.constants.access import (
    BGPSessionType,
    RouteServerSessionMode,
    ConnectionMode,
    LACPTimeout,
)
from ixapi_schema.v2.constants.access import (
    VLanType,
    VLanEthertype,
)

from utils.datastructures import reverse_mapping
from jea.ipam.models import AddressFamilies
from jea.crm.models import (
    OwnableMixin,
    ManagedMixin,
    BillableMixin,
    InvoiceableMixin,
    AccountScopedMixin,
)
from jea.eventmachine.models import StatefulMixin
from jea.catalog.models import (
    MediaType,
)


class Connection(
        StatefulMixin,
        OwnableMixin,
        ManagedMixin,
        BillableMixin,
        InvoiceableMixin,
        AccountScopedMixin,
        models.Model,
    ):
    """
    A model for connections.

    Connections describe the combination of several demarcs
    into a, well, connection. This connection can for example
    be a LAG.

    Remember to require a `billing` and `implementation` contact
    in the contacts array when creating.
    """
    name = models.CharField(max_length=255)

    lacp_timeout = enumfields.EnumField(
        LACPTimeout,
        max_length=4,
        null=True,
        blank=True)

    mode = enumfields.EnumField(ConnectionMode, max_length=16)
    speed = models.PositiveIntegerField(null=True, blank=True)

    def __str__(self):
        """Connection to String"""
        return (f"Connection: {self.name}, "
                f"Mode: {self.mode} "
                f"LACP: {self.lacp_timeout}")

    def __repr__(self):
        """Connection representation"""
        return (f"<Connection id='{self.pk}' name='{self.name}' "
                f"mode='{self.mode}' lacp='{self.lacp_timeout}'>")


class VLan(models.Model):
    """
    A model for a vlan:
    A VLan has an ethertype and an optional name.
    """
    ethertype = enumfields.EnumField(
        VLanEthertype,
        max_length=6,
        default="0x8100")
    number = models.PositiveIntegerField()
    name = models.CharField(
        max_length=128,
        null=True,
        blank=False)

    # VLan Configuration
    vlan_config = models.ForeignKey(
        "VLanConfig",
        related_name="_vlans",
        null=True,
        on_delete=models.CASCADE)

    def __str__(self):
        """VLan to string"""
        return f"VLan {self.number}, {self.ethertype}"

    def __repr__(self):
        """VLan representation"""
        return (f"<VLan id='{self.pk}' name='{self.name}' "
                f"number='{self.number}' ethertype='{self.ethertype}'>")


class VLanConfig(
        AccountScopedMixin,
        models.Model,
    ):
    """
    A VLan configuration for a NetworkService, used in a
    NetworkServiceConfig.
    """
    vlan_type = enumfields.EnumField(VLanType)

    outer_vlan = models.ForeignKey(
        VLan,
        null=True,
        on_delete=models.SET_NULL)

    @property
    def vlans(self):
        """
        Return _iterable_ list of vlans. This is
        required for serialization.
        """
        return self._vlans.all()


class DemarcationPoint(
        StatefulMixin,
        OwnableMixin,
        ManagedMixin,
        BillableMixin,
        InvoiceableMixin,
        AccountScopedMixin,
        models.Model,
    ):
    """
    A model for the (port) demarcation point.
    This represents the demarc assigned to a account.
    """
    name = models.CharField(max_length=255)

    media_type = models.CharField(max_length=20)
    speed = models.PositiveIntegerField(null=True, blank=True)

    # Billing account

    # Other Relations
    point_of_presence = models.ForeignKey(
        "catalog.PointOfPresence",
        on_delete=models.PROTECT,
        related_name="demarcation_points")

    connection = models.ForeignKey(
        Connection,
        on_delete=models.PROTECT,
        null=True, blank=True,
        related_name="demarcation_points")


    def __str__(self):
        """Demarc as String"""
        return f"PortDemarc: {self.name}"

    def __repr__(self):
        """Demarc representation"""
        return (f"<DemarcationPoint id='{self.pk}' "
                f"name='{self.name}' "
                f"consumed_by='{self.consuming_account_id}' "
                f"managed_by='{self.managing_account_id}'>")

    class Meta:
        verbose_name = "Demarc"

#
# Service Access Models
#

class NetworkServiceConfig(
        StatefulMixin,
        OwnableMixin,
        ManagedMixin,
        BillableMixin,
        InvoiceableMixin,
        AccountScopedMixin,
        PolymorphicModel,
    ):
    """
    Polymorphic NetworkService access model.

    Network access is granted by creating an access object
    associated with a billing and owning account.

    The model references `service.NetworkService`.
    """
    capacity = models.PositiveIntegerField(null=True, blank=True)

    # Relations
    connection = models.ForeignKey(
        Connection,
        related_name="network_service_configs",
        on_delete=models.PROTECT)

    network_service = models.ForeignKey(
        "service.NetworkService",
        related_name="configs",
        on_delete=models.PROTECT)

    vlan_config = models.ForeignKey(
        VLanConfig,
        null=True,
        related_name="network_service_configs",
        on_delete=models.PROTECT)

    # Legacy compatibility
    @property
    def inner_vlan(self):
        """
        LEGACY: Get inner vlan from vlan configuration
        """
        if not self.vlan_config:
            return None

        inner_vlan = self.vlan_config.vlans.first()
        if not inner_vlan:
            return None

        return inner_vlan.number

    @property
    def outer_vlan(self):
        """
        LEGACY: Get outer vlan configuration
        """
        if not self.vlan_config:
            return None

        outer_vlan = self.vlan_config.outer_vlan
        if not outer_vlan:
            return None

        return outer_vlan.number


class ExchangeLanNetworkServiceConfig(NetworkServiceConfig):
    """
    An ExchangeLAN network service config model.

    Grant access to a an exchange lan and configure features.
    """
    asns = postgres_fields.ArrayField(
        models.PositiveIntegerField(),
        default=list,
        size=20)
    listed = models.BooleanField(default=True)

    def __str__(self):
        """String from exchange lan network service access"""
        return f"ExchangeLAN NetworkService Configuration for ASNs: {self.asns}"

    def __repr__(self):
        """ExchangeLanNetworkServiceAccess representation"""
        return (f"<ExchangeLanNetworkServiceConfig id='{self.pk}' "
                f"asns='{self.asns}'>")

    class Meta:
        verbose_name = "ExchangeLAN NetworkService Config"


class ClosedUserGroupNetworkServiceConfig(NetworkServiceConfig):
    """
    A config model for closed user groups.

    The closed user group only references mac addresses.
    """
    def __str__(self):
        """String from closed user group service access"""
        return f"ClosedUserGroup NetworkService Configuration (Id: {self.pk})"

    def __repr__(self):
        """ClosedUserGroupNetworkServiceAccess representation"""
        return f"<ClosedUserGroupNetworkServiceConfig id='{self.pk}'>"

    class Meta:
        verbose_name = "ClosedUserGroup NetworkService Config"


class ELineNetworkServiceConfig(NetworkServiceConfig):
    """
    A config model for an e-line network service.
    """
    def __str__(self):
        """String from e-line service config"""
        return f"E-Line NetworkService Configuration (Id: {self.pk})"

    def __repr__(self):
        """E-Line Access representation"""
        return f"<ELineNetworkServiceConfig id='{self.pk}'>"

    class Meta:
        verbose_name = "E-Line NetworkService Config"


class ELanNetworkServiceConfig(NetworkServiceConfig):
    """
    A config model for an e-lan network service.
    """
    def __str__(self):
        """String from e-lan service config"""
        return f"E-Lan NetworkService Configuration (Id: {self.pk})"

    def __repr__(self):
        """E-Lan Access representation"""
        return f"<ELanNetworkServiceConfig id='{self.pk}'>"

    class Meta:
        verbose_name = "E-Lan NetworkService Config"


class ETreeNetworkServiceConfig(NetworkServiceConfig):
    """
    A config model for an e-tree network service.
    """
    def __str__(self):
        """String from e-tree service config"""
        return f"E-Tree NetworkService Configuration (Id: {self.pk})"

    def __repr__(self):
        """E-Tree Access representation"""
        return f"<ETreeNetworkServiceConfig id='{self.pk}'>"

    class Meta:
        verbose_name = "E-Tree NetworkService Config"


class CloudNetworkServiceConfig(NetworkServiceConfig):
    """
    A cloud network service config model.
    """
    cloud_key = models.CharField(max_length=512)

    def __str__(self):
        """String from cloud service config"""
        return f"Cloud NetworkService Configuration (Id: {self.pk})"

    def __repr__(self):
        """E-Line Access representation"""
        return f"<CloudNetworkServiceConfig id='{self.pk}'>"

    class Meta:
        verbose_name = "Cloud NetworkService Config"


NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN = "exchange_lan"
NETWORK_SERVICE_CONFIG_TYPE_CLOSED_USER_GROUP = "closed_user_group"
NETWORK_SERVICE_CONFIG_TYPE_CLOUD = "cloud"
NETWORK_SERVICE_CONFIG_TYPE_ELINE = "eline"

NETWORK_SERVICE_CONFIG_TYPES = {
    ExchangeLanNetworkServiceConfig:
        NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN,
    ClosedUserGroupNetworkServiceConfig:
        NETWORK_SERVICE_CONFIG_TYPE_CLOSED_USER_GROUP,
    ELineNetworkServiceConfig:
        NETWORK_SERVICE_CONFIG_TYPE_ELINE,
    CloudNetworkServiceConfig:
        NETWORK_SERVICE_CONFIG_TYPE_CLOUD,
}

INV_NETWORK_SERVICE_CONFIG_TYPES = reverse_mapping(
    NETWORK_SERVICE_CONFIG_TYPES,
)

#
# Feature Access Configuration
#
class NetworkFeatureConfig(
        StatefulMixin,
        OwnableMixin,
        ManagedMixin,
        BillableMixin,
        InvoiceableMixin,
        AccountScopedMixin,
        PolymorphicModel,
    ):
    """
    The polymorphic feature access base model.

    Access to features like route servers are granted here.
    The configuration is stored in the specific feature
    access object.
    """
    # Relations
    network_feature = models.ForeignKey(
        "service.NetworkFeature",
        on_delete=models.PROTECT,
        related_name="configs")

    network_service_config = models.ForeignKey(
        NetworkServiceConfig,
        on_delete=models.CASCADE,
        related_name="network_feature_configs")


class RouteServerNetworkFeatureConfig(NetworkFeatureConfig):
    """
    Configure RouteServer Access

    This model contains everything required for granting
    accounts access to a RS.
    """
    asn = models.PositiveIntegerField()
    password = models.CharField(max_length=128, blank=True)
    as_set_v4 = models.CharField(max_length=100, null=True)
    as_set_v6 = models.CharField(max_length=100, null=True)
    max_prefix_v4 = models.PositiveIntegerField(
        null=True,
        default=None)
    max_prefix_v6 = models.PositiveIntegerField(
        null=True,
        default=None)
    insert_ixp_asn = models.BooleanField()

    session_mode = enumfields.EnumField(
        RouteServerSessionMode, max_length=20)

    bgp_session_type = enumfields.EnumField(
        BGPSessionType, max_length=10)

    def __str__(self):
        """Routeserver Feature Access Configuration as String"""
        return f"RouteServer FeatureConfiguration for AS{self.asn}"

    def __repr__(self):
        """Representation of feature config"""
        return (f"<RouteServerFeatureConfig id='{self.pk}' "
                f"consuming_account_id='{self.consuming_account_id}'>")

    class Meta:
        verbose_name = "RouteServer FeatureAccess"
        verbose_name_plural = "RouteServer FeatureAccesses"


class IXPRouterNetworkFeatureConfig(NetworkFeatureConfig):
    """
    Configure IXPRouter Access

    Grants a account access to the IXP Router feature.
    """
    password = models.CharField(max_length=128, blank=True)
    max_prefix = models.PositiveIntegerField(default=5000)
    bgp_session_type = enumfields.EnumField(
        BGPSessionType, max_length=10)

    def __str__(self):
        """IXP Router Feature Configuration as String"""
        return f"IXPRouter NetworkFeatureConfig, Account: {self.consuming_account_id}"

    def __repr__(self):
        """Representation of feature config"""
        return (f"<IXPRouterNetworkFeatureConfig id='{self.pk}' "
                f"consuming_account_id='{self.consuming_account_id}'>")

    class Meta:
        verbose_name = "IXPRouter Feature Config"


class BlackholingNetworkFeatureConfig(NetworkFeatureConfig):
    """
    Blackholing Feature Config

    Configure the blackholing feature
    """
    activated = models.BooleanField(default=False)
    ixp_specific_configuration = models.TextField(
        max_length=2048, blank=True, null=True)

    def __str__(self):
        """Blackholing Feature Configuration as String"""
        return (f"Blackholing NetworkFeatureConfig, "
                f"Account: {self.consuming_account_id}")

    def __repr__(self):
        """Representation of feature access"""
        return (f"<BlackholingNetworkFeatureConfig id='{self.pk}' "
                f"consuming_account_id='{self.consuming_account_id}'>")

    class Meta:
        verbose_name = "Blackholing Feature Config"


NETWORK_FEATURE_CONFIG_TYPE_BLACKHOLING = "blackholing"
NETWORK_FEATURE_CONFIG_TYPE_ROUTESERVER = "route_server"
NETWORK_FEATURE_CONFIG_TYPE_IXPROUTER = "ixp_router"

NETWORK_FEATURE_CONFIG_TYPES = {
    BlackholingNetworkFeatureConfig:
        NETWORK_FEATURE_CONFIG_TYPE_BLACKHOLING,
    RouteServerNetworkFeatureConfig:
        NETWORK_FEATURE_CONFIG_TYPE_ROUTESERVER,
    IXPRouterNetworkFeatureConfig:
        NETWORK_FEATURE_CONFIG_TYPE_IXPROUTER,
}

INV_NETWORK_FEATURE_CONFIG_TYPES = reverse_mapping(
    NETWORK_FEATURE_CONFIG_TYPES)

