
"""
JEA Access Models Tests
"""

import pytest
from django.db.models import ProtectedError, ObjectDoesNotExist
from model_bakery import baker

from jea.access.models import (
    Connection,
    DemarcationPoint,
    NetworkServiceConfig,
    ExchangeLanNetworkServiceConfig,
    ClosedUserGroupNetworkServiceConfig,
    ELineNetworkServiceConfig,
    CloudNetworkServiceConfig,
    NetworkFeatureConfig,
    RouteServerNetworkFeatureConfig,
    IXPRouterNetworkFeatureConfig,
    BlackholingNetworkFeatureConfig,
    VLan,
    VLanConfig,
)


@pytest.mark.django_db
def test_connection():
    """Test connection model"""
    account = baker.make("crm.Account")

    billing_role = baker.make("crm.Role", name="biling")
    implementation_role = baker.make("crm.Role", name="implementation")

    contact = baker.make(
        "crm.Contact", roles=[implementation_role, billing_role])

    connection = baker.make(Connection,
                            scoping_account=account,
                            managing_account=account,
                            consuming_account=account,
                            billing_account=account,
                            contacts=[contact])

    # We now have a connection, let's check our relations
    # - Contacts
    assert connection in contact.connections.all()

    # - Accounts
    scoping = connection.scoping_account
    assert scoping

    managing = connection.managing_account
    assert managing
    assert connection in managing.managed_connections.all()

    owning = connection.consuming_account
    assert owning
    assert connection in owning.consumed_connections.all()

    billing = connection.billing_account
    assert billing
    assert connection in billing.billed_connections.all()


def test_connection_representations():
    """Test __str__ and __repr__"""
    connection = baker.prepare(Connection)
    assert isinstance(str(connection), str), "Should return a string"
    assert isinstance(repr(connection), str), "Should return a string"


def test_vlan___str__():
    """Test to str method"""
    vlan = baker.prepare(VLan)
    assert vlan.__str__()


def test_vlan___repr__():
    """Test to repr method"""
    vlan = baker.prepare(VLan)
    assert vlan.__repr__()


def test_vlan_config___str__():
    """Test to str method"""
    vlan_config = baker.prepare(VLanConfig)
    assert vlan_config.__str__()


def test_vlan_config___repr__():
    """Test to repr method"""
    vlan_config = baker.prepare(VLanConfig)
    assert vlan_config.__repr__()


@pytest.mark.django_db
def test_vlan_config___relations():
    """Test vlan relations"""
    outer_vlan = baker.make(VLan)
    vlan_config = baker.make(
        VLanConfig,
        outer_vlan=outer_vlan)
    inner_vlan = baker.make(
        VLan,
        vlan_config=vlan_config)

    # Check, that our vlan is part of the inner vlan list
    assert inner_vlan in vlan_config.vlans

    # Remove outer vlan
    outer_vlan.delete()
    vlan_config.refresh_from_db()
    assert not vlan_config.outer_vlan

    # Delete config and remove inner vlan
    vlan_config.delete()
    assert not inner_vlan in VLan.objects.all()


@pytest.mark.django_db
def test_demarcation_point():
    """Test the demarcation point model"""
    pop = baker.make("catalog.PointOfPresence")
    account = baker.make("crm.Account")
    contact = baker.make("crm.Contact")
    connection = baker.make("access.Connection")
    demarc = baker.make(
        DemarcationPoint,
        connection=connection,
        point_of_presence=pop,
        managing_account=account,
        consuming_account=account,
        scoping_account=account,
        billing_account=account,
        contacts=[contact])

    # Test relations
    assert demarc in pop.demarcation_points.all()
    assert demarc in account.managed_demarcationpoints.all()
    assert demarc in account.consumed_demarcationpoints.all()
    assert demarc in account.billed_demarcationpoints.all()
    assert demarc in contact.demarcationpoints.all()

    # A demarcation point needs to be released before the
    # connection is removed.
    with pytest.raises(ProtectedError):
        connection.delete()


def test_demarcation_point_representation():
    """Test to string and repr methods"""
    demarc = baker.prepare(DemarcationPoint)
    assert str(demarc), "Should return a string"
    assert repr(demarc), "Should return a string"


#
# Test Access Configuration Objects
#

@pytest.mark.django_db
def test_network_service_config():
    """Test polymorphic network service config model"""
    config = baker.make(NetworkServiceConfig)
    managing = config.managing_account
    owning = config.consuming_account
    connection = config.connection

    contact = baker.make("crm.Contact")
    config.contacts.add(contact)

    # Check references
    assert config in set(managing.managed_networkserviceconfigs.all())
    assert config in set(owning.consumed_networkserviceconfigs.all())
    assert config in set(connection.network_service_configs.all())
    assert config in set(contact.networkserviceconfigs.all())


@pytest.mark.django_db
def test_exchange_lan_network_service_config():
    """Test exchange lan network service config model"""
    config = baker.make(ExchangeLanNetworkServiceConfig)
    ip_address = baker.make("ipam.IpAddress")
    config.ip_addresses.add(ip_address)

    # Check ip address relation
    assert ip_address in set(config.ip_addresses.all())
    assert config == ip_address.exchange_lan_network_service_config

    # Check mac address list
    mac_address = baker.make("ipam.MacAddress",
        managing_account=config.managing_account,
        consuming_account=config.consuming_account)
    config.mac_addresses.add(mac_address)

    assert mac_address in config.mac_addresses.all()


def test_exchange_lan_network_service_config_representation():
    """Test exchange lan network service config representation"""
    config = baker.prepare(ExchangeLanNetworkServiceConfig)
    assert str(config), "Should return a string"
    assert repr(config), "Should return a string"


@pytest.mark.django_db
def test_closed_user_group_network_service_config():
    """Test closed usergroup config model"""
    config = baker.make(ClosedUserGroupNetworkServiceConfig)

    # Closed usere groups have a mac address list
    mac_address = baker.make("ipam.MacAddress",
        managing_account=config.managing_account,
        consuming_account=config.consuming_account)
    config.mac_addresses.add(mac_address)

    assert mac_address in set(config.mac_addresses.all())


def test_closed_user_group_network_service_config_representation():
    """Test closed user group config representation"""
    config = baker.prepare(ClosedUserGroupNetworkServiceConfig)
    assert str(config), "Should return a string"
    assert repr(config), "Should return a string"


@pytest.mark.django_db
def test_eline_network_service_config():
    """Test eline network service config model"""
    # Nothing special with this model, but it should be
    # at least creatable.
    config = baker.make(ELineNetworkServiceConfig)
    assert config.pk, "Config Object should have been saved"


def test_eline_network_serivce_config_representation():
    """Test Eline network service config representation"""
    config = baker.prepare(ELineNetworkServiceConfig)
    assert str(config), "Should return a (non empty) string"
    assert repr(config), "Should return a (non empty) string"


@pytest.mark.django_db
def test_cloud_network_service_config():
    """Test eline network service config model"""
    # Nothing special with this model, but it should be
    # at least creatable and check the preesence of the cloud_key
    config = baker.make(CloudNetworkServiceConfig)
    assert config.pk, "Config Object should have been saved"
    assert config.cloud_key


def test_cloud_network_service_config_representation():
    """Test representation of config object"""
    config = baker.prepare(CloudNetworkServiceConfig)
    assert str(config), "Should return a (non empty) string"
    assert repr(config), "Should return a (non empty) string"


#
# Test Feature Configuration
#

@pytest.mark.django_db
def test_network_feature_config():
    """Test polymorphic feature config base"""
    account = baker.make("crm.Account")
    service_config = baker.make(ExchangeLanNetworkServiceConfig)
    config = baker.make(NetworkFeatureConfig,
                        network_service_config=service_config,
                        managing_account=account,
                        consuming_account=account)

    # Check relations and reverse relations
    assert config in service_config.network_feature_configs.all()
    assert config in account.managed_networkfeatureconfigs.all()
    assert config in account.consumed_networkfeatureconfigs.all()


@pytest.mark.django_db
def test_route_server_network_feature_config():
    """Test route server feature config model"""
    config = baker.make(RouteServerNetworkFeatureConfig)

    # All relations have been checked in the base model,
    # so we should make sure, that this just saves
    assert config.pk, "A primary key should have been assigned."


def test_route_server_network_feature_config_representation():
    """Test make representation of feature config"""
    config = baker.prepare(RouteServerNetworkFeatureConfig)
    assert str(config), "Should return a (non empty) string"
    assert repr(config), "Should return a (non empty) string"


@pytest.mark.django_db
def test_ixp_router_network_feature_config():
    """Test IXP router feature config model"""
    config = baker.make(IXPRouterNetworkFeatureConfig)
    assert config.pk, "A primary key should have been assigned."


def test_ixp_router_network_feature_config_representation():
    """Test make representation of the feature config"""
    config = baker.prepare(IXPRouterNetworkFeatureConfig)
    assert str(config), "Should return a (non empty) string"
    assert repr(config), "Should return a (non empty) string"


@pytest.mark.django_db
def test_blackholing_network_feature_config():
    """Test route server feature config model"""
    config = baker.make(BlackholingNetworkFeatureConfig)
    assert config.pk, "A primary key should have been assigned."

    # Blackholing has a list of prefices:
    prefix_a = baker.make("ipam.IpAddress")
    prefix_b = baker.make("ipam.IpAddress")

    config.filtered_prefixes.add(prefix_a)
    config.filtered_prefixes.add(prefix_b)

    # Check relations
    assert prefix_a in set(config.filtered_prefixes.all())
    assert prefix_b.blackholing_network_feature_config == config

    # Check cleaning up models
    config.delete()
    with pytest.raises(ObjectDoesNotExist):
        prefix_a.refresh_from_db()


def test_blackholing_feature_config_representation():
    """Test make representation of blackholing feature config"""
    config = baker.prepare(BlackholingNetworkFeatureConfig)
    assert str(config), "Should return a (non empty) string"
    assert repr(config), "Should return a (non empty) string"

