
"""
Test status message creation
"""

import pytest
from model_bakery import baker

from jea.access import status

@pytest.mark.django_db
def test_mac_address_missing():
    """Test mac address missing feature"""
    config = baker.make("access.ExchangeLanNetworkServiceConfig")
    status_message = status.mac_address_missing(config)

    assert status_message


@pytest.mark.django_db
def test_route_server_network_feature_missing():
    """Test network feature missing message creator"""
    config = baker.make("access.ExchangeLanNetworkServiceConfig")
    feature = baker.make("service.RouteServerNetworkFeature")

    status_message = status.route_server_network_feature_missing(
        config, feature)

    assert status_message 


