
"""
Test config service
"""

import pytest
from model_bakery import baker
from ixapi_schema.v2.entities import access as access_entities
from ixapi_schema.v2.constants.access import VLanType, VLanEthertype

from jea.exceptions import ResourceAccessDenied
from jea.crm.exceptions import (
    RequiredContactRolesInvalid,
)
from jea.crm.services import contacts as contacts_svc
from jea.access.services import (
    configs as configs_svc,
)
from jea.access.models import (
    VLan,
    NetworkServiceConfig,
    BGPSessionType,
    RouteServerSessionMode,
)
from jea.service.exceptions import NetworkFeatureNotAvailable


@pytest.mark.django_db
def test_assign_network_service_contacts():
    """Test (implementation) contact assignment"""
    impl_role = baker.make("crm.Role", name="impl")
    service = baker.make(
        "service.ExchangeLanNetworkService",
        nsc_required_contact_roles=[impl_role])
    config = baker.make(
        "access.ExchangeLanNetworkServiceConfig",
        network_service=service)
    contact = baker.make(
        "crm.Contact",
        roles=[impl_role],
        scoping_account=config.scoping_account)

    assert not contact in config.contacts.all()

    # Assign network service config
    configs_svc.assign_network_service_contacts(
        network_service_config=config,
        contacts=[contact])
    assert contact in config.contacts.all()


@pytest.mark.django_db
def test_assign_network_service_contacts__ownership_fails():
    """
    Test (implementation) contact assignment
    with a failing ownership test.
    """
    # Create a config and an implementation contact.
    # Both have different scoping_accounts.
    c1 = baker.make("crm.Account")
    c2 = baker.make("crm.Account")
    service = baker.make(
        "service.ExchangeLanNetworkService",
        nsc_required_contact_roles=[
            contacts_svc.get_default_role("implementation"),
        ])
    config = baker.make(
        "access.ExchangeLanNetworkServiceConfig",
        network_service=service,
        scoping_account=c1)
    contact = baker.make(
        "crm.Contact",
        roles=[contacts_svc.get_default_role("implementation")],
        scoping_account=c2)

    assert config.scoping_account != contact.scoping_account

    # Assign config
    with pytest.raises(ResourceAccessDenied):
        configs_svc.assign_network_service_contacts(
            scoping_account=config.scoping_account,
            network_service_config=config,
            contacts=[contact])


@pytest.mark.django_db
def test_assign_network_service_contacts__ownership_override():
    """
    Test (implementation) contact assignment with
    ownership override.
    """
    role = contacts_svc.get_default_role("noc")
    service = baker.make(
        "service.ExchangeLanNetworkService",
        nsc_required_contact_roles=[role])
    config = baker.make(
        "access.ExchangeLanNetworkServiceConfig",
        network_service=service)
    contact = baker.make(
        "crm.Contact",
        roles=[role],
        scoping_account=config.scoping_account)

    assert not contact in config.contacts.all()

    # Assign config
    configs_svc.assign_network_service_contacts(
        scoping_account=config.scoping_account,
        network_service_config=config,
        contacts=[contact])

    assert contact in config.contacts.all()


@pytest.mark.django_db
def test_assign_network_service_contacts__clear():
    """Test removal of all (implementation) contacts from object"""
    role = baker.make("crm.Role")
    contact = baker.make("crm.Contact")
    service = baker.make(
        "service.ExchangeLanNetworkService",
        nsc_required_contact_roles=[])
    config = baker.make(
        "access.ExchangeLanNetworkServiceConfig",
        network_service=service,
        scoping_account=contact.scoping_account,
        contacts=[contact])

    assert contact in config.contacts.all()

    # Assign config
    configs_svc.assign_network_service_contacts(
        scoping_account=config.scoping_account,
        network_service_config=config,
        contacts=[])

    assert not contact in config.contacts.all()
    assert not config.contacts.count()


@pytest.mark.django_db
def test_assign_mac_addresses():
    """Test assigning mac addresses to a service config"""
    config = baker.make(
        "access.ExchangeLanNetworkServiceConfig")
    mac = baker.make(
        "ipam.MacAddress",
        scoping_account=config.scoping_account,
        managing_account=config.managing_account,
        consuming_account=config.consuming_account)

    assert not mac in config.mac_addresses.all()

    # Assign mac
    configs_svc.assign_mac_addresses(
        network_service_config=config,
        mac_addresses=[mac])

    assert mac in config.mac_addresses.all()


@pytest.mark.django_db
def test_assign_mac_addresses__invalid_config_type():
    """Test assigning a mac address to an unsupport network service config"""
    config = baker.make("access.CloudNetworkServiceConfig")
    mac = baker.make("ipam.MacAddress")

    with pytest.raises(TypeError):
        configs_svc.assign_mac_addresses(
            network_service_config=config,
            mac_addresses=[mac])


@pytest.mark.django_db
def test_assign_mac_addresses__ownership():
    """Test mac addrss assignment with ownership fail and override"""
    c1 = baker.make("crm.Account")
    c2 = baker.make("crm.Account")
    config = baker.make(
        "access.ExchangeLanNetworkServiceConfig",
        scoping_account=c1)
    mac = baker.make(
        "ipam.MacAddress",
        scoping_account=c2)

    # Preflight check
    assert mac not in config.mac_addresses.all()

    with pytest.raises(ResourceAccessDenied):
        configs_svc.assign_mac_addresses(
            network_service_config=config,
            mac_addresses=[mac])

    # Override config ownership
    configs_svc.assign_mac_addresses(
        scoping_account=mac.scoping_account,
        network_service_config=config,
        mac_addresses=[mac])

    assert mac in config.mac_addresses.all()


@pytest.mark.django_db
def test_assign_mac_addresses__clear():
    """Test removal of mac addresses"""
    mac = baker.make("ipam.MacAddress")
    config = baker.make(
        "access.ExchangeLanNetworkServiceConfig",
        mac_addresses=[mac])

    # Clear
    configs_svc.assign_mac_addresses(
        network_service_config=config)

    assert not mac in config.mac_addresses.all()
    assert not config.mac_addresses.count()


@pytest.mark.django_db
def test_get_network_service_configs():
    """Test getting network service configs"""
    c1 = baker.make("crm.Account")
    c2 = baker.make("crm.Account")

    s1 = baker.make(
        "access.ExchangeLanNetworkServiceConfig",
        scoping_account=c1)
    s2 = baker.make(
        "access.ClosedUserGroupNetworkServiceConfig",
        scoping_account=c2)

    configs = configs_svc.get_network_service_configs(
        scoping_account=c1)

    assert s1 in configs
    assert not s2 in configs


@pytest.mark.django_db
def test_get_network_service_configs__filters__type():
    """Test getting network service configs with filtering"""
    c1 = baker.make("crm.Account")

    # Make some configs
    s1 = baker.make(
        "access.ExchangeLanNetworkServiceConfig",
        scoping_account=c1)
    s2 = baker.make(
        "access.ClosedUserGroupNetworkServiceConfig",
        scoping_account=c1)

    # query configs
    configs = configs_svc.get_network_service_configs(
        scoping_account=c1,
        filters={
            "type": "exchange_lan",
        })

    assert s1 in configs
    assert not s2 in configs


@pytest.mark.django_db
def test_get_network_service_config__lookup():
    """Test getting a single network service config"""
    c1 = baker.make("crm.Account")
    c2 = baker.make("crm.Account")

    # Make some configs
    s1 = baker.make("access.ExchangeLanNetworkServiceConfig",
        scoping_account=c2)

    # Check lookup
    config = configs_svc.get_network_service_config(
        network_service_config=str(s1.pk),
        scoping_account=c2)

    assert config.pk == s1.pk

    # Check invalid pk
    with pytest.raises(NetworkServiceConfig.DoesNotExist):
        configs_svc.get_network_service_config(
            network_service_config="123459012345690",
            scoping_account=c1)


@pytest.mark.django_db
def test_get_network_service_config_ownership():
    """Test ownership test of single network service config"""
    c1 = baker.make("crm.Account")
    c2 = baker.make("crm.Account")

    # Make some configs
    s1 = baker.make(
        "access.ExchangeLanNetworkServiceConfig",
        scoping_account=c1)
    s2 = baker.make(
        "access.ExchangeLanNetworkServiceConfig",
        scoping_account=c2)

    # Check lookup
    config = configs_svc.get_network_service_config(
        network_service_config=s1,
        scoping_account=c1)

    assert config.pk == s1.pk
    assert id(config) == id(s1)

    # Check invalid ownership
    with pytest.raises(ResourceAccessDenied):
        configs_svc.get_network_service_config(
            network_service_config=s2,
            scoping_account=c1)


@pytest.mark.django_db
def test__create_vlan_qinq_config():
    """Test creating a vlan configuration"""
    vlan_config_request = {
        "vlan_type": VLanType.QINQ.value,
        "outer_vlan": {
            "ethertype": VLanEthertype.E_0x8100,
            "number": 123,
        },
        "vlans": [{"number": 23, "name": None},
                  {"number": 42, "name": "named"}],
    }
    vlan_config = configs_svc._create_vlan_config(
        network_service_config_request={
            "vlan_config": vlan_config_request,
        })

    assert vlan_config
    assert vlan_config.vlans.filter(number=23).exists()
    assert vlan_config.vlans.filter(number=42).exists()
    assert vlan_config.outer_vlan
    assert vlan_config.outer_vlan.ethertype == VLanEthertype.E_0x8100
    assert vlan_config.outer_vlan.number == 123


@pytest.mark.django_db
def test__create_vlan_dot1q_config():
    """Test creating a vlan configuration"""
    vlan_config_request = {
        "vlan_type": VLanType.DOT1Q.value,
        "vlans": [{"number": 23, "name": None},
                  {"number": 42, "name": "named"}],
    }
    vlan_config = configs_svc._create_vlan_config(
        network_service_config_request={
            "vlan_config": vlan_config_request,
        })

    assert vlan_config
    assert vlan_config.vlans.filter(number=23).exists()
    assert vlan_config.vlans.filter(number=42).exists()


@pytest.mark.django_db
def test__create_vlan_port_config():
    """Test creating a vlan configuration"""
    vlan_config_request = {
        "vlan_type": VLanType.PORT.value,
    }
    vlan_config = configs_svc._create_vlan_config(
        network_service_config_request={
            "vlan_config": vlan_config_request,
        })

    assert vlan_config


@pytest.mark.django_db
def test_create_eline_network_service_config():
    """Test creating an eline network service config"""
    c1 = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))

    # We need a connection
    connection = baker.make("access.Connection",
        scoping_account=c1)

    # Create a network service
    service = baker.make(
        "service.ELineNetworkService",
        nsc_required_contact_roles=[])
    baker.make(
        "service.MemberJoiningRule",
        consuming_account=c1,
        network_service=service)

    vlan_config = {
        "vlan_type": VLanType.DOT1Q.value,
        "vlans": [{"number": 42, "name": "named"}],
    }

    config_input = {
        "type": "e_line",
        "capacity": None,
        "purchase_order": "PO2342-1111",
        "external_ref": "23199",
        "contract_ref": None,
        "network_service": service,
        "connection": connection,
        "managing_account": c1,
        "consuming_account": c1,
        "billing_account": c1,
        "vlan_config": vlan_config,
    }

    # Create the config
    config = configs_svc.create_network_service_config(
        network_service_config_request=config_input,
        scoping_account=c1)

    assert config
    assert config.pk
    assert config.vlan_config
    assert config.vlan_config.vlans.filter(number=42).exists()


@pytest.mark.django_db
def test_create_elan_network_service_config():
    """Test creating an elan network service config"""
    c1 = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))

    # We need a connection
    connection = baker.make("access.Connection",
        scoping_account=c1)

    # Create a network service
    service = baker.make(
        "service.ELanNetworkService",
        nsc_required_contact_roles=[])

    # There has to be a member joining rule for the account
    baker.make(
        "service.MemberJoiningRule",
        consuming_account=c1,
        network_service=service)


    vlan_config = {
        "vlan_type": VLanType.DOT1Q.value,
        "vlans": [{"number": 42, "name": "named"}],
    }

    mac = baker.make("ipam.MacAddress", scoping_account=c1)

    config_input = {
        "type": "e_lan",
        "capacity": None,
        "purchase_order": "PO2342-1111",
        "external_ref": "23199",
        "contract_ref": None,
        "network_service": service,
        "connection": connection,
        "managing_account": c1,
        "consuming_account": c1,
        "billing_account": c1,
        "vlan_config": vlan_config,
        "mac_addresses": [mac],
    }

    # Create the config
    config = configs_svc.create_network_service_config(
        network_service_config_request=config_input,
        scoping_account=c1)

    assert config
    assert config.pk
    assert config.vlan_config
    assert config.vlan_config.vlans.filter(number=42).exists()
    assert mac in config.mac_addresses.all()


@pytest.mark.django_db
def test_create_etree_network_service_config():
    """Test creating an etree network service config"""
    c1 = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))

    # We need a connection
    connection = baker.make("access.Connection",
        scoping_account=c1)

    # Create a network service
    service = baker.make(
        "service.ETreeNetworkService",
        nsc_required_contact_roles=[])

    baker.make(
        "service.MemberJoiningRule",
        consuming_account=c1,
        network_service=service)

    vlan_config = {
        "vlan_type": VLanType.DOT1Q.value,
        "vlans": [{"number": 42, "name": "named"}],
    }

    mac = baker.make("ipam.MacAddress", scoping_account=c1)

    config_input = {
        "type": "e_tree",
        "capacity": None,
        "purchase_order": "PO2342-1111",
        "external_ref": "23199",
        "contract_ref": None,
        "network_service": service,
        "connection": connection,
        "managing_account": c1,
        "consuming_account": c1,
        "billing_account": c1,
        "vlan_config": vlan_config,
        "mac_addresses": [mac],
    }

    # Create the config
    config = configs_svc.create_network_service_config(
        network_service_config_request=config_input,
        scoping_account=c1)

    assert config
    assert config.pk
    assert config.vlan_config
    assert config.vlan_config.vlans.filter(number=42).exists()
    assert mac in config.mac_addresses.all()


@pytest.mark.django_db
def test_create_exchange_lan_network_service_config():
    """Test creating an exchange lan network service config"""
    c1 = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))

    # We need a connection
    connection = baker.make("access.Connection",
        scoping_account=c1)

    # Implementation contact
    impl_role = baker.make("crm.Role")
    impl = baker.make("crm.Contact",
        roles=[impl_role],
        scoping_account=c1)

    # A mac address
    mac = baker.make("ipam.MacAddress",
        scoping_account=c1)

    # Create a network service
    service = baker.make("service.ExchangeLanNetworkService",
        nsc_required_contact_roles=[impl_role],
        scoping_account=c1)

    net4 = baker.make("ipam.IpAddress",
        version=4,
        address="123.42.42.0",
        prefix_length=24,
        exchange_lan_network_service=service)

    net6 = baker.make("ipam.IpAddress",
        version=6,
        address="1000:23:42::0",
        prefix_length=64,
        exchange_lan_network_service=service)

    config_input = {
        "type": "exchange_lan",
        "asns": [1234],
        "mac_addresses": [mac],
        "inner_vlan": None,
        "outer_vlan": [[0, 4095]],
        "capacity": None,
        "purchase_order": "PO2342-1111",
        "external_ref": "23199",
        "contract_ref": None,
        "network_service": service,
        "contacts": [impl],
        "connection": connection,
        "managing_account": c1,
        "consuming_account": c1,
        "billing_account": c1,
    }

    # Create the config
    config = configs_svc.create_network_service_config(
        network_service_config_request=config_input,
        scoping_account=c1)

    assert config
    assert config.pk
    assert impl in config.contacts.all()
    assert mac in config.mac_addresses.all()


@pytest.mark.django_db
def test_update_eline_network_service_config():
    """Update an existing eline config"""
    impl_role = baker.make("crm.Role")
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    network_service = baker.make(
        "service.ELineNetworkService",
        nsc_required_contact_roles=[impl_role])
    config = baker.make(
        "access.ELineNetworkServiceConfig",
        network_service=network_service,
        scoping_account=account)
    contact = baker.make(
        "crm.Contact",
        roles=[impl_role],
        scoping_account=account)
    mac = baker.make(
        "ipam.MacAddress",
        scoping_account=config.scoping_account,
        managing_account=config.managing_account,
        consuming_account=config.consuming_account)

    # Prepare update
    po_num = "FOO-23-2000-01"
    inner_vlan = 23
    managing_account = "FAIL"

    update = {
        "purchase_order": po_num,
        "inner_vlan": inner_vlan,
        "managing_account": account,
        "consuming_account": account,
        "billing_account": account,
        "contacts": [contact],
    }

    # Update exchange lan network service config
    config = configs_svc.update_network_service_config(
        scoping_account=account,
        network_service_config=config,
        network_service_config_update=update)

    print(config.vlan_config)

    assert contact in config.contacts.all()
    assert config.purchase_order == po_num
    assert config.inner_vlan == inner_vlan
    assert config.managing_account == account
    assert config.consuming_account == account


@pytest.mark.django_db
def test_update_exchange_lan_network_service_config():
    """Update an exchange lan network service config"""
    impl_role = baker.make("crm.Role")
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    service = baker.make(
        "service.ExchangeLanNetworkService",
        nsc_required_contact_roles=[impl_role])
    config = baker.make(
        "access.ExchangeLanNetworkServiceConfig",
        network_service=service,
        scoping_account=account)
    contact = baker.make(
        "crm.Contact",
        roles=[impl_role],
        scoping_account=account)
    mac = baker.make(
        "ipam.MacAddress",
        scoping_account=account,
        managing_account=config.managing_account,
        consuming_account=config.consuming_account)

    # Prepare update
    po_num = "FOO-23-2000-01"
    inner_vlan = 23
    managing_account = baker.make(
        "crm.Account", scoping_account=account)
    consuming_account = baker.make(
        "crm.Account", scoping_account=account)

    update = {
        "purchase_order": po_num,
        "inner_vlan": inner_vlan,
        "managing_account": managing_account,
        "consuming_account": consuming_account,
        "billing_account": account,
        "contacts": [contact],
        "mac_addresses": [mac],
        "asns": [42],
    }

    # Update exchange lan network service config
    config = configs_svc.update_network_service_config(
        scoping_account=account,
        network_service_config=config,
        network_service_config_update=update)

    assert mac in config.mac_addresses.all()
    assert contact in config.contacts.all()
    assert config.purchase_order == po_num
    assert config.inner_vlan == inner_vlan
    assert config.managing_account == managing_account
    assert config.consuming_account == consuming_account


@pytest.mark.django_db
def test_destroy_network_service_config():
    """Test destruction of a network service config"""
    config = baker.make("access.ExchangeLanNetworkServiceConfig",
        _fill_optional=True)

    # Destroy this config
    configs_svc.destroy_network_service_config(
        scoping_account=config.scoping_account,
        network_service_config=config)


@pytest.mark.django_db
def test_create_invalid_network_feature_config():
    """Test creating a network feature config for an incompatible service"""
    account = baker.make(
        "crm.Account")
    managing_account = baker.make(
        "crm.Account",
        scoping_account=account)
    consuming_account = baker.make(
        "crm.Account",
        scoping_account=account)
    billing_account = baker.make(
        "crm.Account",
        scoping_account=account,
        billing_information=baker.make("crm.BillingInformation"))
    service = baker.make(
        "service.ExchangeLanNetworkService")
    feature = baker.make(
        "service.BlackholingNetworkFeature")
    service_config = baker.make(
        "access.ExchangeLanNetworkServiceConfig",
        network_service=service,
        scoping_account=account,
        managing_account=managing_account,
        consuming_account=consuming_account)

    with pytest.raises(NetworkFeatureNotAvailable):
        configs_svc.create_network_feature_config(
            scoping_account=account,
            network_feature_config_input={
                "network_feature": feature,
                "network_service_config": service_config,
                "managing_account": managing_account,
                "consuming_account": consuming_account,
                "billing_account": billing_account,
            })


@pytest.mark.django_db
def test_create_blackholing_network_feature_config():
    """Test feature configuration creation: Blackholing"""
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    managing_account = baker.make(
        "crm.Account",
        scoping_account=account)
    consuming_account = baker.make(
        "crm.Account",
        scoping_account=account)
    service = baker.make(
        "service.ExchangeLanNetworkService")
    feature = baker.make(
        "service.BlackholingNetworkFeature",
        network_service=service)
    service_config = baker.make(
        "access.ExchangeLanNetworkServiceConfig",
        network_service=service,
        scoping_account=account,
        managing_account=managing_account,
        consuming_account=consuming_account)

    config_input = {
        "type": "blackholing",
        "network_service_config": service_config,
        "network_feature": feature,
        "purchase_order": "PO2222-1111",
        "external_ref": "CX000000420",
        "contract_ref": None,
        "activated": True,
        "ixp_specific_configuration": None,
        "managing_account": managing_account,
        "consuming_account": consuming_account,
        "billing_account": account,
    }

    config = configs_svc.create_network_feature_config(
        scoping_account=account,
        network_feature_config_input=config_input)

    # Assertions - at lest it should be created without error.
    assert config
    assert config.pk


@pytest.mark.django_db
def test_create_ixp_router_network_feature_config():
    """Test feature configuration creation: IXPRouter"""
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    managing_account = baker.make(
        "crm.Account",
        scoping_account=account)
    consuming_account = baker.make(
        "crm.Account",
        scoping_account=account)
    service = baker.make(
        "service.ExchangeLanNetworkService")
    feature = baker.make(
        "service.IXPRouterNetworkFeature",
        network_service=service)
    service_config = baker.make(
        "access.ExchangeLanNetworkServiceConfig",
        network_service=service,
        scoping_account=account,
        managing_account=managing_account,
        consuming_account=consuming_account)


    config_input = {
        "type": "ixp_router",
        "network_service_config": service_config,
        "network_feature": feature,
        "purchase_order": "PO2222-1111",
        "external_ref": "CX000000420",
        "contract_ref": None,
        "password": None,
        "bgp_session_type": BGPSessionType.TYPE_ACTIVE,
        "max_prefix": 5000,
        "billing_account": account,
        "managing_account": managing_account,
        "consuming_account": consuming_account,
    }

    config = configs_svc.create_network_feature_config(
        scoping_account=account,
        network_feature_config_input=config_input)

    # Assertions
    assert config
    assert config.pk
    assert config.password
    assert isinstance(config.password, str)


@pytest.mark.django_db
def test_create_route_server_network_feature_config():
    """Test feature configuration creation: RouteServer"""
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    managing_account = baker.make(
        "crm.Account",
        scoping_account=account)
    consuming_account = baker.make(
        "crm.Account",
        scoping_account=account)

    service = baker.make("service.ExchangeLanNetworkService")
    feature = baker.make("service.RouteServerNetworkFeature",
                         available_bgp_session_types=[
                            BGPSessionType.TYPE_ACTIVE,
                            BGPSessionType.TYPE_PASSIVE,
                         ],
                         nfc_required_contact_roles=[],
                         network_service=service,
                         address_families=['af_inet'])

    service_config = baker.make(
        "access.ExchangeLanNetworkServiceConfig",
        network_service=service,
        scoping_account=account,
        managing_account=managing_account,
        consuming_account=consuming_account,
        billing_account=account)

    # AS-SET is dependent on the mode
    as_set_v4 = None
    if feature.session_mode == RouteServerSessionMode.MODE_PUBLIC:
        as_set_v4 = "AS-FOO@RIPE"

    config_input = {
        "type": "route_server",
        "network_service_config": service_config,
        "network_feature": feature,
        "purchase_order": "PO2222-1111",
        "external_ref": "CX000000420",
        "contract_ref": None,
        "password": None,
        "asn": 2342,
        "as_set_v4": as_set_v4,
        "bgp_session_type": BGPSessionType.TYPE_ACTIVE,
        "session_mode": feature.session_mode,
        "insert_ixp_asn": False,
        "max_prefix_v4": 5000,
        "managing_account": managing_account,
        "consuming_account": consuming_account,
        "billing_account": account,
        "contacts": [],
    }

    config = configs_svc.create_network_feature_config(
        scoping_account=account,
        network_feature_config_input=config_input)

    # Assertions
    assert config
    assert config.pk
    assert config.password
    assert isinstance(config.password, str)


@pytest.mark.django_db
def test_update_blackholing_network_feature_config():
    """Test updating a blackholing network feature configuration"""
    config = baker.make("access.BlackholingNetworkFeatureConfig")

    # Prepare update
    purchase_order = "FOO-23-42-1"
    external_ref = "11111-2"
    activated = True
    ixp_specific_configuration = "for something do icmp_ping_of_death();"

    update = {
        "purchase_order": purchase_order,
        "external_ref": external_ref,
        "activated": activated,
        "ixp_specific_configuration": ixp_specific_configuration,
    }

    # Perform update
    config = configs_svc.update_network_feature_config(
        scoping_account=config.scoping_account,
        network_feature_config=config,
        network_feature_config_update=update)

    # Assertions
    assert config.purchase_order == purchase_order
    assert config.external_ref == external_ref
    assert config.activated == activated
    assert config.ixp_specific_configuration == ixp_specific_configuration


@pytest.mark.django_db
def test_update_route_server_network_feature_config():
    """Test updating a routeserver network feature configuration"""
    feature = baker.make("service.RouteServerNetworkFeature",
        address_families=["af_inet6"])
    config = baker.make("access.RouteServerNetworkFeatureConfig",
        network_feature=feature)

    # Prepare update
    purchase_order = "FOO-23-42-1"
    external_ref = "11111-2"
    insert_ixp_asn = True
    password = "password"

    update = {
        "purchase_order": purchase_order,
        "external_ref": external_ref,
        "insert_ixp_asn": insert_ixp_asn,
        "password": password,
        "as_set_v6": "AS-FOO@BAR",
    }

    # Perform update
    config = configs_svc.update_network_feature_config(
        network_feature_config=config,
        network_feature_config_update=update)

    # Assertions
    assert config.purchase_order == purchase_order
    assert config.external_ref == external_ref
    assert config.insert_ixp_asn == insert_ixp_asn
    assert config.password == password


@pytest.mark.django_db
def test_update_ixp_router_network_feature_config():
    """Test updating an ixprouter network feature configuration"""
    config = baker.make("access.IXPRouterNetworkFeatureConfig")

    # Prepare update
    purchase_order = "FOO-23-42-1"
    external_ref = "11111-2"
    password = "password"

    update = {
        "purchase_order": purchase_order,
        "external_ref": external_ref,
        "password": password,
    }

    # Perform update
    config = configs_svc.update_network_feature_config(
        scoping_account=config.scoping_account,
        network_feature_config=config,
        network_feature_config_update=update)

    # Assertions
    assert config.purchase_order == purchase_order
    assert config.external_ref == external_ref
    assert config.password == password

