
"""
Tests for Demarcs Service
"""

import pytest
from model_bakery import baker

from jea.crm.services import contacts as contacts_svc
from jea.exceptions import ResourceAccessDenied
from jea.eventmachine.models import (
    State,
)
from jea.access.models import  (
    MediaType,
    DemarcationPoint,
)
from jea.access.services import demarcs as demarcs_svc
from jea.access.exceptions import (
    DemarcationPointUnavailable,
    DemarcationPointInUse,
    DemarcationPointNotReady,
)


def test_max_speed_for_media_type():
    """Test get max connection speed"""
    # Check properties
    for c_type in MediaType:
        speed = demarcs_svc.max_speed_for_media_type(c_type.value)
        assert speed > 0

    # Unknown type
    with pytest.raises(demarcs_svc.UnknownMediaTypeError):
        demarcs_svc.max_speed_for_media_type("FOO")


@pytest.mark.django_db
def test_allocate_demarcation_point():
    """Test port demarc allocation"""
    account = baker.make(
        "crm.Account")
    managing_account = baker.make(
        "crm.Account",
        scoping_account=account)
    consuming_account = baker.make(
        "crm.Account",
        scoping_account=account)
    billing_account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"),
        scoping_account=account)
    pop = baker.make(
        "catalog.PointOfPresence")
    device = baker.make(
        "catalog.Device",
        physical_point_of_presence=pop)
    baker.make(
        "catalog.DeviceCapability",
        device=device,
        media_type=MediaType.TYPE_10GBASE_LR.value,
        availability_count=1)
    impl_contact = baker.make(
        "crm.Contact",
        roles=[contacts_svc.get_default_role("implementation")],
        scoping_account=account,
        consuming_account=consuming_account)

    demarc_input = {
        "name": "demarc-aaa",
        "media_type": MediaType.TYPE_10GBASE_LR.value,
        "external_ref": "refref2000",
        "point_of_presence": pop,
        "contacts": [impl_contact],
        "managing_account": managing_account,
        "consuming_account": consuming_account,
        "billing_account": billing_account,
    }

    demarc = demarcs_svc.allocate_demarcation_point(
        scoping_account=account,
        demarcation_point_input=demarc_input)

    assert demarc
    assert demarc.pk



@pytest.mark.django_db
def test_allocate_demarcation_point_no_capacity():
    """Test port demarc allocation"""
    account = baker.make(
        "crm.Account")
    managing_account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"),
        scoping_account=account)
    consuming_account = baker.make(
        "crm.Account",
        scoping_account=account)
    pop = baker.make("catalog.PointOfPresence")
    device = baker.make(
        "catalog.Device",
        physical_point_of_presence=pop)
    baker.make(
        "catalog.DeviceCapability",
        device=device,
        media_type=MediaType.TYPE_10GBASE_LR.value,
        availability_count=0)

    demarc_input = {
        "name": "port-demarc-bbb",
        "media_type": MediaType.TYPE_10GBASE_LR.value,
        "external_ref": "refref2001",
        "point_of_presence": pop,
        "managing_account": managing_account,
        "consuming_account": consuming_account,
        "billing_account": managing_account,
    }

    with pytest.raises(demarcs_svc.DemarcationPointUnavailable):
        demarcs_svc.allocate_demarcation_point(
            scoping_account=account,
            demarcation_point_input=demarc_input)


@pytest.mark.django_db
def test_release_demarcation_point():
    """Release a port demarc"""
    demarc = baker.make("access.DemarcationPoint")
    demarcs_svc.release_demarcation_point(
        demarcation_point=demarc)

    assert demarc.state == State.DECOMMISSIONED


@pytest.mark.django_db
def test_release_demarcation_point_in_use():
    """Release a port demarc while in use"""
    connection = baker.make(
        "access.Connection")
    demarc = baker.make(
        "access.DemarcationPoint",
        connection=connection)

    with pytest.raises(demarcs_svc.DemarcationPointInUse):
        demarcs_svc.release_demarcation_point(
            demarcation_point=demarc)


@pytest.mark.django_db
def test_get_demarcation_points():
    """Test get all port demarcs for accounts"""
    account_a = baker.make(
        "crm.Account")
    account_b = baker.make(
        "crm.Account")

    demarc_a = baker.make(
        "access.DemarcationPoint",
        scoping_account=account_a)
    demarc_b = baker.make(
        "access.DemarcationPoint",
        scoping_account=account_b,
        _fill_optional=True)

    demarcs = demarcs_svc.get_demarcation_points(
        scoping_account=account_a)

    assert demarc_a in demarcs
    assert not demarc_b in demarcs


@pytest.mark.django_db
def test_get_demarcation_points_filtered():
    """Test get all port demarcs for accounts"""
    account = baker.make(
        "crm.Account")
    demarc_a = baker.make(
        "access.DemarcationPoint",
        scoping_account=account,
        _fill_optional=True)

    demarc_b = baker.make(
        "access.DemarcationPoint",
        scoping_account=account,
        _fill_optional=True)


    # Filter by connection id:
    demarcs = demarcs_svc.get_demarcation_points(
        scoping_account=account,
        filters={"connection": demarc_b.connection.id})

    assert demarc_b in demarcs
    assert demarc_a not in demarcs

    # Or external ref...
    demarcs = demarcs_svc.get_demarcation_points(
        scoping_account=account,
        filters={"external_ref": demarc_a.external_ref})

    assert demarc_a in demarcs
    assert demarc_b not in demarcs


@pytest.mark.django_db
def test_get_demarcation_points_exclude_archived():
    """Test get all port demarcs for accounts"""
    account = baker.make(
        "crm.Account")
    demarc_a = baker.make(
        "access.DemarcationPoint",
        state=State.ARCHIVED,
        scoping_account=account,
        _fill_optional=True)

    demarc_b = baker.make(
        "access.DemarcationPoint",
        state=State.PRODUCTION,
        scoping_account=account)

    # Filter by connection id:
    demarcs = demarcs_svc.get_demarcation_points(
        scoping_account=account,
        filters={"state__is_not": "archived"})

    assert not demarc_a in demarcs
    assert demarc_b in demarcs


@pytest.mark.django_db
def test_get_dmearcation_point():
    """Test getting a signle port demarc"""
    account_a = baker.make("crm.Account")
    account_b = baker.make("crm.Account")

    demarc_a = baker.make(
        "access.DemarcationPoint",
        scoping_account=account_a)
    demarc_b = baker.make(
        "access.DemarcationPoint",
        scoping_account=account_b)

    # Resolve demarcs
    demarc = demarcs_svc.get_demarcation_point(
        scoping_account=account_a,
        demarcation_point=str(demarc_a.id))
    assert demarc == demarc_a

    with pytest.raises(ResourceAccessDenied):
        demarcs_svc.get_demarcation_point(
            scoping_account=account_a,
            demarcation_point=demarc_b.id)


@pytest.mark.django_db
def test_get_dmearcation_point_scope_check():
    account_a = baker.make("crm.Account")
    account_b = baker.make("crm.Account")

    demarc_a = baker.make(
        "access.DemarcationPoint",
        scoping_account=account_a)

    demarc_b = baker.make(
        "access.DemarcationPoint",
        scoping_account=account_b)

    # We pass a port demarc object to skip lookup:
    # - Managing accounts matches.
    demarc = demarcs_svc.get_demarcation_point(
        demarcation_point=demarc_a,
        scoping_account=account_a)
    assert demarc == demarc_a

    # - Managing accounts missmatch
    with pytest.raises(ResourceAccessDenied):
        demarcs_svc.get_demarcation_point(
            scoping_account=account_a,
            demarcation_point=demarc_b)


@pytest.mark.django_db
def test_update_demarcation_point():
    impl_role = baker.make("crm.Role", name="implementation")
    account = baker.make("crm.Account")
    contact = baker.make("crm.Contact",
        scoping_account=account,
        consuming_account=account,
        roles=[impl_role])
    connection = baker.make("access.Connection",
        scoping_account=account)
    demarc = baker.make("access.DemarcationPoint",
        scoping_account=account)

    # Make Update
    update = {
        "connection": connection,
        "speed": 23,
        "contacts": [contact],
    }

    demarc = demarcs_svc.update_demarcation_point(
        scoping_account=account,
        demarcation_point=demarc,
        demarcation_point_update=update)

    assert demarc.connection == connection
    assert demarc.speed == 23
    assert contact in demarc.contacts.all()


@pytest.mark.django_db
def test_demarcation_point_can_join_connection():
    """Test connect demarc preflight check"""
    connection = baker.make("access.Connection")
    demarc = baker.make("access.DemarcationPoint",
        state=State.ALLOCATED)

    # This should work
    demarcs_svc.demarcation_point_can_join_connection(
        demarcation_point=demarc,
        connection=connection)


@pytest.mark.django_db
def test_demarcation_point_can_join_connection__in_use():
    """Test connect demarc preflight check"""
    connection = baker.make("access.Connection")
    demarc = baker.make("access.DemarcationPoint",
        state=State.ALLOCATED,
        connection=baker.make("access.Connection"))

    # We should get demarc in use
    with pytest.raises(DemarcationPointInUse):
        demarcs_svc.demarcation_point_can_join_connection(
            demarcation_point=demarc,
            connection=connection)


@pytest.mark.django_db
def test_demarcation_point_can_join_connection__not_ready():
    """Test connect demarc preflight check: state error"""
    connection = baker.make("access.Connection")
    demarc = baker.make("access.DemarcationPoint",
        state=State.PRODUCTION)

    with pytest.raises(DemarcationPointNotReady):
        demarcs_svc.demarcation_point_can_join_connection(
            demarcation_point=demarc,
            connection=connection)


@pytest.mark.django_db
def test_demarcation_point_can_join_connection__incompatible():
    """Test connect demarc preflight check: incompatible with connection"""
    connection = baker.make("access.Connection")
    demarc = baker.make("access.DemarcationPoint",
        state=State.ALLOCATED)

    # Create connected demarc with different pop
    baker.make("access.DemarcationPoint",
        connection=connection)

    with pytest.raises(DemarcationPointUnavailable):
        demarcs_svc.demarcation_point_can_join_connection(
            demarcation_point=demarc,
            connection=connection)

