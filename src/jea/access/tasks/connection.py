
"""
Connection Background Tasks
---------------------------

As this is just a sandbox, our tasks center around... waiting.
"""

import time
from typing import Optional

from django.conf import settings
from celery import result as celery_result 

from backend.celery import app as celery_app
from jea.eventmachine import active
from jea.eventmachine.models import State
from jea.access.events import (
    connection_state_changed,
)
from jea.access import models as access_models


@celery_app.task
def set_connection_state_task(connection_id, next_state, wait):
    """
    Set a connection state to production after
    a given timeout.

    :param connection: The connection to put into production
    :param next_state: The next state the connection will assume
    :param wait: The seconds to wait until doing this.
    """
    # Well, this is only acceptable as long as the wait time is
    # not that high and we have multiple workers to dispatch...
    time.sleep(wait)

    connection = access_models.Connection.objects.get(pk=connection_id)
    prev_state = connection.state
    connection.state = next_state
    connection.save()

    dispatch = active.make_dispatch(None)
    dispatch(connection_state_changed(connection, prev_state))


def set_connection_state(
        connection: access_models.Connection,
        next_state: State,
        wait: float = 1.5,
    ) -> Optional[celery_result.AsyncResult]:
    """Spawn set connection state task"""
    if getattr(settings, "TESTING", False):
        print("Skipping task dispatch in testing.")
        return None

    return set_connection_state_task.delay(
        connection.id,
        next_state.value,
        wait)

