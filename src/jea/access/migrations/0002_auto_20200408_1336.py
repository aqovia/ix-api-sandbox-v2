# Generated by Django 2.2.9 on 2020-04-08 13:36

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('catalog', '0001_initial'),
        ('access', '0001_initial'),
        ('crm', '0001_initial'),
        ('service', '0001_initial'),
        ('contenttypes', '0002_remove_content_type_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='networkserviceconfig',
            name='network_service',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='configs', to='service.NetworkService'),
        ),
        migrations.AddField(
            model_name='networkserviceconfig',
            name='polymorphic_ctype',
            field=models.ForeignKey(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='polymorphic_access.networkserviceconfig_set+', to='contenttypes.ContentType'),
        ),
        migrations.AddField(
            model_name='networkserviceconfig',
            name='scoping_account',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='scoped_networkserviceconfigs', to='crm.Account'),
        ),
        migrations.AddField(
            model_name='networkserviceconfig',
            name='vlan_config',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='network_service_configs', to='access.VLanConfig'),
        ),
        migrations.AddField(
            model_name='networkfeatureconfig',
            name='consuming_account',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='consumed_networkfeatureconfigs', to='crm.Account'),
        ),
        migrations.AddField(
            model_name='networkfeatureconfig',
            name='contacts',
            field=models.ManyToManyField(related_name='networkfeatureconfigs', to='crm.Contact'),
        ),
        migrations.AddField(
            model_name='networkfeatureconfig',
            name='managing_account',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='managed_networkfeatureconfigs', to='crm.Account'),
        ),
        migrations.AddField(
            model_name='networkfeatureconfig',
            name='network_feature',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='configs', to='service.NetworkFeature'),
        ),
        migrations.AddField(
            model_name='networkfeatureconfig',
            name='network_service_config',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='network_feature_configs', to='access.NetworkServiceConfig'),
        ),
        migrations.AddField(
            model_name='networkfeatureconfig',
            name='polymorphic_ctype',
            field=models.ForeignKey(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='polymorphic_access.networkfeatureconfig_set+', to='contenttypes.ContentType'),
        ),
        migrations.AddField(
            model_name='networkfeatureconfig',
            name='scoping_account',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='scoped_networkfeatureconfigs', to='crm.Account'),
        ),
        migrations.AddField(
            model_name='demarcationpoint',
            name='billing_account',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='billed_demarcationpoints', to='crm.Account'),
        ),
        migrations.AddField(
            model_name='demarcationpoint',
            name='connection',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='demarcation_points', to='access.Connection'),
        ),
        migrations.AddField(
            model_name='demarcationpoint',
            name='consuming_account',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='consumed_demarcationpoints', to='crm.Account'),
        ),
        migrations.AddField(
            model_name='demarcationpoint',
            name='contacts',
            field=models.ManyToManyField(related_name='demarcationpoints', to='crm.Contact'),
        ),
        migrations.AddField(
            model_name='demarcationpoint',
            name='managing_account',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='managed_demarcationpoints', to='crm.Account'),
        ),
        migrations.AddField(
            model_name='demarcationpoint',
            name='point_of_presence',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='demarcation_points', to='catalog.PointOfPresence'),
        ),
        migrations.AddField(
            model_name='demarcationpoint',
            name='scoping_account',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='scoped_demarcationpoints', to='crm.Account'),
        ),
        migrations.AddField(
            model_name='connection',
            name='billing_account',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='billed_connections', to='crm.Account'),
        ),
        migrations.AddField(
            model_name='connection',
            name='consuming_account',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='consumed_connections', to='crm.Account'),
        ),
        migrations.AddField(
            model_name='connection',
            name='contacts',
            field=models.ManyToManyField(related_name='connections', to='crm.Contact'),
        ),
        migrations.AddField(
            model_name='connection',
            name='managing_account',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='managed_connections', to='crm.Account'),
        ),
        migrations.AddField(
            model_name='connection',
            name='scoping_account',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='scoped_connections', to='crm.Account'),
        ),
    ]
