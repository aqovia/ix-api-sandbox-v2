
"""
Configs Service
---------------

Configure service accesses and features like
route servers and blackholing.
"""

import secrets
import random
import itertools
from typing import List

from django.db import transaction
from ixapi_schema.v2.constants.access import (
    VLanType,
    NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN,
    NETWORK_SERVICE_CONFIG_TYPE_E_LINE,
    NETWORK_SERVICE_CONFIG_TYPE_E_LAN,
    NETWORK_SERVICE_CONFIG_TYPE_E_TREE,
)

from jea.exceptions import ResourceAccessDenied, ValidationError
from jea.crm.exceptions import RequiredContactRolesInvalid
from jea.access.exceptions import (
    SessionModeInvalid,
)
from jea.eventmachine import active
from jea.eventmachine.models import State
from jea.service import models as service_models
from jea.crm.services import (
    accounts as accounts_svc,
    contacts as contacts_svc,
)
from jea.crm import models as crm_models
from jea.ipam import models as ipam_models
from jea.ipam.services import (
    ip_addresses as ip_addresses_svc,
    mac_addresses as mac_addresses_svc,
)
from jea.service.services import (
    network as network_svc,
    membership as membership_svc,
)
from jea.access.services import (
    connections as connections_svc,
)
from jea.access import (
    models as access_models,
    filters as access_filters,
)
from jea.access.models import (
    RouteServerSessionMode,
)
from jea.access.events import (
    network_service_config_created,
    network_service_config_destroyed,
    network_service_config_updated,
    network_service_config_state_changed,

    network_feature_config_created,
    network_feature_config_updated,
    network_feature_config_destroyed,
    network_feature_config_updated,
)
from utils.datastructures import whitelist_filter_fields


def assert_network_service_membership(
        account=None,
        network_service=None,
    ):
    """
    Check if the account can join a private vlan
    by creating a network service config

    :param account: A account
    :param network_service: A network service

    :raises ResourceAccessDenied: when account has no joining rule
    """
    if account == network_service.consuming_account:
        return # We are allowed to join our own service

    # In all other cases, we have to check if there is a rule
    # in place.
    joining_rules = membership_svc.get_member_joining_rules(
        network_service=network_service)
    membership = joining_rules.filter(
        consuming_account=account).first()
    if not membership:
        raise ResourceAccessDenied(
            network_service,
            details=(
                "The consuming account requires a `member-joining-rule` "
                "to access the network-service."))


def _create_legacy_vlan_config(
        scoping_account=None,
        network_service_config_request=None,
    ):
    """
    Create a vlan config from a legacy (v1) request.

    :param scoping_account: A scope limiting account
    :param network_service_config_request: A network service configuration
        request.
    """
    # Get outer vlan from range set
    outer_vlans = network_service_config_request.get("outer_vlan")
    if not outer_vlans:
        raise ValidationError(
            "Please provide a non empty set of vlan ranges.",
            field="outer_vlan")

    inner_vlan_number = network_service_config_request.get("inner_vlan")

    # Select vlan type
    if outer_vlans and inner_vlan_number:
        vlan_type = VLanType.QINQ
    else:
        vlan_type = VLanType.DOT1Q

    # Choose some outer vlan
    gen_outer_vlan_values = list(itertools.chain.from_iterable(outer_vlans))
    gen_outer_vlan = gen_outer_vlan_values[
        random.randint(0, len(gen_outer_vlan_values) - 1)]

    # Create VLan Configuration
    outer_vlan = access_models.VLan(
        number=gen_outer_vlan)
    vlan_config = access_models.VLanConfig(
        vlan_type=vlan_type,
        outer_vlan=outer_vlan)
    outer_vlan.save()
    vlan_config.save()

    # Get inner vlan if present
    inner_vlan_number = network_service_config_request.get("inner_vlan")
    if inner_vlan_number:
        inner_vlan = access_models.VLan(
            number=inner_vlan_number,
            vlan_config=vlan_config)
        inner_vlan.save()

    return vlan_config


def _create_vlan_config(
        scoping_account=None,
        network_service_config_request=None,
    ):
    """
    Create a vlan configuration from a request
    with a vlan config present.

    :param scoping_account: A scoping account
    :param network_service_config_request: A (v2) configuration request
    """
    if not "vlan_config" in network_service_config_request.keys():
        return None

    vlan_config_request = network_service_config_request["vlan_config"]
    vlan_type = VLanType(vlan_config_request["vlan_type"])

    # Create vlan configuration
    vlan_config = access_models.VLanConfig(
        vlan_type=vlan_type)
    vlan_config.save()

    # Add inner vlan configs
    if vlan_type == VLanType.DOT1Q or vlan_type == VLanType.QINQ:
        vlans_request = vlan_config_request["vlans"]
        for vlan_request in vlans_request:
            vlan = access_models.VLan(
                name=vlan_request["name"],
                number=vlan_request["number"],
                vlan_config=vlan_config,
            )
            vlan.save()

    # For a QINQ configuration add the outer vlan
    if vlan_type == VLanType.QINQ:
        outer_vlan_request = vlan_config_request["outer_vlan"]
        outer_vlan = access_models.VLan(
            number=outer_vlan_request["number"],
            ethertype=outer_vlan_request["ethertype"],
        )
        outer_vlan.save()
        vlan_config.outer_vlan = outer_vlan
        vlan_config.save()

    return vlan_config


def _update_vlan_config(
        scoping_account=None,
        network_service_config=None,
        network_service_config_update=None,
    ):
    """
    Update a network service config's vlan configuration.

    :param scoping_account: A scoping account
    :param network_service_config: A network service configuration
    :param network_service_config_update: A network service config
        update with vlan_config.
    """
    vlan_config = network_service_config.vlan_config
    if not vlan_config:
        raise ValidationError("VLanConfig missing", field="vlan_config")

    vlan_config_update = network_service_config_update["vlan_config"]
    vlan_type = vlan_config_update.get(
        "vlan_type", vlan_config.vlan_type)

    if vlan_type == VLanType.PORT:
        for vlan in vlan_config.vlans:
            vlan.delete()
        vlan_config.outer_vlan = None
        vlan_config.save()
        return

    if vlan_type == VLanType.DOT1Q:
        vlan_config.outer_vlan = None

    if vlan_type == VLanType.DOT1Q or vlan_type == VLanType.QINQ:
        if "vlans" in vlan_config_update.keys():
            # Clear current vlans
            for vlan in vlan_config.vlans:
                vlan.delete()

            vlans_update = vlan_config_update["vlans"]
            for vlan_request in vlans_update:
                vlan = access_models.VLan(
                    name=vlan_request["name"],
                    number=vlan_request["number"],
                    vlan_config=vlan_config,
                )
                vlan.save()

    # For a QINQ configuration add the outer vlan
    if vlan_type == VLanType.QINQ:
        if "outer_vlan" in vlan_config_update.keys():
            outer_vlan_request = vlan_config_update["outer_vlan"]
            outer_vlan = access_models.VLan(
                number=outer_vlan_request["number"],
                ethertype=outer_vlan_request["ethertype"],
            )
            outer_vlan.save()
            vlan_config.outer_vlan = outer_vlan
            vlan_config.save()


def _update_legacy_vlan_config(
        scoping_account=None,
        network_service_config=None,
        network_service_config_update=None,
    ):
    """
    Update a network service config's vlan configuration
    from legacy / v1 input.

    :param scoping_account: A scoping account
    :param network_service_config: A network service configuration
    :param network_service_config_update: A network service
        configuration update, containing an inner and outer vlan.
    """
    # Initialize vlan config if non is present
    vlan_config = network_service_config.vlan_config
    if not vlan_config:
        # Create new vlan config
        vlan_config = access_models.VLanConfig(
            vlan_type=VLanType.DOT1Q)
        vlan_config.save()
        # Assign to network service
        network_service_config.vlan_config = vlan_config
        network_service_config.save()

    update_fields = network_service_config_update.keys()

    # Outer VLAN
    if "outer_vlan" in update_fields:
        outer_vlans = network_service_config_update["outer_vlan"]
        if not outer_vlans:
            raise ValidationError(
                "Please provide a non empty set of vlan ranges.",
                field="outer_vlan")

        # Choose some outer vlan
        gen_outer_vlan_values = list(itertools.chain.from_iterable(outer_vlans))
        gen_outer_vlan = gen_outer_vlan_values[
            random.randint(0, len(gen_outer_vlan_values) - 1)]

        # Assign generated vlan value
        if vlan_config.outer_vlan:
            vlan_config.outer_vlan.delete()
        vlan_config.outer_vlan = access_models.VLan(
            number=gen_outer_vlan)
        vlan_config.outer_vlan.save()
        vlan_config.save()

    # Inner VLAN
    if "inner_vlan" in update_fields:
        # Clear out old assignments
        for vlan in vlan_config.vlans:
            vlan.delete()
        # Create new vlan
        inner_vlan_number = network_service_config_update["inner_vlan"]
        if inner_vlan_number is not None:
            # Only create if there actually is one
            vlan = access_models.VLan(
                number=network_service_config_update["inner_vlan"],
                vlan_config=vlan_config)
            vlan.save()


#TODO maybe merge with the below method
@transaction.atomic
def assign_network_feature_contacts(
        scoping_account=None,
        network_feature_config=None,
        contacts=None,
    ):
    """
    Assign contacts to a config object.
    Providing a list of required contacts, will assert the presence
    of at least one of the required contact types.

    :param network_feature_config: A network feature configuration,
        where the contacts can be assigned to.
    :param scoping_account: Override config managing account property
        when checking the ownership of the contact.
    :param contacts: A list of contact ids or contact objects.

    :raises RequiredContactTypesMissing:
    :raises ValidationError: In case a config object is missing
    """
    # Apply permission check and resovlve object if required.
    config = get_network_feature_config(
        scoping_account=scoping_account,
        network_feature_config=network_feature_config)

    # Now, do we have our config?
    if not config:
        raise ValidationError("A config object is required")

    if not scoping_account:
        scoping_account = config.scoping_account

    if not config.network_feature:
        raise ValidationError(
            "The config requires a network_feature.",
            field="network_service")

    # Load all contacts
    if not contacts:
        contacts = []
    contacts = [contacts_svc.get_contact(
                    contact=c,
                    scoping_account=scoping_account)
                for c in contacts]

    # Check the required contacts are included
    contacts_svc.assert_presence_of_contact_roles(
        config.network_feature.nfc_required_contact_roles.all(),
        contacts)

    # Reset contacts
    config.contacts.clear()

    if not contacts:
        return config # Nothing else to do

    # Assign contacts
    for contact in contacts:
        config.contacts.add(contact)

    return config


@transaction.atomic
def assign_network_service_contacts(
        scoping_account=None,
        network_service_config=None,
        contacts=None,
    ):
    """
    Assign contacts to a config object.
    Providing a list of required contacts, will assert the presence
    of at least one of the required contact types.

    :param network_service_config: A network service configuration,
        the implementation contacts can be assigned to.
    :param scoping_account: Override config managing account property
        when checking the ownership of the contact.
    :param contacts: A list of contact ids or contact objects.

    :raises RequiredContactTypesMissing:
    :raises ValidationError: In case a config object is missing
    """
    # Apply permission check and resovlve object if required.
    config = get_network_service_config(
        scoping_account=scoping_account,
        network_service_config=network_service_config)

    # Now, do we have our config?
    if not config:
        raise ValidationError("A config object is required")

    if not scoping_account:
        scoping_account = config.scoping_account

    if not config.network_service:
        raise ValidationError(
            "The config requires a network_service.",
            field="network_service")

    # Load all contacts
    if not contacts:
        contacts = []
    contacts = [contacts_svc.get_contact(
                    contact=c,
                    scoping_account=scoping_account)
                for c in contacts]

    # Check the required contacts are included
    contacts_svc.assert_presence_of_contact_roles(
        config.network_service.nsc_required_contact_roles.all(),
        contacts)

    # Reset contacts
    config.contacts.clear()

    if not contacts:
        return config # Nothing else to do

    # Assign contacts
    for contact in contacts:
        config.contacts.add(contact)

    return config


@transaction.atomic
def assign_mac_addresses(
        scoping_account=None,
        network_service_config=None,
        mac_addresses=None
    ):
    """
    Assign mac address to supported service configurations.
    Currently the only supported config types are:

      - ExchangeLanNetworkServiceConfig
      - ClosedUserGroupNetworkServiceConfig

    :param network_service_config: The network service config to assign macs to
    :param mac_addresses: A list of mac address objects.
    """
    if not hasattr(network_service_config, "mac_addresses"):
        raise TypeError("Config object does not have mac_addresses")

    if not scoping_account:
        scoping_account = network_service_config.scoping_account

    # Reset mac addresses
    network_service_config.mac_addresses.clear()

    if not mac_addresses:
        return network_service_config # Nothing left to do here

    # Assign mac addresses
    for mac in mac_addresses:
        mac = mac_addresses_svc.get_mac_address(
            scoping_account=scoping_account,
            mac_address=mac)

        network_service_config.mac_addresses.add(mac)

    return network_service_config


def get_network_service_configs(
        scoping_account=None,
        filters=None,
    ):
    """
    Get all service configurations within a account context.

    :param scoping_account: Limit the scope to the managing account.
    :param filters: A dict of filter params
    """
    configs = access_filters.NetworkServiceConfigFilter(filters).qs

    # Apply ownership filtering
    if scoping_account:
        configs = configs.filter(scoping_account=scoping_account)

    return configs


def get_network_service_config(
        scoping_account=None,
        network_service_config=None,
    ):
    """
    Get a single network service configuration.
    If billing or owning account are present, perform an
    ownership test.

    :param config: The config to load. Might be an ID.
    :param managing_account: The billing account
    :param consuming_account: The owning account of the config.
    """
    if isinstance(network_service_config, access_models.NetworkServiceConfig):
        # Just perform an ownership test
        if scoping_account and not \
            network_service_config.scoping_account == scoping_account:
            raise ResourceAccessDenied(network_service_config)

        return network_service_config

    # Otherwise get configs queryset:
    configs = get_network_service_configs(
        scoping_account=scoping_account)

    return configs.get(pk=network_service_config)


@transaction.atomic
@active.command
def create_network_service_config(
        dispatch,
        scoping_account=None,
        network_service_config_request=None,
    ) -> access_models.NetworkServiceConfig:
    """
    Create a new network service config.

    :param scoping_account: The managing account
    :param network_service_config_request: The configuration request
    """
    # Load networkservice and connection and check access permissions.
    network_service = network_svc.get_network_service(
        scoping_account=scoping_account,
        network_service=network_service_config_request["network_service"])
    connection = connections_svc.get_connection(
        scoping_account=scoping_account,
        connection=network_service_config_request["connection"])

    # Managing, consuming and billing account
    managing_account = accounts_svc.get_account(
        scoping_account=scoping_account,
        account=network_service_config_request["managing_account"])
    consuming_account = accounts_svc.get_account(
        scoping_account=scoping_account,
        account=network_service_config_request["consuming_account"])
    billing_account = accounts_svc.get_account(
        scoping_account=scoping_account,
        account=network_service_config_request["billing_account"])

    # Make sure, that the billing account can be used for billing
    accounts_svc.assert_billing_information(billing_account)

    # Dispatch network service configuration creation:
    config_type = network_service_config_request["type"]

    # ExchangeLan
    if config_type == NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN:
        config = _create_exchange_lan_network_service_config(
            scoping_account=scoping_account,
            managing_account=managing_account,
            consuming_account=consuming_account,
            billing_account=billing_account,
            network_service=network_service,
            connection=connection,
            network_service_config_request=network_service_config_request)
    # ELine
    elif config_type == NETWORK_SERVICE_CONFIG_TYPE_E_LINE:
        config = _create_eline_network_service_config(
            scoping_account=scoping_account,
            managing_account=managing_account,
            consuming_account=consuming_account,
            billing_account=billing_account,
            network_service=network_service,
            connection=connection,
            network_service_config_request=network_service_config_request)
    # ELan
    elif config_type == NETWORK_SERVICE_CONFIG_TYPE_E_LAN:
        config = _create_elan_network_service_config(
            scoping_account=scoping_account,
            managing_account=managing_account,
            consuming_account=consuming_account,
            billing_account=billing_account,
            network_service=network_service,
            connection=connection,
            network_service_config_request=network_service_config_request)
    # ETree
    elif config_type == NETWORK_SERVICE_CONFIG_TYPE_E_TREE:
        config = _create_etree_network_service_config(
            scoping_account=scoping_account,
            managing_account=managing_account,
            consuming_account=consuming_account,
            billing_account=billing_account,
            network_service=network_service,
            connection=connection,
            network_service_config_request=network_service_config_request)
    else:
        raise ValidationError("Unknown network service: {}".format(
            network_service_config_request["type"]))

    # Assign many to many contacts
    if "contacts" in network_service_config_request.keys():
        assign_network_service_contacts(
            scoping_account=scoping_account,
            network_service_config=config,
            contacts=network_service_config_request["contacts"])

    # Run statemachine transitions and refresh config
    dispatch(network_service_config_created(config))
    config.refresh_from_db()

    return config


def _create_exchange_lan_network_service_config(
        scoping_account=None,
        managing_account=None,
        consuming_account=None,
        billing_account=None,
        network_service=None,
        connection=None,
        network_service_config_request=None,
    ):
    """
    Create a new config for the exchange lan network service.

    :param network_service: The network service to configure
    :param connection: The connection to configure for the network service
    :param scoping_account: The owning account of the config.
    :param billing_account: The account paying for the config
    :param managing_account: A validated managing account
    :param consuming_account: A validated consuming account
    :param network_service_config_request: The configuration data
    """
    request_keys = network_service_config_request.keys()

    # Check if the service type is correct
    if not isinstance(network_service,
                      service_models.ExchangeLanNetworkService):
        raise ValidationError(
            "Service type must match config type. {} given, expected {}" \
                .format(network_service.__class__,
                        "ExchangeLanNetworkService"))

    # Create VLan configuration
    if "outer_vlan" in request_keys:
        # Assume v1
        vlan_config = _create_legacy_vlan_config(
            scoping_account=scoping_account,
            network_service_config_request=network_service_config_request)
    else:
        # Assume v2
        vlan_config = _create_vlan_config(
            scoping_account=scoping_account,
            network_service_config_request=network_service_config_request)

    if not vlan_config:
        raise ValidationError(
            "No valid vlan configuration present found in request")

    # Create the Exchange Lan Network Access Configuration
    config = access_models.ExchangeLanNetworkServiceConfig(
        state=State.REQUESTED,
        asns=network_service_config_request["asns"],
        vlan_config=vlan_config,
        capacity=network_service_config_request["capacity"],
        purchase_order=network_service_config_request["purchase_order"],
        external_ref=network_service_config_request["external_ref"],
        contract_ref=network_service_config_request["contract_ref"],
        network_service=network_service,
        scoping_account=scoping_account,
        consuming_account=consuming_account,
        managing_account=managing_account,
        billing_account=billing_account,
        connection=connection)
    config.save()

    # Assign mac address(es)
    mac_addresses = network_service_config_request.get("mac_addresses", [])
    if mac_addresses:
        assign_mac_addresses(
            scoping_account=scoping_account,
            network_service_config=config,
            mac_addresses=mac_addresses)

    # Allocate ip addresses
    ip_addresses_svc.allocate_ip_address(
        scoping_account=scoping_account,
        network_service_config=config,
        version=4)
    if network_service.ip_addresses.filter(version=6):
        ip_addresses_svc.allocate_ip_address(
            scoping_account=scoping_account,
            network_service_config=config,
            version=6)

    return config


def _create_eline_network_service_config(
        scoping_account=None,
        managing_account=None,
        consuming_account=None,
        billing_account=None,
        network_service=None,
        connection=None,
        network_service_config_request=None,
    ):
    """
    Create a new config for an eline network service.

    :param managing_account: The billing account
    :param consuming_account: The owning account of the config.
    :param network_servie: The network service to configure
    :param billing_account: The account paying for the eline config
    :param connection: The connection to configure for the network service
    :param network_service_config_request: The configuration data
    """
    # Check if the service type is correct
    if not isinstance(network_service, service_models.ELineNetworkService):
        raise ValidationError(
            "Service type must match config type. {} given, expected {}" \
                .format(network_service.__class__.__name__,
                        "ELineNetworkService"))
    # Membership check
    assert_network_service_membership(
        account=consuming_account,
        network_service=network_service)

    # Only allow for one membership per account
    if network_service.configs.filter(
        consuming_account=consuming_account).exists():
            raise ValidationError(
                "Only one eline network service config per account",
                field="network_service")

    # Only allow for two network service configs on an eline
    if network_service.configs.count() > 2:
        raise ValidationError(
            "There can only be two configs on an eline",
            field="network_service")

    vlan_config = _create_vlan_config(
        scoping_account=scoping_account,
        network_service_config_request=network_service_config_request)

    # Create the eline config
    config = access_models.ELineNetworkServiceConfig(
        state=State.REQUESTED,
        capacity=network_service_config_request.get("capacity"),
        purchase_order=network_service_config_request.get("purchase_order"),
        external_ref=network_service_config_request.get("external_ref"),
        contract_ref=network_service_config_request.get("contract_ref"),
        network_service=network_service,
        scoping_account=scoping_account,
        consuming_account=consuming_account,
        managing_account=managing_account,
        billing_account=billing_account,
        vlan_config=vlan_config,
        connection=connection)
    config.save()

    return config


def _create_elan_network_service_config(
        scoping_account=None,
        managing_account=None,
        consuming_account=None,
        billing_account=None,
        network_service=None,
        connection=None,
        network_service_config_request=None,
    ):
    """
    Create a new config for an ELan service.
    This is pretty much the same as with ELines, however we support
    the assignment of mac addresses.

    :param managing_account: The billing account
    :param consuming_account: The owning account of the config.
    :param billing_account: The account paying the bills
    :param network_servie: The network service to configure
    :param connection: The connection to configure for the network service
    :param network_service_config_request: The configuration data
    """
    # Check if the service type is correct
    if not isinstance(network_service, service_models.ELanNetworkService):
        raise ValidationError(
            "Service type must match config type. {} given, expected {}" \
                .format(network_service.__class__, "ELanNetworkService"))

    # Membership check
    assert_network_service_membership(
        account=consuming_account,
        network_service=network_service)

    vlan_config = _create_vlan_config(
        scoping_account=scoping_account,
        network_service_config_request=network_service_config_request)

    # Create the eline config
    config = access_models.ELanNetworkServiceConfig(
        state=State.REQUESTED,
        capacity=network_service_config_request.get("capacity"),
        purchase_order=network_service_config_request.get("purchase_order"),
        external_ref=network_service_config_request.get("external_ref"),
        contract_ref=network_service_config_request.get("contract_ref"),
        network_service=network_service,
        scoping_account=scoping_account,
        consuming_account=consuming_account,
        managing_account=managing_account,
        billing_account=billing_account,
        vlan_config=vlan_config,
        connection=connection)
    config.save()

    # Assign mac address(es)
    mac_addresses = network_service_config_request.get("mac_addresses", [])
    if mac_addresses:
        assign_mac_addresses(
            scoping_account=scoping_account,
            network_service_config=config,
            mac_addresses=mac_addresses)

    return config


def _create_etree_network_service_config(
        scoping_account=None,
        managing_account=None,
        consuming_account=None,
        billing_account=None,
        network_service=None,
        connection=None,
        network_service_config_request=None,
    ):
    """
    Create a new config for an ETree service.

    :param managing_account: The billing account
    :param consuming_account: The owning account of the config.
    :param billing_account: The billing account
    :param network_servie: The network service to configure
    :param connection: The connection to configure for the network service
    :param network_service_config_request: The configuration data
    """
    # Check if the service type is correct
    if not isinstance(network_service, service_models.ETreeNetworkService):
        raise ValidationError(
            "Service type must match config type. {} given, expected {}" \
                .format(network_service.__class__, "ELanNetworkService"))

    # Membership check
    assert_network_service_membership(
        account=consuming_account,
        network_service=network_service)

    vlan_config = _create_vlan_config(
        scoping_account=scoping_account,
        network_service_config_request=network_service_config_request)

    # Create the eline config
    config = access_models.ELanNetworkServiceConfig(
        state=State.REQUESTED,
        capacity=network_service_config_request.get("capacity"),
        purchase_order=network_service_config_request.get("purchase_order"),
        external_ref=network_service_config_request.get("external_ref"),
        contract_ref=network_service_config_request.get("contract_ref"),
        network_service=network_service,
        scoping_account=scoping_account,
        consuming_account=consuming_account,
        managing_account=managing_account,
        billing_account=billing_account,
        vlan_config=vlan_config,
        connection=connection)
    config.save()

    # Assign mac address(es)
    mac_addresses = network_service_config_request.get("mac_addresses", [])
    if mac_addresses:
        assign_mac_addresses(
            scoping_account=scoping_account,
            network_service_config=config,
            mac_addresses=mac_addresses)

    return config


@transaction.atomic
@active.command
def update_network_service_config(
        dispatch,
        scoping_account=None,
        network_service_config=None,
        network_service_config_update=None,
    ):
    """
    We allow for updates on the network service config
    objects. However, how much is updated depends on the
    type of network service configuration.

    :param dispatch: A bound dispatch
    :param scoping_account: A account managing the network service config.
    :param network_service_config: The network service configuration
        to update.
    :param network_service_config_update: A dict with config updates.
    """
    update_fields = network_service_config_update.keys()

    # Update common fields and properties
    # Accounts
    if "managing_account" in update_fields:
        managing_account = accounts_svc.get_account(
            account=network_service_config_update["managing_account"],
            scoping_account=scoping_account)
        network_service_config.managing_account = managing_account

    if "consuming_account" in update_fields:
        consuming_account = accounts_svc.get_account(
            account=network_service_config_update["consuming_account"],
            scoping_account=scoping_account)
        network_service_config.consuming_account = consuming_account

    if "billing_account" in update_fields:
        billing_account = accounts_svc.get_account(
            account=network_service_config_update["billing_account"],
            scoping_account=scoping_account)
        # Make sure account can be used for billing
        accounts_svc.assert_billing_information(billing_account)
        network_service_config.billing_account = billing_account

    # Vlan Configuration
    if "outer_vlan" in update_fields or "inner_vlan" in update_fields:
        _update_legacy_vlan_config(
            scoping_account=scoping_account,
            network_service_config=network_service_config,
            network_service_config_update=network_service_config_update)
    if "vlan_config" in update_fields:
        _update_vlan_config(
            scoping_account=scoping_account,
            network_service_config=network_service_config,
            network_service_config_update=network_service_config_update)

    # Connection
    if "connection" in network_service_config_update.keys():
        connection = connections_svc.get_connection(
            scoping_account=scoping_account,
            connection=network_service_config_update["connection"])
        network_service_config.connection = connection

    # Use update method dependent on config type.
    # Ownership is tested in the individual update methods
    if isinstance(
            network_service_config,
            access_models.ExchangeLanNetworkServiceConfig):
        network_service_config = _update_exchange_lan_network_service_config(
            scoping_account=scoping_account,
            network_service_config=network_service_config,
            network_service_config_update=network_service_config_update)
    elif isinstance(
            network_service_config,
            access_models.ELineNetworkServiceConfig):
        network_service_config = _update_eline_network_service_config(
            scoping_account=scoping_account,
            network_service_config=network_service_config,
            network_service_config_update=network_service_config_update)
    elif isinstance(
            network_service_config,
            access_models.ELanNetworkServiceConfig):
        network_service_config = _update_elan_network_service_config(
            scoping_account=scoping_account,
            network_service_config=network_service_config,
            network_service_config_update=network_service_config_update)
    elif isinstance(
            network_service_config,
            access_models.ETreeNetworkServiceConfig):
        network_service_config = _update_etree_network_service_config(
            scoping_account=scoping_account,
            network_service_config=network_service_config,
            network_service_config_update=network_service_config_update)
    else:
        raise NotImplementedError(
            ("Update method missing for "
             "network service config type: {}").format(
                 network_service_config.__class__.__name__))

    # Assign contacts
    if "contacts" in update_fields:
        assign_network_service_contacts(
            scoping_account=scoping_account,
            network_service_config=network_service_config,
            contacts=network_service_config_update["contacts"])

    network_service_config.save()

    # Inform observers about the update
    dispatch(network_service_config_updated(network_service_config))
    # Update configuration from database in case the state was
    # changed by the observers
    network_service_config.refresh_from_db()

    return network_service_config


def _update_exchange_lan_network_service_config(
        scoping_account=None,
        network_service_config=None,
        network_service_config_update=None,
    ):
    """
    Update an existing Exchange Lan Network Service configuration.

    :param scoping_account: The account managing the config
    :param config: An exchange lan network service config
    :param config_update: Update data
    """
    update_fields = network_service_config_update.keys()

    # Clean update data
    update = whitelist_filter_fields(network_service_config_update, [
        "capacity",
        "purchase_order",
        "external_ref",
        "asns",
    ])

    # Update the configuration with other fields
    for attr, value in update.items():
        setattr(network_service_config, attr, value)

    # Assign mac addresses if present in update
    if "mac_addresses" in update_fields:
        assign_mac_addresses(
            scoping_account=scoping_account,
            network_service_config=network_service_config,
            mac_addresses=network_service_config_update["mac_addresses"])

    return network_service_config


def _update_eline_network_service_config(
        scoping_account=None,
        network_service_config=None,
        network_service_config_update=None,
    ):
    """
    Update an existing ELine network service configuration.

    :param scoping_account: A account managing the config.
    :param config: An eline network service config
    :param config_update: Update data
    """
    # Clean update data
    update = whitelist_filter_fields(network_service_config_update, [
        "capacity",
        "purchase_order",
        "external_ref",
    ])

    # Update the configuration with other fields
    for attr, value in update.items():
        setattr(network_service_config, attr, value)

    return network_service_config


def _update_elan_network_service_config(
        scoping_account=None,
        network_service_config=None,
        network_service_config_update=None,
    ):
    """
    Update an existing ELine network service configuration.

    :param scoping_account: A account managing the config.
    :param config: An eline network service config
    :param config_update: Update data
    """
    # Clean update data
    update = whitelist_filter_fields(network_service_config_update, [
        "capacity",
        "purchase_order",
        "external_ref",
    ])

    # Update the configuration with other fields
    for attr, value in update.items():
        setattr(network_service_config, attr, value)

    # Assign mac addresses if present in update
    if "mac_addresses" in update_fields:
        assign_mac_addresses(
            scoping_account=scoping_account,
            network_service_config=network_service_config,
            mac_addresses=network_service_config_update["mac_addresses"])

    return network_service_config


def _update_etree_network_service_config(
        scoping_account=None,
        network_service_config=None,
        network_service_config_update=None,
    ):
    """
    Update an existing ETree network service configuration.

    :param scoping_account: A account managing the config.
    :param config: An eline network service config
    :param config_update: Update data
    """
    # Clean update data
    update = whitelist_filter_fields(network_service_config_update, [
        "capacity",
        "purchase_order",
        "external_ref",
    ])

    # Update the configuration with other fields
    for attr, value in update.items():
        setattr(network_service_config, attr, value)

    # Assign mac addresses if present in update
    if "mac_addresses" in update_fields:
        assign_mac_addresses(
            scoping_account=scoping_account,
            network_service_config=network_service_config,
            mac_addresses=network_service_config_update["mac_addresses"])

    return network_service_config


@active.command
def destroy_network_service_config(
        dispatch,
        scoping_account=None,
        network_service_config=None
    ):
    """
    Destroy a service config.

    :param config: The config to destroy. Might be an ID.
    :param managing: The owning account of the config.
    """
    config = get_network_service_config(
        scoping_account=scoping_account,
        network_service_config=network_service_config)

    # So for now... just dispatch the events and remove the config.
    dispatch(network_service_config_state_changed(
        config,
        prev_state=config.state,
        next_state=State.DECOMMISSIONED))
    dispatch(network_service_config_destroyed(config))

    # Destroy destroy destroy
    config.delete()

    return config

#
# Feature Configurations
#

def get_network_feature_configs(
        scoping_account=None,
        filters=None,
    ):
    """
    Get all feature configurations within a account context.

    :param managing_account: Scope configs to those of this account
    :param consuming_account: Scope configs to those of this account
    :param filters: A dict of filter params
    """
    configs = access_filters.NetworkFeatureConfigFilter(filters).qs

    # Apply ownership filtering
    if scoping_account:
        configs = configs.filter(scoping_account=scoping_account)

    return configs


def get_network_feature_config(
        scoping_account=None,
        network_feature_config=None,
    ):
    """
    Get a single feature configuration.
    If billing or owning account are present, perform an ownership test.

    :param config: The config to load. Might be an ID.
    :param managing_account: The billing account
    :param consuming_account: The owning account of the config.
    """
    if isinstance(network_feature_config, access_models.NetworkFeatureConfig):
        # Just perform an ownership test
        if scoping_account and not \
            network_feature_config.scoping_account == scoping_account:
            raise ResourceAccessDenied(network_feature_config)

        return network_feature_config

    configs = get_network_feature_configs(scoping_account=scoping_account)

    return configs.get(pk=network_feature_config)


@active.command
def create_network_feature_config(
        dispatch,
        scoping_account=None,
        network_feature_config_input=None,
    ):
    """
    Create a new network feature config.

    :param scoping_account: The account managing the config.
    :param config_input: The configuration data
    """
    # Load network feature and service config
    network_feature = network_svc.get_network_feature(
        scoping_account=scoping_account,
        network_feature=network_feature_config_input["network_feature"])
    service_config = get_network_service_config(
        scoping_account=scoping_account,
        network_service_config=network_feature_config_input["network_service_config"])

    # Check permissions on related objects
    managing_account = accounts_svc.get_account(
        scoping_account=scoping_account,
        account=network_feature_config_input.get("managing_account"))
    consuming_account = accounts_svc.get_account(
        scoping_account=scoping_account,
        account=network_feature_config_input.get("consuming_account"))

    billing_account = accounts_svc.get_account(
        scoping_account=scoping_account,
        account=network_feature_config_input["billing_account"])

    # Make sure, that the billing account can be used for billing
    accounts_svc.assert_billing_information(billing_account)

    # Check that the feature is available for
    # the service in question.
    network_svc.assert_network_feature_available(
        network_feature, service_config.network_service)

    # Dispatch configuration creation
    if network_feature_config_input["type"] == \
        access_models.NETWORK_FEATURE_CONFIG_TYPE_BLACKHOLING:
        config = _create_blackholing_network_feature_config(
            network_feature, service_config,
            scoping_account, managing_account, consuming_account,
            billing_account,
            network_feature_config_input)
    elif network_feature_config_input["type"] == \
        access_models.NETWORK_FEATURE_CONFIG_TYPE_ROUTESERVER:
        config = _create_route_server_network_feature_config(
            network_feature, service_config,
            scoping_account, managing_account, consuming_account,
            billing_account,
            network_feature_config_input)
    elif network_feature_config_input["type"] == \
        access_models.NETWORK_FEATURE_CONFIG_TYPE_IXPROUTER:
        config = _create_ixprouter_network_feature_config(
            network_feature, service_config,
            scoping_account, managing_account, consuming_account,
            billing_account,
            network_feature_config_input)
    else:
        raise ValidationError("Unknown network feature: {}".format(
            config["type"]))

    dispatch(network_feature_config_created(config))

    # Reload model after state machine transition
    config.refresh_from_db()

    return config


def _create_blackholing_network_feature_config(
        network_feature,
        network_service_config,
        scoping_account,
        managing_account,
        consuming_account,
        billing_account,
        config_input,
    ):
    """
    Create a blackholing network feature configuration.

    :param network_feature: The network feature to configure
    :param network_service_config: The service the feature will be attached to
    :param scoping_account: The account managing the config
    :param managing_account: The billing account
    :param consuming_account: The owning account of the config.
    :param config_input: Validated configuration data
    """
    if not isinstance(network_feature,
                      service_models.BlackholingNetworkFeature):
        raise ValidationError(
            ("The selected feature ({}) does not match the "
             "feature config for: blackholing").format(
                 network_feature.__class__.__name__))

    # Create the configuration for the blackholing feature
    feature = access_models.BlackholingNetworkFeatureConfig(
        state=State.REQUESTED,
        network_feature=network_feature,
        network_service_config=network_service_config,
        scoping_account=scoping_account,
        managing_account=managing_account,
        consuming_account=consuming_account,
        billing_account=billing_account,
        purchase_order=config_input["purchase_order"],
        external_ref=config_input["external_ref"],
        contract_ref=config_input["contract_ref"],
        activated=config_input["activated"],
        ixp_specific_configuration=config_input["ixp_specific_configuration"])
    feature.save()

    return feature


def _create_route_server_network_feature_config(
        network_feature,
        network_service_config,
        scoping_account,
        managing_account,
        consuming_account,
        billing_account,
        config_input,
    ):
    """
    Create a route server feature configuration.

    :param network_feature: The network feature to configure
    :param network_service_config: A network service configuration
    :param scoping_account: The account managing the config.
    :param managing_account: The billing account.
    :param consuming_account: The owning account of the config.
    :param config_input: Validated configuration data.
    """
    if not isinstance(network_feature,
                      service_models.RouteServerNetworkFeature):
        raise ValidationError(
            ("The selected feature ({}) does not match the "
             "feature config for: route_server").format(
                 network_feature.__class__.__name__))

    # Create password if not exists
    password = config_input["password"]
    if not password:
        password = secrets.token_hex(23)

    # Check if the session mode matches
    session_mode = config_input["session_mode"]
    if network_feature.session_mode != session_mode:
        raise SessionModeInvalid(
            ("The session_mode of the config must match the "
             "mode of the feature to configue."))

    # AS sets:
    as_set_v4 = config_input.get("as_set_v4")
    as_set_v6 = config_input.get("as_set_v6")
    if session_mode == RouteServerSessionMode.MODE_PUBLIC:
        # check network feature address families
        address_families = network_feature.address_families
        af_inet = ipam_models.AddressFamilies.AF_INET
        af_inet6 = ipam_models.AddressFamilies.AF_INET6

        if af_inet in address_families and not as_set_v4:
            raise SessionModeInvalid(
                ("The session mode requires the `as_set_v4`."),
                field="as_set_v4")
        if af_inet6 in address_families and not as_set_v6:
            raise SessionModeInvalid(
                ("The session mode requires the `as_set_v6`."),
                field="as_set_v6")

    # Check if session type is valid
    bgp_session_type = config_input["bgp_session_type"]
    if not bgp_session_type in network_feature.available_bgp_session_types:
        raise ValidationError(
            ("The bgp_session_type `{}` is not available on the "
             "route server.").format(bgp_session_type.value),
            field="bgp_session_type")

    # Create the Route Server access configuration:
    config = access_models.RouteServerNetworkFeatureConfig(
        state=State.REQUESTED,
        network_feature=network_feature,
        network_service_config=network_service_config,
        scoping_account=scoping_account,
        managing_account=managing_account,
        consuming_account=consuming_account,
        billing_account=billing_account,
        asn=config_input["asn"],
        purchase_order=config_input["purchase_order"],
        external_ref=config_input["external_ref"],
        contract_ref=config_input["contract_ref"],
        password=password,
        as_set_v4=as_set_v4,
        as_set_v6=as_set_v6,
        session_mode=session_mode,
        bgp_session_type=config_input["bgp_session_type"],
        max_prefix_v4=config_input.get("max_prefix_v4"),
        max_prefix_v6=config_input.get("max_prefix_v6"),
        insert_ixp_asn=config_input["insert_ixp_asn"])
    config.save()

    # Get contacts and check if the required contact types
    # of the network feature are present
    contacts = config_input["contacts"]
    assign_network_feature_contacts(
        network_feature_config=config,
        contacts=contacts,
        scoping_account=scoping_account)

    return config


def _create_ixprouter_network_feature_config(
        network_feature,
        network_service_config,
        scoping_account,
        managing_account,
        consuming_account,
        billing_account,
        config_input,
    ):
    """
    Create an ixprouter network feature configuration.
    The IXP router requires a password. The password can be either
    supplied in the config_input. In case no password is present,
    a password is generated.

    :param network_feature: The network feature to configure
    :param network_service_config: The service the feature will configured for
    :param managing_account: The billing account
    :param consuming_account: The owning account of the config
    :param billing_account: The account paying for the feature config
    :param config_input: The configuration data
    """
    if not isinstance(network_feature,
                      service_models.IXPRouterNetworkFeature):
        raise ValidationError(
            ("The selected feature ({}) does not match the "
             "feature config for: ixprouter").format(
                 network_feature.__class__.__name__))

    # Create password if not exists
    password = config_input["password"]
    if not password:
        password = secrets.token_hex(23)

    # Create the IXP Router configuration:
    feature = access_models.IXPRouterNetworkFeatureConfig(
        state=State.REQUESTED,
        network_feature=network_feature,
        network_service_config=network_service_config,
        scoping_account=scoping_account,
        managing_account=managing_account,
        consuming_account=consuming_account,
        billing_account=billing_account,
        purchase_order=config_input["purchase_order"],
        external_ref=config_input["external_ref"],
        contract_ref=config_input["contract_ref"],
        password=password,
        max_prefix=config_input.get("max_prefix"),
        bgp_session_type=config_input["bgp_session_type"])
    feature.save()

    return feature


@active.command
def update_network_feature_config(
        dispatch,
        scoping_account=None,
        network_feature_config=None,
        network_feature_config_update=None,
    ):
    """
    Update a network feature configuration.
    The correct update method is determined by the type of
    config passed. Pattern matching would be fun.

    :param dispatch: A bound event dispatcher
    :param scoping_account: The account managing the feature config
    :param consuming_account: The account owning the configuration
    :param config: The network feature config
    """
    if not network_feature_config_update:
        raise ValidationError("Missing config update")

    # Load the network feature configuration (and test ownership)
    config = get_network_feature_config(
        scoping_account=scoping_account,
        network_feature_config=network_feature_config)

    # Check permissions on related objects
    managing_account = accounts_svc.get_account(
        scoping_account=scoping_account,
        account=network_feature_config_update.get("managing_account"))
    consuming_account = accounts_svc.get_account(
        scoping_account=scoping_account,
        account=network_feature_config_update.get("consuming_account"))

    billing_account = accounts_svc.get_account(
        scoping_account=scoping_account,
        account=network_feature_config_update.get("billing_account"))

    if billing_account:
        # Assign account for billing, if billing information is present
        accounts_svc.assert_billing_information(billing_account)


    # Choose update method based on feature config type
    if isinstance(config, access_models.BlackholingNetworkFeatureConfig):
        config = _update_blackholing_network_feature_config(
            network_feature_config, network_feature_config_update)
    elif isinstance(config, access_models.RouteServerNetworkFeatureConfig):
        config = _update_route_server_network_feature_config(
            config, network_feature_config_update)
    elif isinstance(config, access_models.IXPRouterNetworkFeatureConfig):
        config = _update_ixp_router_network_feature_config(
            config, network_feature_config_update)
    else:
        raise NotImplementedError(
            ("The provided network feature config: '{}' "
             "has no update capabilities.").format(
                 config.__class__.__name__))

    # Done? Inform our observers
    dispatch(network_feature_config_updated(config))

    return config


def _update_blackholing_network_feature_config(
        config,
        config_update,
    ):
    """
    Update an existing blackholing network feature configuration.

    :param config: The network feature config
    :param config_update: The update to apply. Might be partial.
    """
    # Clean update data
    update = whitelist_filter_fields(config_update, [
        "purchase_order",
        "external_ref",
        "ixp_specific_configuration",
        "activated",
        "scoping_account",
        "managing_account",
        "consuming_account",
    ])

    with transaction.atomic():
        for attr, value in update.items():
            setattr(config, attr, value)
        config.save()

    # TODO: This is a stub. Add IP address assignment.

    return config


@transaction.atomic
def _update_route_server_network_feature_config(
        config,
        config_update,
    ):
    """
    Update a route server network feature configuration.

    :param config: The route server config
    :param config_update: The update to apply. Might be partial.
    """
    # Clean update data
    update = whitelist_filter_fields(config_update, [
        "purchase_order",
        "external_ref",
        "password",
        "max_prefix_v4",
        "max_prefix_v6",
        "insert_ixp_asn",
        "bgp_session_type",
        "managing_account",
        "consuming_account",
        "billing_account",
    ])

    # Get network service feature
    feature = config.network_feature

    # Set whitelisted attributes
    for attr, value in update.items():
        setattr(config, attr, value)

    # Set session mode
    if "session_mode" in config_update.keys():
        session_mode = config_update["session_mode"]

        # Check if the session mode matches
        if feature.session_mode != session_mode:
            raise SessionModeInvalid(
                ("The session_mode of the config must match the "
                 "mode of the feature to configue."))

    # Handle dependent as set
    as_set_v4 = config_update.get("as_set_v4", config.as_set_v4)
    as_set_v6 = config_update.get("as_set_v6", config.as_set_v6)

    if config.session_mode == RouteServerSessionMode.MODE_PUBLIC:
        families = feature.address_families
        af_inet = ipam_models.AddressFamilies.AF_INET
        af_inet6 = ipam_models.AddressFamilies.AF_INET6

        if af_inet in families and not as_set_v4:
            raise SessionModeInvalid(
                ("The session mode requires the `as_set_v4`"),
                field="as_set_v4")

        if af_inet6 in families and not as_set_v6:
            raise SessionModeInvalid(
                ("The session mode requires the `as_set_v6`"),
                field="as_set_v6")

    config.as_set_v4 = as_set_v4
    config.as_set_v6 = as_set_v6

    # Update contacts
    if "contacts" in config_update.keys():
        contacts = config_update["contacts"]
        assign_network_feature_contacts(
            network_feature_config=config,
            contacts=contacts,
            scoping_account=scoping_account)

    # Persist changes
    config.save()

    return config


def _update_ixp_router_network_feature_config(
        config,
        config_update,
    ):
    """
    Update an existing ixp router feature configuration.

    :param config: The network feature config
    :param config_update: The update to apply. Might be partial.
    """
    # Clean update data
    update = whitelist_filter_fields(config_update, [
        "purchase_order",
        "external_ref",
        "password",
        "max_prefix",
        "bgp_session_type",
        "managing_account",
        "consuming_account",
        "billing_account",
    ])

    with transaction.atomic():
        for attr, value in update.items():
            setattr(config, attr, value)
        config.save()

    return config


@active.command
def destroy_network_feature_config(
        dispatch,
        scoping_account=None,
        network_feature_config=None
    ):
    """
    Destroy a feature config.

    :param network_feature_config: The config to destroy. Might be an ID.
    :param scoping_account: The account managing the config.
    """
    config = get_network_feature_config(
        scoping_account=scoping_account,
        network_feature_config=network_feature_config)

    config.delete()
    dispatch(network_feature_config_destroyed(config))

    return config

