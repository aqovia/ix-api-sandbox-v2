
"""
Connections Service
-------------------

Create new connections with multiple port demarcation points
or disconnect port demarcs.

In general: Configure connections.
"""

from typing import Iterable, Optional, Any

from django.utils import text as text_utils
from django.db import transaction

from jea.exceptions import (
    ResourceAccessDenied,
    ValidationError,
)
from jea.eventmachine import active
from jea.eventmachine.models import State
from jea.crm.services import (
    accounts as accounts_svc,
    contacts as contacts_svc,
)
from jea.access import models as access_models
from jea.access.models import (
    Connection,
    DemarcationPoint,
)
from jea.access.services import demarcs as demarcs_svc
from jea.access.events import (
    demarc_connected,
    demarc_disconnected,
    demarc_state_changed,
    connection_created,
    connection_updated,
    connection_destroyed,
    connection_state_changed,
)
from jea.access.filters import (
    ConnectionFilter,
)
from jea.access.exceptions import (
    ConnectionModeUnavailable,
    DemarcationPointUnavailable,
    DemarcationPointInUse
)
from jea.crm import models as crm_models


def make_connection_name(account=None) -> str:
    """Make connection name"""
    # When a account is present, limit to account connections and
    # include account in the naming schema: conn-account-name-32
    if account:
        counter = account.consumed_connections.count() + 1
        tag = text_utils.slugify(account.name)[:42]
        return f"conn-{tag}-{counter}"

    # Fallback
    counter = Connection.objects.count() + 1
    return f"conn-{counter}"


def get_connections(
        scoping_account=None,
        filters=None
    ) -> Iterable[Connection]:
    """
    Get connections scoped to managing account.
    Either or both might be None, in which case the filter is
    not applied and all connection objects are considered.

    :param scoping_account: Manages connections
    :param filters: A mapping of string filter params
    """
    if filters:
        connections = ConnectionFilter(filters).qs
    else:
        connections = access_models.Connection.objects.all()

    # Apply ownership filteres
    if scoping_account:
        connections = connections.filter(scoping_account=scoping_account)

    return connections


def get_connection(
        connection=None,
        scoping_account=None,
    ) -> Connection:
    """
    Get a connection and perform ownership check on it,
    in case billing- or owning account are provided.

    :param connection: The connection to load
    :param scoping_account: The account managing the connection

    :raises ResourceAccessDenied: When the managing account
        lacks access rights.
    """
    if not connection:
        raise ValidationError("A connection needs to be provided")

    if not isinstance(connection, access_models.Connection):
        connection = Connection.objects.get(pk=connection)

    # We don't need to fetch the connection from the
    # database, however we need to perform a ownership test
    if scoping_account and not \
        connection.scoping_account == scoping_account:
        raise ResourceAccessDenied(connection)

    # Everythings fine, just pass back the connection
    return connection


@active.command
def connect_demarcation_point(
        dispatch,
        connection=None,
        demarcation_point=None,
        scoping_account=None,
    ) -> Connection:
    """
    Add a port demarcation point to a connection.

    :param connection: The connection the port demarc should be attached to.
    :param demarcation_point: The allocated port demarcation point to
        include in the connection.
    :returns: The connection the port was attached to.
    """
    connection = get_connection(
        connection=connection,
        scoping_account=scoping_account)

    # We require port demarcs provided from our user and we
    # need to check the ownership of these. So, we only allow
    # port demarcs assigned to the billing account.
    # This will fail and raise an error if a port demarc is unavilable
    # to a given account.
    demarcation_point = demarcs_svc.get_demarcation_point(
        demarcation_point=demarcation_point,
        scoping_account=scoping_account)

    # Check if there is anything to do
    if demarcation_point.connection == connection:
        return connection # Nothing to do here!

    # Check if we can join the connection
    demarcs_svc.demarcation_point_can_join_connection(
        scoping_account=scoping_account,
        connection=connection,
        demarcation_point=demarcation_point)

    # Looking good. Let's assign this port to this connection
    with transaction.atomic():
        demarcation_point.connection = connection
        demarcation_point.save()

        # Emit events
        dispatch(demarc_connected(
            demarcation_point,
            connection))

    # Refersh model after state transition
    connection.refresh_from_db()

    return connection


@active.command
def disconnect_demarcation_point(
        dispatch,
        scoping_account=None,
        demarcation_point=None,
    ) -> DemarcationPoint:
    """
    Remove a port demarcation point from a connection.

    :param scoping_account: The account used for scope checking
    :param demarcation_point: The port demarcation point to disconnect
    """
    # Get demarc
    demarcation_point = demarcs_svc.get_demarcation_point(
        demarcation_point=demarcation_point,
        scoping_account=scoping_account)

    # Check if demarc is connected
    connection = demarcation_point.connection
    if not connection:
        raise DemarcationPointNotConnected

    # Disconnect
    demarcation_point.connection = None
    demarcation_point.save()

    # Emit event
    dispatch(demarc_disconnected(
        demarcation_point,
        connection))

    # Refresh model after state transition
    demarcation_point.refresh_from_db()

    return demarcation_point


@transaction.atomic
@active.command
def create_connection(
        dispatch,
        scoping_account=None,
        connection_input=None,
    ) -> Connection:
    """
    Create a new connection based on user data supplied
    via `connection_input` and assigned to the billing and
    owning account.

    Reseller connections might be used from a subaccount.

    :param scoping_account: The account managing the connection

    :returns: A new connection.
    """
    if not scoping_account:
        raise ValidationError("A `scoping_account` is required.")

    # Check permissions on related objects
    managing_account = accounts_svc.get_account(
        scoping_account=scoping_account,
        account=connection_input["managing_account"])
    consuming_account = accounts_svc.get_account(
        scoping_account=scoping_account,
        account=connection_input["consuming_account"])
    billing_account = accounts_svc.get_account(
        scoping_account=scoping_account,
        account=connection_input["billing_account"])

    # Make sure billing account can be used for billing
    accounts_svc.assert_billing_information(billing_account)

    # Load contacts
    contacts = connection_input["contacts"]
    contacts_svc.assert_presence_of_contact_roles(
        required_contact_roles=[
            contacts_svc.get_default_role("implementation"),
        ],
        contacts=contacts,
        scoping_account=scoping_account)

    # Check ownership on implementation contacts
    contacts = [
        contacts_svc.get_contact(
            scoping_account=scoping_account,
            contact=c)
        for c in contacts]

    # Prepare mode and timeout
    connection_mode = connection_input["mode"]
    lacp_timeout = None
    if connection_mode == access_models.ConnectionMode.MODE_LACP:
        lacp_timeout = connection_input.get(
            "lacp_timeout",
            access_models.LACPTimeout.TIMEOUT_SLOW)

    # Connections are named by us
    connection_name = make_connection_name(account=consuming_account)

    # Get the ratelimiting on the connection, if there is any.
    connection_speed = connection_input.get("connection_speed", None)

    # We not have everything required to create a new connection,
    # but in case anything goes wrong with assigning the port demarc
    # to a connection, we want to be able to rollback.
    connection = access_models.Connection(
        name=connection_name,
        state=State.REQUESTED,
        mode=connection_mode,
        lacp_timeout=lacp_timeout,
        speed=connection_speed,

        # Ownable
        scoping_account=scoping_account,
        managing_account=managing_account,
        consuming_account=consuming_account,
        billing_account=billing_account,
        external_ref=connection_input.get("external_ref"),
        contract_ref=connection_input.get("contract_ref"),
        purchase_order=connection_input.get("purchase_order", ""),
    )
    connection.save()

    # Assign contacts
    connection.contacts.set(contacts)

    # Emit events and refresh model after state transition
    dispatch(connection_created(connection))
    connection.refresh_from_db()

    return connection


def assert_connection_can_assume_mode(
        connection: Connection,
        mode: access_models.ConnectionMode,
    ):
    """
    Check that a connection is able to assume a mode:
    E.g. a lag can only be change to standalone, if there
    is only one demarc connected.

    :raises ConnectionModeUnavailable:
    """
    # Check modes:
    if mode == access_models.ConnectionMode.MODE_STANDALONE:
        if connection.demarcation_points.count() > 1:
            raise ConnectionModeUnavailable(
                "There is more than one demarc attached to the connection")

    # All other should be ok


@transaction.atomic
@active.command
def update_connection(
        dispatch,
        scoping_account=None,
        connection=None,
        connection_update=None,
    ) -> Connection:
    """Update a given connection"""
    if not connection_update:
        raise ValidationError("update data for connection is missing")

    update_fields = connection_update.keys()

    # Load connection and check permissions
    connection = get_connection(
        connection=connection,
        scoping_account=scoping_account)

    # Allright. Let's see what we can update
    # Ownership: Billing and owning account
    if "managing_account" in update_fields:
        managing_account = accounts_svc.get_account(
            account=connection_update["managing_account"],
            scoping_account=scoping_account)
        connection.managing_account = managing_account

    if "consuming_account" in update_fields:
        consuming_account = accounts_svc.get_account(
            account=connection_update["consuming_account"],
            scoping_account=scoping_account)
        connection.consuming_account = consuming_account

    if "billing_account" in update_fields:
        billing_account = accounts_svc.get_account(
            account=connection_update["billing_account"],
            scoping_account=scoping_account)
        accounts_svc.assert_billing_information(billing_account)
        connection.billing_account = billing_account

    # Ratelimiting speed
    if "speed" in update_fields:
        connection.speed = connection_update["speed"]

    # Connection Mode and LACP timeout
    mode = connection_update.get("mode")
    lacp_timeout = connection_update.get(
        "lacp_timeout", connection.lacp_timeout)

    if "mode" in update_fields:
        assert_connection_can_assume_mode(connection, mode)
        if mode == access_models.ConnectionMode.MODE_LACP:
            if not lacp_timeout:
                raise ValidationError(
                    "A valid lacp timeout (slow, fast) is required",
                    field="lacp_timeout")
        else:
            lacp_timeout = None

        connection.mode = mode
        connection.lacp_timeout = lacp_timeout

    if "lacp_timeout" in update_fields:
        connection.lacp_timeout = lacp_timeout

    # Implementation contacts:
    # Check ownership on implementation contacts
    if "contacts" in update_fields:
        contacts = connection_update["contacts"]
        contacts = [
            contacts_svc.get_contact(
                scoping_account=scoping_account,
                contact=c)
            for c in contacts]

        # Check for presence of required accounts
        contacts_svc.assert_presence_of_contact_roles(
            required_contact_roles=[
                contacts_svc.get_default_role("implementation"),
            ],
            contacts=contacts,
            scoping_account=scoping_account)

        connection.contacts.set(contacts)

    # Purchase Order / External Ref / Contract Ref
    # TODO: Assigning these should be refactored and generalized.
    if "purchase_order" in update_fields:
        connection.purchase_order = connection_update["purchase_order"]
    if "external_ref" in update_fields:
        connection.external_ref = connection_update["external_ref"]
    if "contract_ref" in update_fields:
        connection.contract_ref = connection_update["contract_ref"]

    # Persist changes and trigger state changes
    connection.save()
    dispatch(connection_updated(connection))
    connection.refresh_from_db()

    return connection


@transaction.atomic
@active.command
def archive_connection(
        dispatch,
        scoping_account=None,
        connection=None,
    ):
    """
    Archive a connection.

    Release all connected demarcation points and then archive
    this connection.

    When accounts are provided, they will be used for scope checking.

    :param scoping_account: The account managing the connection
    :param connection: The connection to dissolve.
    """
    # Perform scope checking. In case the accounts do not match,
    # the object does not exist in the requested scope.
    connection = get_connection(
        scoping_account=scoping_account,
        connection=connection)

    # Disconnect all port demarcs
    for demarc in connection.demarcation_points.all():
        disconnect_demarcation_point(
            scoping_account=scoping_account,
            demarcation_point=demarc)

    # Archive connection
    prev_state = connection.state
    connection.state = State.ARCHIVED
    connection.save()

    dispatch(connection_state_changed(connection, prev_state))
    dispatch(connection_destroyed(connection))

    connection.refresh_from_db()

    return connection

